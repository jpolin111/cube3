using System;
using Gtk;
using Cube;
using System.IO;
using Aria;
using System.Collections.Generic;

namespace cubeget
{
	class MainClass
	{
		static CubeProjectSelect projSelect;
		
		public static void Main (string[] args)
		{		
			string CDE_MAIN = null;
			//Initialize the localization (Translations support)
			Mono.Unix.Catalog.Init("i8n1","./data/locale");						
			Console.WriteLine("[Cube] Language : " + Gtk.Global.SetLocale());						
			
			CubeProcess cube = new CubeProcess();			
			
			if(args.Length > 0)
			{								
				if(Directory.Exists(args[0]))
					Directory.SetCurrentDirectory(args[0]);
				if(args.Length == 2)
					CDE_MAIN = args[1];
			}
			
			//Check if intended to run using CDE - Camicri Apps Bundle (cube-mono)
			if(CDE_MAIN!=null)
				InitializeUseCDE(CDE_MAIN);
			else
			{
				try{
					//Delete all app redirects (if any)
					if(File.Exists(cube.Configuration.BinDirectory+"axel"))
						File.Delete(cube.Configuration.BinDirectory+"axel");
					if(File.Exists(cube.Configuration.BinDirectory+"aria2c"))
						File.Delete(cube.Configuration.BinDirectory+"aria2c");
				}catch{}
			}
			
			//Set PATH to third party downloaders (For Windows)
			if (!cube.Configuration.IsAtLinuxSystem)
            {
				try{
                string environ = System.Environment.GetEnvironmentVariable("PATH");
                environ = environ + ";" +
                    Directory.GetCurrentDirectory() + "\\" +
                    cube.Configuration.ThirdPartyDirectory + "axel;" +
                    Directory.GetCurrentDirectory() + "\\" +
                    cube.Configuration.ThirdPartyDirectory + "aria2;" +
					Directory.GetCurrentDirectory() + "\\" +
					cube.Configuration.BinDirectory + ";";
                System.Environment.SetEnvironmentVariable("PATH", environ);                
				}
				catch
				{
					Console.WriteLine("[Cube] Failed to set downloader path for non-apt-linux system. Problems may occur in downloading.");
				}
            }
			
			//Load theme based on the configuration file
			string theme = CubeSConfigurationManager.GetValue("config","gtk-theme");
			if(theme==null)
				theme = "default";
			CubeResources.ApplyGtkTheme(theme);
			
			//Initialize all and start the Cube Project Select window
			Application.Init ();
			
			projSelect = new CubeProjectSelect(cube);			
			projSelect.Show();
			projSelect.OpenedProject += HandleOpenedProject;
			
			//Set event invoke for any internal exceptions
			GLib.ExceptionManager.UnhandledException += HandleUnhandledException;
					
			Application.Run ();			
		}

		static void HandleUnhandledException (GLib.UnhandledExceptionArgs args)
		{
			CubeConfiguration config = new CubeConfiguration();
			string log = config.LogsDirectory + "cube-crash-"+DateTime.Now.ToLongDateString().Replace(" ","")+"-"+DateTime.Now.ToLongTimeString()+".log";
			File.WriteAllText(log,args.ExceptionObject+"\n"+args.ToString());
		}		
		
		public static void InitializeUseCDE(string location)
		{
			CubeConfiguration config = new CubeConfiguration();
			Console.WriteLine("[Cube] Will now invoke all programs using cube-mono");
			string cde_path = config.TemporaryFilesDirectory + "cde_location.cube";
			if(File.Exists(cde_path))
				File.Delete(cde_path);
			
			if(!location.EndsWith("/"))
				location += "/";
			
			location += "cde-exec";
			
			StreamWriter writer = new StreamWriter(cde_path);
			writer.WriteLine(location);
			writer.Close();
						
			//Create a aria2c clone
			writer = new StreamWriter(config.BinDirectory+"aria2c");
			writer.WriteLine("#!/bin/bash");
			writer.WriteLine(config.CDEPath + " -f aria2c $@");
			writer.Close();
			
			//Create a axel clone
			writer = new StreamWriter(config.BinDirectory+"axel");
			writer.WriteLine("#!/bin/bash");
			writer.WriteLine(config.CDEPath + " -f axel $@");
			writer.Close();
			
			ProcessOutput.Run("chmod","+x "+config.BinDirectory+"aria2c");
			ProcessOutput.Run("chmod","+x "+config.BinDirectory+"axel");	
		}

		static void HandleOpenedProject (object sender, EventArgs e)
		{			
			projSelect.Destroy();
			
			//Open the cube main window containing the current project selected
			CubeWindow win = new CubeWindow(sender as CubeProject);
			win.Visible = true;
		}
	}
}