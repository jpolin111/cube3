
// This file has been generated by the GUI designer. Do not modify.
namespace cubeget
{
	public partial class AddAptRepositorySelectDialog
	{
		private global::Gtk.VBox vboxAddApt;
		private global::Gtk.HBox hbox2;
		private global::Gtk.Image imgIcon;
		private global::Gtk.Label lblAddAptDesc;
		private global::Gtk.Label label4;
		private global::Gtk.HBox hbox3;
		private global::Gtk.ComboBox cboSeries;
		private global::Gtk.Button btnAddApt;
		private global::Gtk.Button buttonCancel;
		
		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget cubeget.AddAptRepositorySelectDialog
			this.Name = "cubeget.AddAptRepositorySelectDialog";
			this.Title = global::Mono.Unix.Catalog.GetString ("Cube Add Apt Repository : Select Series");
			this.Icon = global::Gdk.Pixbuf.LoadFromResource ("cubeget.bin.Debug.data.resources.icons.big.cubelogo.ico");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Internal child cubeget.AddAptRepositorySelectDialog.VBox
			global::Gtk.VBox w1 = this.VBox;
			w1.Name = "dialog1_VBox";
			w1.BorderWidth = ((uint)(2));
			// Container child dialog1_VBox.Gtk.Box+BoxChild
			this.vboxAddApt = new global::Gtk.VBox ();
			this.vboxAddApt.Name = "vboxAddApt";
			this.vboxAddApt.Spacing = 6;
			this.vboxAddApt.BorderWidth = ((uint)(6));
			// Container child vboxAddApt.Gtk.Box+BoxChild
			this.hbox2 = new global::Gtk.HBox ();
			this.hbox2.Name = "hbox2";
			this.hbox2.Spacing = 6;
			// Container child hbox2.Gtk.Box+BoxChild
			this.imgIcon = new global::Gtk.Image ();
			this.imgIcon.Name = "imgIcon";
			this.hbox2.Add (this.imgIcon);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox2 [this.imgIcon]));
			w2.Position = 0;
			w2.Expand = false;
			w2.Fill = false;
			// Container child hbox2.Gtk.Box+BoxChild
			this.lblAddAptDesc = new global::Gtk.Label ();
			this.lblAddAptDesc.Name = "lblAddAptDesc";
			this.lblAddAptDesc.Xalign = 0F;
			this.lblAddAptDesc.LabelProp = global::Mono.Unix.Catalog.GetString ("The PPA ppa:ubuntu/wine is from the Wine Adajsdjasldj\nwith an repository address from <b>deb asdkj sadhjashd main</b>\n\nYour project operating system series is <b>precise</b>. You can now add it to the repository.");
			this.lblAddAptDesc.UseMarkup = true;
			this.lblAddAptDesc.Wrap = true;
			this.hbox2.Add (this.lblAddAptDesc);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox2 [this.lblAddAptDesc]));
			w3.Position = 1;
			this.vboxAddApt.Add (this.hbox2);
			global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.vboxAddApt [this.hbox2]));
			w4.Position = 0;
			w4.Expand = false;
			w4.Fill = false;
			// Container child vboxAddApt.Gtk.Box+BoxChild
			this.label4 = new global::Gtk.Label ();
			this.label4.Name = "label4";
			this.label4.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Series Available</b>");
			this.label4.UseMarkup = true;
			this.vboxAddApt.Add (this.label4);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.vboxAddApt [this.label4]));
			w5.Position = 1;
			w5.Expand = false;
			w5.Fill = false;
			// Container child vboxAddApt.Gtk.Box+BoxChild
			this.hbox3 = new global::Gtk.HBox ();
			this.hbox3.Name = "hbox3";
			this.hbox3.Spacing = 6;
			// Container child hbox3.Gtk.Box+BoxChild
			this.cboSeries = global::Gtk.ComboBox.NewText ();
			this.cboSeries.Name = "cboSeries";
			this.hbox3.Add (this.cboSeries);
			global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox3 [this.cboSeries]));
			w6.Position = 0;
			// Container child hbox3.Gtk.Box+BoxChild
			this.btnAddApt = new global::Gtk.Button ();
			this.btnAddApt.CanFocus = true;
			this.btnAddApt.Name = "btnAddApt";
			this.btnAddApt.UseUnderline = true;
			// Container child btnAddApt.Gtk.Container+ContainerChild
			global::Gtk.Alignment w7 = new global::Gtk.Alignment (0.5F, 0.5F, 0F, 0F);
			// Container child GtkAlignment.Gtk.Container+ContainerChild
			global::Gtk.HBox w8 = new global::Gtk.HBox ();
			w8.Spacing = 2;
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Image w9 = new global::Gtk.Image ();
			w9.Pixbuf = global::Stetic.IconLoader.LoadIcon (
				this,
				"add",
				global::Gtk.IconSize.Menu
			);
			w8.Add (w9);
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Label w11 = new global::Gtk.Label ();
			w11.LabelProp = global::Mono.Unix.Catalog.GetString ("Add Apt Repository");
			w11.UseUnderline = true;
			w8.Add (w11);
			w7.Add (w8);
			this.btnAddApt.Add (w7);
			this.hbox3.Add (this.btnAddApt);
			global::Gtk.Box.BoxChild w15 = ((global::Gtk.Box.BoxChild)(this.hbox3 [this.btnAddApt]));
			w15.Position = 1;
			w15.Expand = false;
			w15.Fill = false;
			this.vboxAddApt.Add (this.hbox3);
			global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.vboxAddApt [this.hbox3]));
			w16.Position = 2;
			w16.Expand = false;
			w16.Fill = false;
			w1.Add (this.vboxAddApt);
			global::Gtk.Box.BoxChild w17 = ((global::Gtk.Box.BoxChild)(w1 [this.vboxAddApt]));
			w17.Position = 0;
			// Internal child cubeget.AddAptRepositorySelectDialog.ActionArea
			global::Gtk.HButtonBox w18 = this.ActionArea;
			w18.Name = "dialog1_ActionArea";
			w18.Spacing = 10;
			w18.BorderWidth = ((uint)(5));
			w18.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));
			// Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.buttonCancel = new global::Gtk.Button ();
			this.buttonCancel.CanDefault = true;
			this.buttonCancel.CanFocus = true;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseStock = true;
			this.buttonCancel.UseUnderline = true;
			this.buttonCancel.Label = "gtk-cancel";
			this.AddActionWidget (this.buttonCancel, -6);
			global::Gtk.ButtonBox.ButtonBoxChild w19 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w18 [this.buttonCancel]));
			w19.Expand = false;
			w19.Fill = false;
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.DefaultWidth = 475;
			this.DefaultHeight = 230;
			this.Show ();
		}
	}
}
