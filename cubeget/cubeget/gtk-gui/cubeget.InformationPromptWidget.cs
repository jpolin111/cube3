
// This file has been generated by the GUI designer. Do not modify.
namespace cubeget
{
	public partial class InformationPromptWidget
	{
		private global::Gtk.VBox vbox3;
		private global::Gtk.Label label7;
		
		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget cubeget.InformationPromptWidget
			global::Stetic.BinContainer.Attach (this);
			this.Name = "cubeget.InformationPromptWidget";
			// Container child cubeget.InformationPromptWidget.Gtk.Container+ContainerChild
			this.vbox3 = new global::Gtk.VBox ();
			this.vbox3.Name = "vbox3";
			this.vbox3.Spacing = 6;
			this.vbox3.BorderWidth = ((uint)(6));
			// Container child vbox3.Gtk.Box+BoxChild
			this.label7 = new global::Gtk.Label ();
			this.label7.Name = "label7";
			this.label7.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>No Package Found :(</b>\n\nTo search for a specific package, type its name to the search bar above\n\nYou can also search for an occurence of a package name (In list form)\n\t<b>firefox*</b> - shows all packages that <i>starts</i> with the word <i>'firefox'</i>\n\t<b>*firefox</b> - shows all packages that <i>ends</i> with the word <i>'firefox'</i>\n\t<b>*firefox*</b> - shows all packages that <i>contains</i> the word <i>'firefox'</i>\n\n<b>Quick Search</b> - If packages are listed, click on any part of the list\nand type some text to search in the list");
			this.label7.UseMarkup = true;
			this.vbox3.Add (this.label7);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox3 [this.label7]));
			w1.Position = 0;
			this.Add (this.vbox3);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
		}
	}
}
