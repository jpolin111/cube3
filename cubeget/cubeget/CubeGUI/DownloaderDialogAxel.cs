/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Gtk;
using System.Collections.Generic;
using System.Threading;
using Cube;
using Axel;
using Mono.Unix;

namespace cubeget
{
	public partial class DownloaderDialogAxel : Gtk.Dialog
	{		
		CubeSource[] s;		
		CubePackage[] p;		
		AxelDownloader axel;
		CubeProject proj;
		
		public bool Success = false;		
		public bool StopAllDownloads = false;
		public bool blnSourceVerified = false;
		
		
		CubeConfiguration config = new CubeConfiguration();
		string strMessage;
		
		int intMode = -1;
		int intCount = 0;
		int intSourceStart = 0;
		
		public event EventHandler Finished;
		public event EventHandler ErrorOccured;
		
		List<CubePackage> lstFailedToDownloadPackages;
		List<CubeSource> lstFailedToDownloadSources;
		List<CubeReleaseGPG> lstReleases;
		List<AxelData> lstData;
		
		ListStore lstrStatus = new ListStore(typeof(string),typeof(int),typeof(string),typeof(string));
		
		public DownloaderDialogAxel (string strMessage, string strInnerMessage)
		{
			this.Build ();
			this.Title = strMessage;
			this.strMessage = strInnerMessage;
			this.Shown += HandleShown;
			this.imgDownload.Pixbuf = CubeResources.LoadIcon("big","download");
			lblMessage.Text = strInnerMessage;
			
			lstFailedToDownloadPackages = new List<CubePackage>();
			lstFailedToDownloadSources = new List<CubeSource>();
			
			InitializeTree();			
			this.Show();			
		}

		void HandleShown (object sender, EventArgs e)
		{
			
		}
		
		public void SetPackagesToDownload(CubePackage[] pks, string strDownloadDir)
		{
			intMode = 1;			
			this.p = pks;
			
			lstData = new List<AxelData>();
			AxelData data;
			
			foreach(CubePackage p in pks)
			{
				data = new AxelData();								
				data.FileDirectory = strDownloadDir;
				data.URL = p.GetValue("Filename");
				data.IsAsynchronous = true;

				if(!config.IsAtLinuxSystem)
				{		
					//Comment : To be redirected to PATH environment
					//data.ApplicationName = "axel.exe";
					////Comment : To be redirected to PATH environmentdata.ApplicationDirectory = config.ThirdPartyDirectory + "axel";					
				}
				else
				{
					data.ApplicationName = "axel";
					data.ApplicationDirectory = config.BinDirectory;
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","axel-parameters");
				string axel_conn = CubeSConfigurationManager.GetValue("config","axel-connections");
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(axel_conn!=null)
				{
					int conn = int.Parse(axel_conn);
					data.Connections = conn;
				}
				
				string size = ConvertSize(p.GetValue("Size"));
				
				lstrStatus.AppendValues(p.GetValue("Package"),0,size,"Queued");
				lstData.Add(data);
			}
			StartDownloading();
		}
		
		public void SetSourcesToDownload(CubeProject p, CubeSource[] s, CubeReleaseGPG[] r, string strDownloadDir)
		{			
			intMode = 2;
			this.s = s;	
			this.proj = p;
			lstReleases = new List<CubeReleaseGPG>();
			lstData = new List<AxelData>();
			
			AxelData data;
			
			if(r !=null)
			{
			//Releases
			foreach(CubeReleaseGPG rel in r)
			{
				//Release Link				
				data = new AxelData();				
				data.FileDirectory = strDownloadDir;
				data.FileName = rel.ReleaseFilename;
				data.URL = rel.ReleaseLink;
				data.DeleteExisting = true;				
				data.IsAsynchronous = true;
				data.Connections = 2;
				
				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.ApplicationName = "axel.exe";
					//data.ApplicationDirectory = config.ThirdPartyDirectory + "axel";
				}
				else
				{
					data.ApplicationName = "axel";
					data.ApplicationDirectory = config.BinDirectory;
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","axel-parameters");				
				if(extra_params!=null)
					data.Arguments = extra_params;
				
				lstrStatus.AppendValues(rel.ReleaseLink,0,"","Queued");
				lstReleases.Add(rel);
				lstData.Add (data);	
					
				/*				
				//Release GPG Link
				data = new AxelData();
				data.FileDirectory = strDownloadDir;
				data.FileName = rel.ReleaseGPGFilename;
				data.URL = rel.ReleaseGPGLink;
				data.DeleteExisting = true;
				data.IsAsynchronous = true;
				data.Connections = 1;
				
				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.ApplicationName = "axel.exe";
					//data.ApplicationDirectory = config.ThirdPartyDirectory + "axel";
				}
				else
				{
					data.ApplicationName = "axel";
					data.ApplicationDirectory = config.BinDirectory;
				}
				
				if(extra_params!=null)
					data.Arguments = extra_params;
				
								
				lstrStatus.AppendValues(rel.ReleaseGPGLink,0,"","Queued");
				lstReleases.Add(rel);
				lstData.Add (data);
				*/
			}
			}
			intSourceStart = lstData.Count;
						
			//Sources
			//-1 to separate "status" source list (Expected always at end)
			for(int i=0;i<s.Length-1;i++)
			{								
				//Source Link
				data = new AxelData();				
				data.FileDirectory = strDownloadDir;
				data.FileName = s[i].Filename;
				data.URL = s[i].Link;
				data.DeleteExisting = true;
				data.IsAsynchronous = true;

				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.ApplicationName = "axel.exe";
					//data.ApplicationDirectory = config.ThirdPartyDirectory + "axel";
				}
				else
				{
					data.ApplicationName = "axel";
					data.ApplicationDirectory = config.BinDirectory;
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","axel-parameters");
				string axel_conn = CubeSConfigurationManager.GetValue("config","axel-connections");
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(axel_conn!=null)
				{
					int conn = int.Parse(axel_conn);
					data.Connections = conn;
				}
				
				lstrStatus.AppendValues(s[i].Link,0,"","Queued");
				lstData.Add(data);
			}
						
			StartDownloading();
		}
		
		public void InitializeTree()
		{
			List<TreeColumnHelper> lst = new List<TreeColumnHelper>();
			lst.Add(new TreeColumnHelper(Catalog.GetString("Package"),0,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Progress"),1,"progress",true));			
			lst.Add(new TreeColumnHelper(Catalog.GetString("Size"),2,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Status"),3,"text",true));			
			TreeHelper helper = new TreeHelper(treeDownload,lstrStatus,lst.ToArray());
			helper.Initialize();
		}
		
		public void StartDownloading()
		{			
			if(StopAllDownloads)
				return;
			
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);
			lstrStatus.SetValue(iter,3,Catalog.GetString("Downloading"));
			
			if(((string)lstrStatus.GetValue(iter,2)).Contains("kb"))
				lstData[intCount].Connections = 2;
			
			if(intMode == 2) //If sources, check source if already updated
			{
				if(!(lstData[intCount].FileName.EndsWith("Release") || lstData[intCount].FileName.EndsWith(".gpg")))
				{
					if(CubeReleaseGPG.CheckIfSourceUpdated(this.proj,s[intCount - intSourceStart]))
					{
						HandleFinished(axel,EventArgs.Empty);
						return;
					}		
				}
			}
									
			axel = new AxelDownloader(lstData[intCount]);
			axel.ProgressChanged += HandleProgressChanged;			
			axel.Finished += HandleFinished;
			axel.ErrorOccured += HandleErrorOccured;
						
			axel.StartDownloadThread();
		}
		
		void HandleProgressChanged (object sender, EventArgs e)
		{			
			Application.Invoke(delegate{
				Gtk.TreeIter iter;
				lstrStatus.IterNthChild(out iter,intCount);	
				lstrStatus.SetValue(iter,1,axel.Progress);
				if(intMode == 2)
				{
					lstrStatus.SetValue(iter,2,axel.TotalSize);
				}
				QueueDraw();
			});
		}	
		
		void HandleFinished (object sender, EventArgs e)
		{
			Application.Invoke(delegate{
				
				Gtk.TreeIter iter;			
				lstrStatus.IterNthChild(out iter,intCount);
				lstrStatus.SetValue(iter,3,Catalog.GetString("Done"));
				
				if(Finished!=null)
				{
					if(intMode == 1)
						Finished(p[intCount],EventArgs.Empty);
					else if(intMode == 2)
					{
						if(lstData[intCount].FileName.EndsWith("Release") || lstData[intCount].FileName.EndsWith(".gpg"))
						{						
							Finished(lstReleases[intCount],EventArgs.Empty);
						}
						else
						{
							Console.WriteLine("Finished download {0}",s[intCount - intSourceStart].Filename);
							Finished(s[intCount - intSourceStart],EventArgs.Empty);
						}
					}
				}
				
				intCount++;
				
				lblMessage.Markup = strMessage + " <i>("+intCount+" of "+lstData.Count+")</i>";
				
				prgProgress.Fraction = (float)intCount/(float)lstData.Count;
			
				if(intCount<lstData.Count)
				{
					StartDownloading();
				}
				else
				{
					Success = true;
					buttonCancel.Activate();				
				}
			});
		}
		
		void HandleErrorOccured (object sender, EventArgs e)
		{
			Application.Invoke(delegate{
				Gtk.TreeIter iter;
				lstrStatus.IterNthChild(out iter,intCount);
				lstrStatus.SetValue(iter,3,string.Format(Catalog.GetString("Failed. {0}"),axel.ErrorMessage));
				
				try{
					System.IO.File.Delete(lstData[intCount].FullFilePath);
				}catch{}				
							
				if(intMode == 1)
				{
					if(ErrorOccured!=null)
						ErrorOccured(p[intCount],EventArgs.Empty);
					lstFailedToDownloadPackages.Add(p[intCount]);
				}
				else if(intMode == 2)
				{
					if(ErrorOccured!=null)
					{
						if(lstData[intCount].FileName.EndsWith("Release") || lstData[intCount].FileName.EndsWith(".gpg"))
							ErrorOccured(lstReleases[intCount],EventArgs.Empty);
						else
						{
							ErrorOccured(s[intCount - intSourceStart],EventArgs.Empty);
							lstFailedToDownloadSources.Add(s[intCount - intSourceStart]);
						}
					}				
				}
				
				intCount++;			
					prgProgress.Fraction = (float)intCount/(float)lstData.Count;
				
				if(intCount<lstData.Count)
				{	
					StartDownloading();
				}
				else
					buttonCancel.Activate();
			});
		}
		
		string ConvertSize(string size)
		{
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
		
		public void StopAllDownload()
		{
			StopAllDownloads = true;
			axel.Terminate();
		}
		
		public AxelDownloader Axel
		{
			get { return axel; }
		}
		
		public CubePackage[] PackagesFailedToDownload
		{
			get { return this.lstFailedToDownloadPackages.ToArray(); }
		}
		
		public CubeSource[] SourcesFailedToDownload
		{
			get { return this.lstFailedToDownloadSources.ToArray(); }
		}		
	}
}