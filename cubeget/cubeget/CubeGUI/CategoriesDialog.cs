// 
//  CategoriesDialog.cs
//  
//  Author:
//       Jake Capangpangan <camicrisystems@gmail.com>
// 
//  Copyright (c) 2013 Camicri Systems
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
using System;
using System.Collections.Generic;
using Cube;
using Gtk;
using Mono.Unix;
using System.IO;

namespace cubeget
{
	public partial class CategoriesDialog : Gtk.Dialog
	{
		SortedList<string,string> dicSections = new SortedList<string, string>();
		ListStore lstStore = new ListStore(typeof(string));
		CubeConfiguration config = new CubeConfiguration();
		
		public event EventHandler CategorySelected;
		
		public string Category = "";
		
		public CategoriesDialog ()
		{
			this.Build ();				
			InitializeTree();			
			InitializeCategoryList();
		}		
		
		public void InitializeTree()
		{
			List<TreeColumnHelper> cols = new List<TreeColumnHelper>();
			
			cols.Add(new TreeColumnHelper(Catalog.GetString("Category"),0,"text",true));			
			
			TreeHelper helper = new TreeHelper(treeCategories,lstStore,cols.ToArray());
			helper.Initialize();
			treeCategories.SearchColumn = 0;
			
			treeCategories.RowActivated += delegate(object o, RowActivatedArgs args) {
				int index = Convert.ToInt32(args.Path.ToString());
				try
				{
					this.Category = dicSections.Values[index];
					if(CategorySelected!=null)
						CategorySelected(this,EventArgs.Empty);					
				}
				catch
				{
				}
				this.Destroy();
			};
		}
		
		public void InitializeCategoryList()
		{
			dicSections = new SortedList<string, string>();
			
			string categoryFilePath = config.CategoriesConfigurationFile;
			if(File.Exists(categoryFilePath))
			{
				StreamReader reader = new StreamReader(categoryFilePath);
				string line;
				while(!reader.EndOfStream)
				{
					line = reader.ReadLine();
					if(!line.StartsWith("#"))
					{
						if(line.Contains(","))
							dicSections.Add(line.Split(',')[0].Trim(),line.Split(',')[1].Trim());
					}
				}
				reader.Close();
				foreach(string key in dicSections.Keys)
					lstStore.AppendValues(key);
			}
		}
	}
}

