// 
//  AddAptRepositoryDialog.cs
//  
//  Author:
//       Jake Capangpangan <camicrisystems@gmail.com>
// 
//  Copyright (c) 2014 Camicri Systems
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
using System;
using Cube;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class AddAptRepositoryDialog : Gtk.Dialog
	{
		CubeAddAptRepositoryFetcher addApt;
		bool blnSuccess = false;
		string strError = null;
		
		public AddAptRepositoryDialog ()
		{
			this.Build ();			
			Initialize();			
			
			this.imgIcon.Pixbuf = CubeResources.LoadIcon("big","ppa");
		}
		
		public void Initialize()
		{
			btnCheck.Clicked += HandleBtnCheckClicked;
			this.ResizeMode = ResizeMode.Immediate;
			this.lblWait.Visible = false;		
		}		

		void HandleBtnCheckClicked (object sender, EventArgs e)
		{
			this.vboxCheck.Visible = false;
			this.lblWait.Visible = true;
			
			addApt = new CubeAddAptRepositoryFetcher();
			addApt.GetRepositoriesSuccess += HandleAddAptGetRepositoriesSuccess;
			addApt.GetRepositoriesFailed += HandleAddAptGetRepositoriesFailed;			
			addApt.StartRepositoryPPAFetch(entPPA.Text);
		}

		void HandleAddAptGetRepositoriesFailed (object sender, EventArgs e)
		{
			strError = addApt.ErrorMessage;
			Application.Invoke(delegate {
							
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,strError);
				md.Title = Catalog.GetString("Error");
				md.Run();
				md.Destroy();
				
				this.Destroy();
			});
		}

		void HandleAddAptGetRepositoriesSuccess (object sender, EventArgs e)
		{
			this.blnSuccess = true;
			this.Destroy();
		}		
		
		public bool Success
		{
			get { return this.blnSuccess; }
		}
		
		public string ErrorMessage
		{
			get { return this.strError; }
		}
		
		public CubeAddAptRepositoryFetcher Fetcher
		{
			get { return this.addApt; }
		}
	}
}