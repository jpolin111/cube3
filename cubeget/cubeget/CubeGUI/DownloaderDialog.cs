/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Gtk;
using System.Collections.Generic;
using System.Threading;
using Cube;
using Aria;
using Mono.Unix;

namespace cubeget
{
	public partial class DownloaderDialog : Gtk.Dialog
	{
		AriaDownloader aria;		
		CubeSource[] s;		
		CubePackage[] p;		
		
		public bool Success = false;		
		public bool StopAllDownloads = false;
		
		CubeConfiguration config = new CubeConfiguration();
		string strMessage;
		
		int intMode = -1;
		int intCount = 0;
		int intSourceStart = 0;
		
		public event EventHandler Finished;
		public event EventHandler ErrorOccured;
		
		List<CubePackage> lstFailedToDownloadPackages;
		List<CubeSource> lstFailedToDownloadSources;
		List<CubeReleaseGPG> lstReleases;		
		List<AriaData> lstData;
		
		public DownloaderDialog (string strMessage, string strInnerMessage)
		{
			this.Build ();
			this.Title = strMessage;
			this.strMessage = strInnerMessage;
			this.Shown += HandleShown;
			this.imgDownload.Pixbuf = CubeResources.LoadIcon("big","download");
			lblMessage.Text = strInnerMessage;
			
			lstFailedToDownloadPackages = new List<CubePackage>();
			lstFailedToDownloadSources = new List<CubeSource>();
			
			InitializeTree();			
			this.Show();
		}
		
		public void SetPackagesToDownload(CubePackage[] pks, string strDownloadDir)
		{						
			intMode = 1;			
			this.p = pks;
			
			lstData = new List<AriaData>();
			AriaData data;
			foreach(CubePackage p in pks)
			{
				data = new AriaData();					
				data.File_Path = strDownloadDir;				
				data.URL = p.GetValue("Filename");
				data.Is_Asynchronous = true;

				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.Application_Name = "aria2c.exe";
					//data.Application_Path = config.ThirdPartyDirectory + "aria2";					
				}	
				else
				{
					data.Application_Name = "aria2c";
					data.Application_Path = config.BinDirectory;
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","aria-parameters");
				string http_proxy = CubeSConfigurationManager.GetValue("config","aria-http");
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(http_proxy!=null)
					data.HTTP_Proxy = http_proxy;
				
				string size = ConvertSize(p.GetValue("Size"));
				
				lstrStatus.AppendValues(p.GetValue("Package"),0,size,"Queued");
				lstData.Add(data);
			}
			HandleShown(null,EventArgs.Empty);
		}
		
		public void SetSourcesToDownload(CubeSource[] s, CubeReleaseGPG[] r, string strDownloadDir)
		{						
			intMode = 2;
			this.s = s;			
			lstReleases = new List<CubeReleaseGPG>();
			lstData = new List<AriaData>();
			
			AriaData data;
			
			if(r!=null)
			{
			//Releases
			foreach(CubeReleaseGPG rel in r)
			{
				//Release Link				
				data = new AriaData();				
				data.File_Path = strDownloadDir;
				data.File_Name = rel.ReleaseFilename;
				data.URL = rel.ReleaseLink;
				data.Delete_Existing = true;
				data.Is_Asynchronous = true;
				
				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.Application_Name = "aria2c.exe";
					//data.Application_Path = config.ThirdPartyDirectory + "aria2";
				}
				else
				{
					data.Application_Name = "aria2c";
					data.Application_Path = config.BinDirectory;
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","aria-parameters");
				string http_proxy = CubeSConfigurationManager.GetValue("config","aria-http");
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(http_proxy!=null)
					data.HTTP_Proxy = http_proxy;
				
				lstrStatus.AppendValues(rel.ReleaseLink,0,"","Queued");
				lstReleases.Add(rel);
				lstData.Add (data);				
							
				/*Release GPG Link
				data = new AriaData();				
				data.File_Path = strDownloadDir;
				data.File_Name = rel.ReleaseGPGFilename;
				data.URL = rel.ReleaseGPGLink;
				data.Delete_Existing = true;
				data.Is_Asynchronous = true;
				
				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.Application_Name = "aria2c.exe";
					//data.Application_Path = config.ThirdPartyDirectory + "aria2";
				}
				else
				{
					data.Application_Name = "aria2c";
					data.Application_Path = config.BinDirectory;
				}
								
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(http_proxy!=null)
					data.HTTP_Proxy = http_proxy;
								
				lstrStatus.AppendValues(rel.ReleaseGPGLink,0,"","Queued");
				lstReleases.Add(rel);
				lstData.Add (data);
				*/
			}			
			}
			intSourceStart = lstData.Count;
			
			//Sources
			//-1 to separate "status" source list (Expected always at end)
			for(int i=0;i<s.Length-1;i++)
			{				
				//Source Link
				data = new AriaData();				
				data.File_Path = strDownloadDir;
				data.File_Name = s[i].Filename;
				data.URL = s[i].Link;
				data.Delete_Existing = true;
				data.Is_Asynchronous = true;

				if(!config.IsAtLinuxSystem)
				{
					//Comment : To be redirected to PATH environment
					//data.Application_Name = "aria2c.exe";
					//data.Application_Path = config.ThirdPartyDirectory + "aria2";
				}
				else
				{
					data.Application_Name = "aria2c";
					data.Application_Path = config.BinDirectory;				
				}
				
				string extra_params = CubeSConfigurationManager.GetValue("config","aria-parameters");
				string http_proxy = CubeSConfigurationManager.GetValue("config","aria-http");
				if(extra_params!=null)
					data.Arguments = extra_params;
				if(http_proxy!=null)
					data.HTTP_Proxy = http_proxy;
				
				lstrStatus.AppendValues(s[i].Link,0,"","Queued");
				lstData.Add(data);
			}
			HandleShown(null,EventArgs.Empty);
		}
		
		ListStore lstrStatus = new ListStore(typeof(string),typeof(int),typeof(string),typeof(string));
		
		public void InitializeTree()
		{
			List<TreeColumnHelper> lst = new List<TreeColumnHelper>();
			lst.Add(new TreeColumnHelper(Catalog.GetString("Package"),0,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Progress"),1,"progress",true));			
			lst.Add(new TreeColumnHelper(Catalog.GetString("Size"),2,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Status"),3,"text",true));			
			TreeHelper helper = new TreeHelper(treeDownload,lstrStatus,lst.ToArray());
			helper.Initialize();
		}
		
		public void HandleShown (object sender, EventArgs e)
		{						
			if(StopAllDownloads)
				return;
			Console.WriteLine(Catalog.GetString("Starting download..."));
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);
			lstrStatus.SetValue(iter,3,Catalog.GetString("Downloading"));
			Console.WriteLine("Starting download "+(lstData[intCount]).File_Name);
			aria = new AriaDownloader(lstData[intCount]);
			aria.ProgressChanged += HandleProgressChanged;
			aria.Finished += HandleFinished;
			aria.ErrorOccured += HandleErrorOccured;	
			aria.StartDownload();
		}

		void HandleErrorOccured (object sender, EventArgs e)
		{
			Application.Invoke(delegate{
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);
			lstrStatus.SetValue(iter,3,Catalog.GetString("Failed"));
			Console.WriteLine("Error occured!" + aria.Error_Message);			
				
			try{
				System.IO.File.Delete(lstData[intCount].Full_File_Path);
			}catch{}
							
			if(intMode == 1)
			{
				if(ErrorOccured!=null)
					ErrorOccured(p[intCount],EventArgs.Empty);
				lstFailedToDownloadPackages.Add(p[intCount]);
			}
			else if(intMode == 2)
			{
				if(ErrorOccured!=null)
				{
					if(lstData[intCount].File_Name.EndsWith("Release") || lstData[intCount].File_Name.EndsWith(".gpg"))
						ErrorOccured(lstReleases[intCount],EventArgs.Empty);
					else
					{
						ErrorOccured(s[intCount - intSourceStart],EventArgs.Empty);
						lstFailedToDownloadSources.Add(s[intCount - intSourceStart]);
					}
				}				
			}
				
			intCount++;			
				prgProgress.Fraction = (float)intCount/(float)lstData.Count;
			
			if(intCount<lstData.Count)
			{
				HandleShown(null,EventArgs.Empty);
			}
			else
				buttonCancel.Activate();
			});
		}

		void HandleFinished (object sender, EventArgs e)
		{
			Application.Invoke(delegate{
			Gtk.TreeIter iter;			
			lstrStatus.IterNthChild(out iter,intCount);
			lstrStatus.SetValue(iter,3,Catalog.GetString("Done"));
			
			if(Finished!=null)
			{
				if(intMode == 1)
					Finished(p[intCount],EventArgs.Empty);
				else if(intMode == 2)
				{
					if(lstData[intCount].File_Name.EndsWith("Release") || lstData[intCount].File_Name.EndsWith(".gpg"))
					{						
						Finished(lstReleases[intCount],EventArgs.Empty);
					}
					else
					{
						Console.WriteLine("Finished download {0}",s[intCount - intSourceStart].Filename);
						Finished(s[intCount - intSourceStart],EventArgs.Empty);
					}
				}
			}
				
			intCount++;
				
			lblMessage.Markup = strMessage + " <i>("+intCount+" of "+lstData.Count+")</i>";
				
				prgProgress.Fraction = (float)intCount/(float)lstData.Count;
			
			if(intCount<lstData.Count)
			{
				HandleShown(null,EventArgs.Empty);
			}
			else
			{
				Success = true;
				buttonCancel.Activate();				
			}
			});
		}

		void HandleProgressChanged (object sender, EventArgs e)
		{			
			Application.Invoke(delegate{
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);			
			lstrStatus.SetValue(iter,1,aria.Progress);			
			QueueDraw();
			});
		}	
		
		string ConvertSize(string size)
		{
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
		
		public void StopAllDownload()
		{
			StopAllDownloads = true;
			aria.Kill();
		}
		
		public AriaDownloader Aria
		{
			get { return aria; }
		}
		
		public CubePackage[] PackagesFailedToDownload
		{
			get { return this.lstFailedToDownloadPackages.ToArray(); }
		}
		
		public CubeSource[] SourcesFailedToDownload
		{
			get { return this.lstFailedToDownloadSources.ToArray(); }
		}

		protected void OnTreeDownloadAdded (object o, Gtk.AddedArgs args)
		{
			throw new System.NotImplementedException ();
		}
	}
}

