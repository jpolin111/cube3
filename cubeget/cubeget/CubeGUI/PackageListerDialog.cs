/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Cube;
using System.Collections.Generic;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class PackageListerDialog : Gtk.Dialog
	{
		CubePackage[] packages;
		bool blnDownload = true;
		
		public PackageListerDialog (CubePackage[] pckToDownload,bool blnDownload,string title,string strMessage,string type)
		{			
			this.Build ();			
			this.lblMessage.Markup = strMessage;
			this.packages = pckToDownload;
			this.blnDownload = blnDownload;
			this.Title = title;
			Initialize();
			
			if(type == "list-question")
				this.image727.Pixbuf = CubeResources.LoadIcon("big","dialog-question");
			else if(type == "list-information")
			{
				this.image727.Pixbuf = CubeResources.LoadIcon("big","dialog-information");
				buttonCancel.Visible = false;
			}
			else if(type == "list-error")
			{
				this.image727.Pixbuf = CubeResources.LoadIcon("big","dialog-error");
				buttonCancel.Visible = false;
				lblSizeMessage.Visible = false;				
			}
			else if(type == "list-error-cancel")
			{
				this.image727.Pixbuf = CubeResources.LoadIcon("big","dialog-error");
				buttonCancel.Visible = true;
				lblSizeMessage.Visible = false;
			}
			
			AddToTree();			
		}
		
		ListStore lstrPackage = new ListStore(typeof(string),typeof(string),typeof(string));
		
		private void Initialize()
		{
			List<TreeColumnHelper> lst = new List<TreeColumnHelper>();
			lst.Add(new TreeColumnHelper(Catalog.GetString("Package"),0,"text",true));			
			lst.Add(new TreeColumnHelper(Catalog.GetString("Size"),1,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Version"),2,"text",true));
			
			
			TreeHelper helper = new TreeHelper(treePackages,lstrPackage,lst.ToArray());
			treePackages = helper.Initialize();
			ShowAll();
		}
		
		private void AddToTree()
		{
			double total = 0.0;
			foreach(CubePackage p in packages)
			{
				string size = "";
				if(blnDownload)
					size = p.GetValue("Size");
				else
					size = p.GetValue("Installed-Size");
				lstrPackage.AppendValues(p.GetValue("Package"),ConvertSize(size),p.GetValue("Version"));
				total += Convert.ToDouble(size);				
			}
			string strTotal = total.ToString();
			if(strTotal.Contains("."))
				strTotal = strTotal.Split('.')[0].Trim();
			if(blnDownload)
				lblSizeMessage.Text = string.Format(Catalog.GetString("A total of {0} space will be used"),ConvertSize(strTotal.Trim()));
			else
				lblSizeMessage.Text = "";
		}
		
		string ConvertSize(string size)
		{
			if(size==null)
				return "undefined";
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
		
		public Button ButtonOK
		{
			get { return buttonOk; }
			set { buttonOk = value; }
		}
	}
}

