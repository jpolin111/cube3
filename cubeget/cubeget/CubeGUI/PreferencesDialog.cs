/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using System.IO;
using System.Collections.Generic;
using Cube;

namespace cubeget
{
	public partial class PreferencesDialog : Gtk.Dialog
	{
		public PreferencesDialog ()
		{			
			this.Build ();
			this.buttonOk.Clicked += HandleClicked;	
			
			LoadCurrentConfiguration();
		}
		
		List<string> lstThemes;
		public void LoadCurrentConfiguration()
		{
			string gtk_theme = CubeSConfigurationManager.GetValue("config","gtk-theme");			
			lstThemes = new List<string>(Directory.GetDirectories(new CubeConfiguration().ThemesDirectory));
			
			for(int i = 0 ; i < lstThemes.Count; i++)
			{
				lstThemes[i] = new DirectoryInfo(lstThemes[i]).Name;
				cboGTKTheme.InsertText(i,lstThemes[i]);
			}
			int index = lstThemes.IndexOf(gtk_theme);
			if(index!=-1)
				cboGTKTheme.Active = index;
			else
				cboGTKTheme.Active = 0;
			
			CubeConfigurationManager cmgr = new CubeConfigurationManager(new CubeConfiguration().ConfigurationDirectory);
			
			if(!cmgr.OpenConfiguration("config"))
				return;
			
			string defaultIns = cmgr.GetValue("default-installer");
			string dpkgParams = cmgr.GetValue("dpkg-parameters");
			string apt_getParams = cmgr.GetValue("apt-get-parameters");
			string defaultDl = cmgr.GetValue("default-downloader");
			string ariaParams = cmgr.GetValue("aria-parameters");
			string aria_http = cmgr.GetValue("aria-http");
			string axelParams = cmgr.GetValue("axel-parameters");
			string axel_con = cmgr.GetValue("axel-connections");
			string enable_reverse_dep = cmgr.GetValue("enable-reverse-dependency-check");
			
			if(defaultIns!=null)
			{
				if(defaultIns == "dpkg")
					radDPKG.Active = true;
				else
					radAPT.Active = true;
			}
			
			if(dpkgParams!=null) entDPKGParams.Text = dpkgParams;
			if(apt_getParams!=null) entAPTGETParams.Text = apt_getParams;
			if(defaultDl!=null)
			{
				if(defaultDl == "aria")
					radAria.Active = true;
				else
					radAxel.Active = true;
			}
			if(ariaParams!=null) entAriaParams.Text = ariaParams;
			if(aria_http!=null) entAriaHTTP.Text = aria_http;
			if(axelParams!=null) entAxelParams.Text = axelParams;
			if(axel_con!=null) spinAxelMaxCon.Value = double.Parse(axel_con);
			if(enable_reverse_dep!=null) {
				if(enable_reverse_dep == "true")
					chkReverseDep.Active = true;
				else
					chkReverseDep.Active = false;
			}
		}

		void HandleClicked (object sender, EventArgs e)
		{
			CubeConfiguration config = new CubeConfiguration();
			CubeConfigurationManager cmgr = new CubeConfigurationManager(config.ConfigurationDirectory);
			
			if(!cmgr.OpenConfiguration("config"))
				cmgr.CreateConfiguration("config");
			if(!cmgr.OpenConfiguration("config"))
				return;			
			
			if(radAPT.Active)
				cmgr.AddValue("default-installer","apt-get",true);
			else if(radDPKG.Active)
				cmgr.AddValue("default-installer","dpkg",true);
			
			if(entDPKGParams.Text.Length>0)
				cmgr.AddValue("dpkg-parameters",entDPKGParams.Text,true);
			if(entAPTGETParams.Text.Length>0)
				cmgr.AddValue("apt-get-parameters",entAPTGETParams.Text,true);
			
			if(radAria.Active)
				cmgr.AddValue("default-downloader","aria",true);
			else if(radAxel.Active)
				cmgr.AddValue("default-downloader","axel",true);
			
			if(entAriaParams.Text.Length>0)
				cmgr.AddValue("aria-parameters",entAriaParams.Text,true);
			if(entAriaHTTP.Text.Length>0)
				cmgr.AddValue("aria-http",entAriaHTTP.Text,true);
			
			if(entAxelParams.Text.Length>0)
				cmgr.AddValue("axel-parameters",entAxelParams.Text,true);
			
			if(chkReverseDep.Active)
				cmgr.AddValue("enable-reverse-dependency-check","true",true);
			else
				cmgr.AddValue("enable-reverse-dependency-check","false",true);
			
			cmgr.AddValue("axel-connections",spinAxelMaxCon.Value+"",true);
						
			cmgr.AddValue("gtk-theme",cboGTKTheme.ActiveText,true);
			cmgr.Save();
		}
	}
}