/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Cube;
using Gtk;
using Mono.Unix;
using System.IO;

namespace cubeget
{
	public partial class InstallationDialog : Gtk.Dialog
	{
		CubePackage[] packages;
		public bool Success = false;
		public int Result = 0;
		string packageDir;		
		
		public InstallationDialog (CubePackage[] packages,string packageDir)
		{
			this.Build ();
			this.imgInstall.Pixbuf = CubeResources.LoadIcon("big","install");
			this.packages = packages;
			this.packageDir = packageDir;			
		}
		
		public void Start()
		{
			string default_ins = CubeSConfigurationManager.GetValue("config","default-installer");
			
			if(HasRepacked()) //If selected packages contains repacked package, Use DPKG as installer, since apt-get always rejects repacked packages.
				Start_DPKG();
			else
			{
				if(default_ins == "dpkg")
					Start_DPKG();
				else
					Start_APT_GET();
			}				
		}
		
		public bool HasRepacked()
		{
			foreach(CubePackage p in packages)
			{
				if(p.PackageDictionary.ContainsKey("Repacked"))
					return true;
			}
			return false;
		}
		
		public void Start_DPKG()
		{
			this.Title = Catalog.GetString("Using DPKG for installation");
			
			CubeDPKGInstaller installer = new CubeDPKGInstaller(packages,packageDir);
			installer.StartInstallation();			
						
			installer.Finished += (sender, e) => {
				Application.Invoke(delegate{					
					if(installer.Success)
						Success = true;
					else
					{	
						MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,installer.ErrorMessage);
						md.Title = Catalog.GetString("Error occured");
						md.Run();
						md.Destroy();
						if(installer.ErrorType == 1 && File.Exists(installer.OutputFilePath)) //If installation problem
						{							
							ShowTextDialog showText = new ShowTextDialog(File.ReadAllText(installer.OutputFilePath));
							showText.Title = "Installation Output";
							showText.Run();
							showText.Destroy();
						}
					}
					buttonCancel.Activate();
				});
			};
		}
		
		public void Start_APT_GET()
		{					
			this.Title = Catalog.GetString("Using APT-GET for installation");			
			
			CubeAPTInstaller installerApt = new CubeAPTInstaller(packages,packageDir);
			installerApt.StartInstallation();
						
			installerApt.Finished += (sender, e) => {
				Application.Invoke(delegate{					
					if(installerApt.Success)
						Success = true;
					else
					{	
						MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,installerApt.ErrorMessage);
						md.Title = Catalog.GetString("Error occured");
						md.Run();
						md.Destroy();
						if(installerApt.ErrorType == 1 && File.Exists(installerApt.OutputFilePath)) //If installation problem
						{							
							ShowTextDialog showText = new ShowTextDialog(File.ReadAllText(installerApt.OutputFilePath));
							showText.Title = "Installation Output";
							showText.Run();
							showText.Destroy();
						}
					}
					buttonCancel.Activate();
				});
			};
			
			this.Visible = false;
		}
	}
}