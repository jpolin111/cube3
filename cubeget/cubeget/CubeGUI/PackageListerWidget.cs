/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using System.Collections.Generic;
using Cube;
using Gtk;
using System.Threading;
using Mono.Unix;

namespace cubeget
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class PackageListerWidget : Gtk.Bin
	{
		
		public event EventHandler PackageSelected;
		public event EventHandler PackageLoadingStarted;
		public event EventHandler PackageLoadingFinished;
		
		public event EventHandler RequestForInstalledPackages;
		public event EventHandler RequestForDownloadedPackages;
		public event EventHandler RequestForBrokenPackages;
		public event EventHandler RequestForUpgradablePackages;
		public event EventHandler RequestForCategoryFilter;
		public event EventHandler RequestForAllPackages;
		
		public PackageListerWidget ()
		{
			this.Build ();
						
			this.imgbtnInstalled.Pixbuf = CubeResources.LoadIcon("small","package-install");
			this.imgbtnBroken.Pixbuf = CubeResources.LoadIcon("small","package-purge");
			this.imgbtnCategory.Pixbuf = CubeResources.LoadIcon("small","edit-find");
			this.imgbtnDownloaded.Pixbuf = CubeResources.LoadIcon("small","package-downloaded");
			this.imgbtnUpgradable.Pixbuf = CubeResources.LoadIcon("small","package-upgrade");			
			
			/*
			this.btnInstalled.Image = new Image(CubeResources.LoadIcon("small","package-install"));
			this.btnBroken.Image = new Image(CubeResources.LoadIcon("small","package-purge"));
			this.btnDownloaded.Image = new Image(CubeResources.LoadIcon("small","package-downloaded"));
			this.btnUpgradable.Image = new Image(CubeResources.LoadIcon("small","package-upgrade"));
			this.btnCategory.Image = new Image(CubeResources.LoadIcon("small","edit-find"));
			
			this.btnInstalled.Label = Catalog.GetString("Installed");
			this.btnDownloaded.Label = Catalog.GetString("Downloaded");
			this.btnBroken.Label = Catalog.GetString("Broken");
			this.btnUpgradable.Label = Catalog.GetString("Upgradable");
			this.btnCategory.Label = Catalog.GetString("Category");
			*/
		}
		
		ListStore lstStore = new ListStore(typeof(string),typeof(Gdk.Pixbuf),typeof(string),typeof(string));
		
		public void InitializeTree()
		{
			lstStore = new ListStore(typeof(string),typeof(Gdk.Pixbuf),typeof(string),typeof(string));
			List<TreeColumnHelper> cols = new List<TreeColumnHelper>();
			
			cols.Add(new TreeColumnHelper("#",0,"text",true));
			cols.Add(new TreeColumnHelper("S",1,"pixbuf",true));
			cols.Add(new TreeColumnHelper(Catalog.GetString("Package"),2,"text",true));
			cols.Add(new TreeColumnHelper(Catalog.GetString("Description"),3,"text",true));
			
			TreeHelper helper = new TreeHelper(treePackages,lstStore,cols.ToArray());
			helper.Initialize();
			treePackages.SearchColumn = 2;
			
			btnInstalled.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForInstalledPackages!=null)
					RequestForInstalledPackages(this,EventArgs.Empty);
			};
			
			btnDownloaded.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForDownloadedPackages!=null)
					RequestForDownloadedPackages(this,EventArgs.Empty);
			};
			
			btnUpgradable.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForUpgradablePackages!=null)
					RequestForUpgradablePackages(this,EventArgs.Empty);
			};
			
			btnBroken.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForBrokenPackages!=null)
					RequestForBrokenPackages(this,EventArgs.Empty);
			};			
			
			btnCategory.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForCategoryFilter!=null)
					RequestForCategoryFilter(this,EventArgs.Empty);
			};
			
			btnAll.Clicked += delegate(object sender, EventArgs e) {
				if(RequestForAllPackages!=null)
					RequestForAllPackages(this,EventArgs.Empty);
			};
		}
		
		public void LoadPackages_Start(List<CubePackage> lstPackages)
		{
			lstStore.Clear();
						
			if(PackageLoadingStarted!=null)
				PackageLoadingStarted(this,EventArgs.Empty);
						
			treePackages.RowActivated += delegate(object o, RowActivatedArgs args) {
				int index = Convert.ToInt32(args.Path.ToString());
				try
				{
					if(PackageSelected!=null)					
						PackageSelected(lstPackages[index],EventArgs.Empty);
				}
				catch
				{
				}					
			};
			
			Thread t = new Thread(LoadPackages_Thread);
			t.Start(lstPackages);
						
			lblLabel.Markup = string.Format(Catalog.GetString("<b>Listing packages</b> <i>{0} package(s) listed</i>"),lstPackages.Count);
		}
		
		public void LoadPackages_Thread(object list)
		{
			List<CubePackage> lstPackages = list as List<CubePackage>;
			int intCount = 1;
			Application.Invoke(delegate{
				foreach(CubePackage p in lstPackages)
				{
					string status = p.GetValue("Status");
					Gdk.Pixbuf statusImage = CubeResources.LoadIcon("small","package-available");
					
					if(status == "0")
						statusImage = CubeResources.LoadIcon("small","package-installed-updated");
					else if(status == "-1")
						statusImage = CubeResources.LoadIcon("small","package-upgrade");
					else if(status == "-2")
					{
						if(p.PackageDictionary.ContainsKey("Repacked"))
							statusImage = CubeResources.LoadIcon("small","package-repacked");
						else
							statusImage = CubeResources.LoadIcon("small","package-downloaded");
					}
					else if(status == "-3")
						statusImage = CubeResources.LoadIcon("small","package-broken");
													
					lstStore.AppendValues(intCount+"",statusImage,p.GetValue("Package"),p.GetValue("Description"));				
					intCount++;
				}	
			});
						
			LoadPackages_Finished();
		}
		
		public void LoadPackages_Finished()
		{
			if(PackageLoadingFinished !=null)
				PackageLoadingFinished(this,EventArgs.Empty);
		}
	}
}