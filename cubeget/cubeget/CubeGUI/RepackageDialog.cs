// 
//  RepackageDialog.cs
//  
//  Author:
//       camicri <camicrisystems@gmail.com>
// 
//  Copyright (c) 2014 Camicri Systems
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
using System;
using Cube;
using Gtk;
using System.Collections.Generic;
using System.Threading;
using Mono.Unix;
using System.IO;

namespace cubeget
{
	public partial class RepackageDialog : Gtk.Dialog
	{
		List<CubePackage> lstpks = new List<CubePackage>();
		ListStore lstrStatus = new ListStore(typeof(string),typeof(string));
		string message = "Repacking selected packages...";
		string dir = "";
		int intCount = 0;		
				
		CubeDebRecover debrec = new CubeDebRecover();
		CubeProject proj;
		
		Thread t;
		
		public RepackageDialog (CubePackage[] pks, CubeProject proj)
		{
			
			this.Build ();
			this.lstpks = new List<CubePackage>(pks);
			this.imgRepack.Pixbuf = CubeResources.LoadIcon("big","repacked");
			this.lblMessage.Text = this.message;
			this.buttonCancel.Activated += HandleDestroyed;
			this.proj = proj;
			
			this.dir = Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + proj.SharedPackagesDirectory;
			
			InitializeTree();
			InitializeList();
			
			this.Destroyed += HandleDestroyed;
			this.Show();
		}

		void HandleDestroyed (object sender, EventArgs e)
		{
			if(t!=null)
			{
				if(t.IsAlive)
					t.Abort();
			}
			intCount = lstpks.Count;
		}		
		
		public void InitializeTree()
		{
			List<TreeColumnHelper> lst = new List<TreeColumnHelper>();
			lst.Add(new TreeColumnHelper(Catalog.GetString("Package"),0,"text",true));
			lst.Add(new TreeColumnHelper(Catalog.GetString("Status"),1,"text",true));
			TreeHelper helper = new TreeHelper(treeRepack,lstrStatus,lst.ToArray());			
			helper.Initialize();
		}
		
		public void InitializeList()
		{
			foreach(CubePackage p in lstpks)
				lstrStatus.AppendValues(p.GetValue("Package"),"Queued");
		}
		
		public void Start()
		{
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);			
			lstrStatus.SetValue(iter,1,Catalog.GetString("Repacking"));
			lblMessage.Text = message + " (" + (intCount + 1) + " of " + lstpks.Count + ")";
			
			t = new Thread(Repack_Thread);
			t.Start(lstpks[intCount]);			
		}
		
		public void Repack_Thread(object p1)
		{
			CubePackage p = (CubePackage)p1;			
			
			bool res = debrec.RecoverDeb(p,dir);
			Application.Invoke(delegate{
				Repack_Finished(res);
			});
		}
		
		public void Repack_Finished(bool res)
		{		
			Gtk.TreeIter iter;
			lstrStatus.IterNthChild(out iter,intCount);
			
			if(res)
				lstrStatus.SetValue(iter,1,Catalog.GetString("Done"));
			else
				lstrStatus.SetValue(iter,1,Catalog.GetString("Failed"));
			intCount++;
						
			prgProgress.Fraction = (double)intCount / (double)lstpks.Count;
			this.ShowAll();
			
			if(intCount < lstpks.Count)
				Start();
			else
				Stop ();
		}
		
		public void Stop()
		{			
			intCount = lstpks.Count;
			this.buttonCancel.Activate();
		}
	}
}