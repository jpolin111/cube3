/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using Cube;
using System.IO;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class ModifyRepositoryDialog : Gtk.Dialog
	{
		CubeProject p;
		bool saved = false;
		public ModifyRepositoryDialog (CubeProject p)
		{
			this.Build ();			
	
			this.buttonOk.Clicked += HandleButtonOkhandleClicked;
			this.image9.Pixbuf = Cube.CubeResources.LoadIcon("big","software-properties");
			
			this.p = p;
			LoadText();
		}

		void HandleButtonOkhandleClicked (object sender, EventArgs e)
		{
			MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,Catalog.GetString("Save changes?"));
			if((ResponseType)md.Run() == ResponseType.Yes)
			{
				StreamWriter writer = new StreamWriter(p.AptDirectory+"sources.list");
				writer.Write(this.txtSourcesFile.Buffer.Text);
				writer.Close();
				
				saved = true;
			}
			md.Destroy();			
			
			this.Destroy();
		}		
		
		public void LoadText()
		{			
			StreamReader reader = new StreamReader(p.AptDirectory+"sources.list");
			string text = reader.ReadToEnd();
			this.txtSourcesFile.Buffer.Text = text;			
			reader.Close();
		}
		
		public string Buffer
		{
			get { return this.txtSourcesFile.Buffer.Text;}
		}	
		
		public bool Saved
		{
			get { return saved;}
		}
	}
}

