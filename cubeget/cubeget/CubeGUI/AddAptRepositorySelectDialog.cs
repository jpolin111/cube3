// 
//  AddAptRepositorySelectDialog.cs
//  
//  Author:
//       Jake Capangpangan <camicrisystems@gmail.com>
// 
//  Copyright (c) 2014 Camicri Systems
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
using System;
using Cube;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class AddAptRepositorySelectDialog : Gtk.Dialog
	{
		CubeProcess procMgr;
		CubeAddAptRepositoryFetcher addApt = new CubeAddAptRepositoryFetcher();
		string strError = null;
		string strDebAddress = null;
		string codename = null;
		bool blnSuccess = false;
		
		public AddAptRepositorySelectDialog (CubeProcess procMgr, CubeAddAptRepositoryFetcher addApt)
		{
			this.Build ();
		 	this.addApt = addApt;
			this.procMgr = procMgr;
			
			this.btnAddApt.Clicked += HandleBtnAddAptClicked;
			this.imgIcon.Pixbuf = CubeResources.LoadIcon("big","ppa");
			InitializeFetchedRepository();
		}
		
		void HandleBtnAddAptClicked (object sender, EventArgs e)
		{
			string text = cboSeries.ActiveText;			
			codename = text;
			strDebAddress = String.Format(addApt.DebAddressFormat,cboSeries.ActiveText);
			this.blnSuccess = true;
			this.Destroy();
		}
		
		public void InitializeFetchedRepository()
		{
			if(addApt.Series.Length > 0 && addApt.DebAddressFormat != null && addApt.Title !=null)
			{						 
				string text = String.Format(Catalog.GetString("The PPA <b>{0}</b> is from the <b>{1}</b> "),addApt.PPA,addApt.Title);
				text += Catalog.GetString("with repository address of <b>") + addApt.DebAddressFormat + "</b>\n\n";
				
				foreach(string code in addApt.Series)						
					cboSeries.AppendText(code);
				cboSeries.Active = 0;
				
				if(procMgr.Project.CodeName != null)
				{
					int count = 0;
					foreach(string code in addApt.Series)
					{
						if(procMgr.Project.CodeName == code)
						{
							this.codename = code;
							cboSeries.Active = count;
							break;
						}
						count++;
					}
				}
					
				if(codename!=null)
					text += String.Format(Catalog.GetString("Your project series is <b>{0}</b>. You can now add it in your repository list"),codename);
				else
				{
					if(procMgr.Project.CodeName != null)
						text += String.Format(Catalog.GetString("Your project series is <b>{0}</b>. It is not included in the supported series, please select one or cancel if you are not sure."),procMgr.Project.CodeName);
					else
						text += Catalog.GetString("Unable to detect project series. Please select one or cancel if you are not sure.");
				}
					
				lblAddAptDesc.Markup = text;				
			}
			else
			{				
				strError = addApt.ErrorMessage;
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,"Not all vital information from ppa were fetched.");
				md.Title = Catalog.GetString("Insufficient details fetched");
				this.Destroy();		
			}
		}
		
		public string CodeName
		{
			get { return this.codename;}
		}
		
		public string AptDebAddress
		{
			get { return this.strDebAddress; }
		}
		
		public bool Success
		{
			get { return this.blnSuccess; }
		}
		
		public string ErrorMessage
		{
			get { return this.strError; }
		}
	}	
}