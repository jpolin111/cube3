/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Cube;
using System.Collections.Generic;
using Gtk;
using Aria;
using System.IO;
using System.Drawing;
using Accessibility;
using Mono.Unix;
using Axel;

namespace cubeget
{	
	[System.ComponentModel.ToolboxItem(true)]
	public partial class PackageInformationWidget : Gtk.Bin
	{
		public event EventHandler MarkPackage;
		public event EventHandler MarkPackageToInstall;
		public event EventHandler UnmarkPackage;
		public event EventHandler UnmarkPackageToInstall;
		public event EventHandler DownloadPackage;
		public event EventHandler InstallPackage;
		public event EventHandler AppSharePackage;
		public event EventHandler Verify;
		
		AriaDownloader ariaDownloader;
		AxelDownloader axelDownloader;
		CubePackage p;
		CubeProcess proc;
			
		public PackageInformationWidget (CubeProcess proc)
		{
			this.Build ();
			this.proc = proc;
			CheckForRootRestrictions();
		}		
		
		public void InitializeEvents(CubePackage p,List<CubePackage> pcksMarked,List<CubePackage> pcksMarkedInstall)
		{						
			this.p = p;
			btnDownloadThis.Visible = false;
			btnMarkThis.Visible = false;
			btnVerify.Visible = false;
			
			if(p.GetValue("Status") != "0" && p.GetValue("Status")!="-2")
			{
				btnMarkThis.Visible = true;
				btnDownloadThis.Visible = true;
				
				if(!pcksMarked.Contains(p) && MarkPackage!=null)
				{
					btnMarkThis.Clicked += (sender, e) => MarkPackage(p,EventArgs.Empty);					
				}
				else if(UnmarkPackage!=null)
				{
					btnMarkThis.Label = Catalog.GetString("Unmark this");
					btnMarkThis.Clicked += (sender, e) => UnmarkPackage(p,EventArgs.Empty);					
				}
				
				if(DownloadPackage!=null)
					btnDownloadThis.Clicked += (sender, e) => DownloadPackage(p,EventArgs.Empty);
			}
			else if(p.GetValue("Status") == "-2")
			{
				btnMarkThis.Visible = true;
				btnMarkThis.Label = Catalog.GetString("Mark this");
				btnDownloadThis.Visible = true;
				
				btnVerify.Visible = true;
				btnVerify.Clicked += (sender, e) => Verify(p,EventArgs.Empty);
				
				btnDownloadThis.Label = Catalog.GetString("Install this");
				btnDownloadThis.Sensitive = true;
				
				if(InstallPackage!=null)
					btnDownloadThis.Clicked += (sender, e) => InstallPackage(p,EventArgs.Empty);
				if(!pcksMarkedInstall.Contains(p) && MarkPackageToInstall!=null)
				{
					btnMarkThis.Label = Catalog.GetString("Mark this");
					btnMarkThis.Clicked += (sender, e) => MarkPackageToInstall(p,EventArgs.Empty);
				}
				else if(UnmarkPackageToInstall!=null)
				{
					btnMarkThis.Label = Catalog.GetString("Unmark this");
					btnMarkThis.Clicked += (sender, e) => UnmarkPackageToInstall(p,EventArgs.Empty);
				}
			}
			
			btnAppShare.Clicked += (sender,e) => AppSharePackage(p,EventArgs.Empty);
			
			if(!proc.Configuration.IsAtLinuxSystem)
				btnAppShare.Sensitive = false;
			if(!proc.HostAndProjectCompatible)				
				btnAppShare.Sensitive = false;
			if(p.PackageDictionary.ContainsKey("Status"))
				if(p.GetValue("Status") == "0")
					btnAppShare.Sensitive = false;
		}
		
		public void Initialize(CubePackage p, List<CubePackage> pcksMarked,List<CubePackage> pcksMarkedInstall)
		{												
			lblPackageName.Markup = "<b>"+p.GetValue("Package").ToUpperInvariant()+"</b> ("+p.GetValue("Version")+") "+ConvertSize(p.GetValue("Size"));
			lblPackageDesk.Markup = "<i>"+p.GetValue("Description")+"</i>";
			
			ListStore lstStore = new ListStore(typeof(string),typeof(string));
			List<TreeColumnHelper> cols = new List<TreeColumnHelper>();
			cols.Add(new TreeColumnHelper(Catalog.GetString("Category"),0,"text",true));
			cols.Add(new TreeColumnHelper(Catalog.GetString("Value"),1,"text",true));			
			
			TreeHelper treeHelper = new TreeHelper(treeInfo,lstStore,cols.ToArray());
			treeHelper.Initialize();
			
			foreach(string key in p.PackageDictionary.Keys)
			{
				lstStore.AppendValues(key,p.GetValue(key));
			}						
						
			if(p.GetValue("Status") == "0")
			{								
				imgStatus.Pixbuf = CubeResources.LoadIcon("big","package-install");
				lblStatus.Markup = Catalog.GetString("<i>Installed</i>");
			}
			else if(p.PackageDictionary.ContainsKey("Broken"))
			{
				imgStatus.Pixbuf = CubeResources.LoadIcon("big","package-broken");
				lblStatus.Markup = Catalog.GetString("<b>Broken</b>");		
			}
			else if(p.GetValue("Status") == "-1")
			{				
				if(p.PackageDictionary.ContainsKey("Host Version"))
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","host-available");
					lblStatus.Markup = Catalog.GetString("<i>Needs Update\nAvailable to this computer</i>");
				}
				else
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","package-upgrade");
					lblStatus.Markup = Catalog.GetString("<i>Needs update</i>");
				}
			}
			else if(p.GetValue("Status") == "-2")
			{
				if(p.PackageDictionary.ContainsKey("Repacked"))
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","repacked");
					lblStatus.Markup = Catalog.GetString("<i>Repacked</i>");
					btnAppShare.Sensitive = true;
				}
				else
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","download");
					lblStatus.Markup = Catalog.GetString("<i>Downloaded</i>");
				}
			}
			else
			{
				if(p.PackageDictionary.ContainsKey("Host Version"))
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","host-available");
					lblStatus.Markup = Catalog.GetString("<i>Available to this computer</i>");
				}
				else
				{
					imgStatus.Pixbuf = CubeResources.LoadIcon("big","package-available");
					lblStatus.Markup = Catalog.GetString("<i>Available</i>");
				}
			}			
						
			btnViewScreenShot.Clicked += HandleClicked;
						
			try
			{				
				string img = proc.Project.ScreenshotsDirectory + p.GetValue("Package");
				if(File.Exists(img))
				{
					imgScreenShot.Pixbuf = new Gdk.Pixbuf(img,200,200,true);
					btnViewScreenShot.Label = Catalog.GetString("Reload screenshot");
					lblScreenShotLabel.Markup = "<b>"+p.GetValue("Package").ToUpper()+"</b>";
					btnViewFullImage.Clicked += (sender, e) => {
						ImageViewer viewer = new ImageViewer();
						viewer.Label = p.GetValue("Package").ToUpper();
						viewer.Image = new Gdk.Pixbuf(img);
						viewer.Show();
					};
				}
			}
			catch
			{
				
			}
		}
		
		void HandleClicked (object sender, EventArgs e)
		{
			if(!Directory.Exists(proc.Project.ScreenshotsDirectory))
				Directory.CreateDirectory(proc.Project.ScreenshotsDirectory);
			
			string strDefaultDownloader = CubeSConfigurationManager.GetValue("config","default-downloader");
			if(strDefaultDownloader!=null)
			{
				if(strDefaultDownloader == "axel")
					DownloadScreenshotAxel();
				else
					DownloadScreenshotAria();
			}			
		}
		
		public void DownloadScreenshotAxel()
		{
			CubeConfiguration config = new CubeConfiguration();			
			
			AxelData data = new AxelData();
			data.FileDirectory = proc.Project.ScreenshotsDirectory;
			data.FileName = p.GetValue("Package");			
			data.DeleteExisting = true;
			data.IsAsynchronous = true;
			data.Connections = 3;
			data.URL = "http://screenshots.ubuntu.com/screenshot/"+p.GetValue("Package");
			if(!config.IsAtLinuxSystem)
			{
				//Comment : To be redirected to PATH environment
			}
			else
			{				
				data.ApplicationName = "axel";
				data.ApplicationDirectory = config.BinDirectory;
			}
			
			btnViewScreenShot.Sensitive = false;
			btnViewScreenShot.Label = Catalog.GetString("Downloading...");
			
			axelDownloader = new AxelDownloader(data);
			axelDownloader.Finished += (sender2, e2) => {
				Application.Invoke(delegate{
					imgScreenShot.Pixbuf = new Gdk.Pixbuf(data.FullFilePath,200,200,true);
					btnViewScreenShot.Label = Catalog.GetString("Reload screenshot");
					btnViewScreenShot.Sensitive = true;
					lblScreenShotLabel.Markup = "<b>"+p.GetValue("Package").ToUpper()+"</b>";
					btnViewFullImage.Clicked += (sender3, e3) => {
						ImageViewer viewer = new ImageViewer();
						viewer.Label = "<b>"+p.GetValue("Package").ToUpper()+"</b>";
						viewer.Image = new Gdk.Pixbuf(data.FullFilePath);
						viewer.Show();
					};
				});
			};
			
			axelDownloader.StateChanged += (sender2, e2) => {
				Application.Invoke(delegate{
					btnViewScreenShot.Label = Catalog.GetString("Downloading...")+axelDownloader.Progress+"%";
				});
			};
			
			axelDownloader.ErrorOccured += (sender2, e2) => {
				Application.Invoke(delegate{
					btnViewScreenShot.Visible = true;
					btnViewScreenShot.Sensitive = true;
					btnViewScreenShot.Label = Catalog.GetString("View Screenshot");	
				});				
			};
			
			axelDownloader.StartDownloadThread();
		}
		
		public void DownloadScreenshotAria()
		{
			CubeConfiguration config = new CubeConfiguration();
			
			AriaData data = new AriaData();
			data = new AriaData();				
			data.File_Path = proc.Project.ScreenshotsDirectory;
			data.File_Name = p.GetValue("Package");
			data.URL = "http://screenshots.ubuntu.com/screenshot/"+p.GetValue("Package");
			data.Delete_Existing = true;
			data.Is_Asynchronous = true;
			if(!config.IsAtLinuxSystem)
			{
				//Comment : To be redirected to PATH environment
				//data.Application_Name = "aria2c.exe";
				//data.Application_Path = config.ThirdPartyDirectory + "aria2";
			}
			else
			{
				data.Application_Name = "aria2c";
				data.Application_Path = config.BinDirectory;
			}
			
			btnViewScreenShot.Sensitive = false;
			btnViewScreenShot.Label = Catalog.GetString("Downloading...");
			
			ariaDownloader = new AriaDownloader(data);
			ariaDownloader.Finished += (sender2, e2) => {
				Application.Invoke(delegate{
					imgScreenShot.Pixbuf = new Gdk.Pixbuf(data.Full_File_Path,200,200,true);
					btnViewScreenShot.Label = Catalog.GetString("Reload screenshot");
					btnViewScreenShot.Sensitive = true;
					lblScreenShotLabel.Markup = "<b>"+p.GetValue("Package").ToUpper()+"</b>";
					btnViewFullImage.Clicked += (sender3, e3) => {
						ImageViewer viewer = new ImageViewer();
						viewer.Label = "<b>"+p.GetValue("Package").ToUpper()+"</b>";
						viewer.Image = new Gdk.Pixbuf(data.Full_File_Path);
						viewer.Show();
					};
				});
			};
			ariaDownloader.ErrorOccured += (sender2, e2) => {
				Application.Invoke(delegate{
					btnViewScreenShot.Visible = true;
					btnViewScreenShot.Sensitive = true;
					btnViewScreenShot.Label = Catalog.GetString("View Screenshot");
				});
			};
			
			ariaDownloader.StartDownload();			
		}		
		
		public void StopAllDownloader()
		{
			if(ariaDownloader!=null)
				ariaDownloader.Kill ();
			else if(axelDownloader!=null)
				axelDownloader.Terminate();
		}
		
		public void CheckForRootRestrictions()
		{
			CubeConfiguration config = new CubeConfiguration();
			if(config.ComputerName == "root")
			{
				btnDownloadThis.Sensitive = false;
			}
		}
		
		string ConvertSize(string size)
		{
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} KB",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} MB",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} GB",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
	}
}

