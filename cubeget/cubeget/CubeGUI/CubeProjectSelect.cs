/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */

using System;
using Cube;
using Gtk;
using System.Threading;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeProjectSelect : Gtk.Window
	{		
		CubeProcess cube;
		
		public event EventHandler OpenedProject;		
		
		public CubeProjectSelect (CubeProcess cube) : base(Gtk.WindowType.Toplevel)
		{
			this.Build ();	
			
			this.imgLogo.Pixbuf = CubeResources.LoadIcon("big","cubelogo");
			
			this.cube = cube;
			this.cube.ProgressChanged += (sender, e) => Application.Invoke(delegate {
				double percent = double.Parse(sender.ToString());
				if(percent >= 0 && percent <=1)
					prgProgress.Fraction = percent;
			});
			
			btnOpenProj.Clicked += OpenProj_Clicked;
			btnCreateProj.Clicked += CreateProj_Clicked;
			cboProjects.Changed += ProjItem_Changed;
			
			this.DeleteEvent += OnDeleteEvent;
			this.Initialize();
		}

		void ProjItem_Changed (object sender, EventArgs e)
		{
			CubeProject p = cube.Projects[cboProjects.Active];
			lblOS.Text = p.OperatingSystem;
			lblArch.Text = p.Architecture;
			lblDateCreated.Text = p.DateCreated;
		}
		
		private void Initialize()
		{			
			cube.LoadProjects();
			
			entProjName.Text = cube.Configuration.ComputerName;
			
			if(cube.Configuration.IsAtLinuxSystem && cube.Configuration.ComputerName!="root")
			{
				grpCreate.Sensitive = true;
			}
			
			if(cube.Projects.Length != 0)
			{								
				grpOpenExist.Sensitive = true;
				for(int i = 0; i < cube.Projects.Length ; i++)
					cboProjects.AppendText(cube.Projects[i].ProjectName);
				cboProjects.Active = 0;
			}
		}
		
		void OpenProj_Clicked (object sender, EventArgs e)
		{
			int intIndex = cboProjects.Active;
			
			if(OpenedProject != null)
				OpenedProject(cube.Projects[intIndex],EventArgs.Empty);
			
			this.Destroy();
		}
		
		void CreateProj_Clicked (object sender, EventArgs e)
		{
			if(entProjName.Text.Length == 0)
				return;
			
			prgProgress.Visible = true;			
			
			btnCreateProj.Sensitive = false;
			
			Thread t = new Thread(CreateProject);			
			t.Start();			
		}
		
		void CreateProject()
		{						
			int res = cube.CreateProject(entProjName.Text.Replace(" ","-").Trim());
			
			if(res == 0)
			{			
				Application.Invoke( delegate {
					btnCreateProj.Sensitive = true;
					OpenedProject(cube.Project,EventArgs.Empty);
					this.Destroy();
				});
			}
			else
			{				
				Application.Invoke( delegate {
					MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,string.Format(Catalog.GetString("Project {0} already exist. Please try another name or delete the project under projects directory"),entProjName.Text));
					md.Title = Catalog.GetString("Project already exist");
					md.Run();
					md.Destroy();
					prgProgress.Visible = false;		
					btnCreateProj.Sensitive = true;
				});
			}
		}
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}
	}
}