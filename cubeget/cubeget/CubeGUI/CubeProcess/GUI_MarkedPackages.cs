/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * Independent_MarkedPackagesTree -- A part of CubeWindow
 * 	Controlling of ToDownload and ToInstall tab are coded here. Initialization of TreeViews and
 * adding of marked packages for download or install are included in this file.
 * 
 * */

using System;
using System.Collections.Generic;
using Cube;
using Gtk;
using System.Threading;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{		
		List<CubePackage> lstMarkedPackages = new List<CubePackage>();
		List<CubePackage> lstMarkedPackagesToInstall = new List<CubePackage>();
		ListStore lstStore = new ListStore(typeof(string),typeof(string));
		ListStore lstStoreInstallation = new ListStore(typeof(string),typeof(string));
		
		public void InitializeMarkedPackagesTree()
		{
			List<TreeColumnHelper> cols = new List<TreeColumnHelper>();
			cols.Add(new TreeColumnHelper("#",0,"text",true));
			cols.Add(new TreeColumnHelper(Catalog.GetString("Package Name"),1,"text",true));			
			TreeHelper tree = new TreeHelper(treeMarked,lstStore,cols.ToArray());
			tree.Initialize();
			
			treeMarked.RowActivated += delegate(object o, RowActivatedArgs args) {				
				string packageName = lstMarkedPackages[int.Parse(args.Path.ToString())].GetValue("Package");
				SearchPackage(packageName);
			};
			
			treeMarked.SearchColumn = 1;
		}
		
		public void InitializeMarkedPackagesToDownloadTree()
		{
			List<TreeColumnHelper> cols = new List<TreeColumnHelper>();
			cols.Add(new TreeColumnHelper("#",0,"text",true));
			cols.Add(new TreeColumnHelper(Catalog.GetString("Package Name"),1,"text",true));			
			TreeHelper tree = new TreeHelper(treeMarkedInstall,lstStoreInstallation,cols.ToArray());
			tree.Initialize();			
			treeMarkedInstall.RowActivated += delegate(object o, RowActivatedArgs args) {						
				string packageName = lstMarkedPackagesToInstall[int.Parse(args.Path.ToString())].GetValue("Package");
				SearchPackage(packageName);
			};
			treeMarkedInstall.SearchColumn = 1;
		}
		
		public void UpdateMarkedPacakgesTree()
		{
			lstStore.Clear();
			int i = 1;
			foreach(CubePackage p in lstMarkedPackages)
			{
				lstStore.AppendValues(i+"",p.GetValue("Package"));
				i++;
			}
			
			lblMarkedPackages.Text = string.Format(Catalog.GetString("Marked Packages ({0})"),lstMarkedPackages.Count);
			lblToDownloadTab.Text = string.Format(Catalog.GetString("To Download ({0})"),lstMarkedPackages.Count);
			ntMarks.CurrentPage = 0;
		}
		
		public void UpdateMarkedPacakgesInstallTree()
		{
			lstStoreInstallation.Clear();
			int i = 1;
			foreach(CubePackage p in lstMarkedPackagesToInstall)
			{
				lstStoreInstallation.AppendValues(i+"",p.GetValue("Package"));
				i++;
			}
			
			lblMarkedPackages1.Text = string.Format(Catalog.GetString("Marked Packages ({0})"),lstMarkedPackagesToInstall.Count);
			lblToInstallTab.Text = string.Format(Catalog.GetString("To Install ({0})"),lstMarkedPackagesToInstall.Count);
			ntMarks.CurrentPage = 1;
		}
		
		public void MarkAllUpdates()
		{
			strLastStatus = lblProgressStat.Text;
			DisableAllComponents();
			lblProgressStat.Text = Catalog.GetString("Marking all updates...");
				
			Thread t = new Thread(MarkAllUpgrades_Thread);
			t.Start();
		}
		
		public void MarkAllUpgrades_Thread()
		{
			CubePackage[] updates = CubePackageFinder.FindUpdates(procMgr.Manager.AvailablePackagesDic);
			Application.Invoke(delegate{
				MarkAllUpgrades_Finished(updates);
			});
		}
		
		public void MarkAllUpgrades_Finished(CubePackage[] updates)
		{
			lblProgressStat.Text = strLastStatus;
			EnableAllComponents();
			
			foreach(CubePackage p in updates)
			{
				if(!lstMarkedPackages.Contains(p))
					lstMarkedPackages.Add(p);
			}
			UpdateMarkedPacakgesTree();
		}
		
		public void MarkAllDownloads()
		{						
			strLastStatus = lblProgressStat.Text;
			DisableAllComponents();
			lblProgressStat.Text = Catalog.GetString("Marking all downloaded packages...");
				
			Thread t = new Thread(MarkAllDownloads_Thread);
			t.Start();
		}
		
		public void MarkAllDownloads_Thread()
		{
			CubePackage[] downloads = CubePackageFinder.FindByStatus("-2",this.procMgr.Manager.AvailablePackagesDic);
			Application.Invoke(delegate{
				MarkAllDownloads_Finished(downloads);
			});
		}
		
		public void MarkAllDownloads_Finished(CubePackage[] downloads)
		{
			lblProgressStat.Text = strLastStatus;
			EnableAllComponents();
			foreach(CubePackage p in downloads)
			{
				if(!lstMarkedPackagesToInstall.Contains(p))
					lstMarkedPackagesToInstall.Add(p);
			}
			UpdateMarkedPacakgesInstallTree();
		}
		
		public void ClearMarkedPackages()
		{
			lstStore.Clear();
			lstMarkedPackages.Clear();
			UpdateMarkedPacakgesTree();
		}
		
		public void ClearMarkedPackagesToInstall()
		{
			lstStoreInstallation.Clear();
			lstMarkedPackagesToInstall.Clear();
			UpdateMarkedPacakgesInstallTree();
		}
	}
}