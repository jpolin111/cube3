/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * DeleteAlreadyInstalledPackages -- A part of CubeWindow
 * 	Cleaning of unnecessary packages (packages that are no longer needed by the cube) are
 * coded here. Other cleaning operations must be included in this file
 * 
 * */

using System;
using System.IO;
using Cube;
using System.Collections.Generic;
using System.Threading;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{		
		public void DeleteLocalPackages_Start()
		{
			strLastStatus = lblProgressStat.Text;
			DisableAllComponents();
			lblProgressStat.Text = Catalog.GetString("Finding downloaded but not needed packages...");
			Thread t = new Thread(DeleteLocalPackages_Thread);
			t.Start();
		}
		
		public void DeleteLocalPackages_Thread()
		{
			List<CubePackage> toDelete = new List<CubePackage>();
			toDelete.AddRange(CubePackageFinder.FindByKey("Downloaded",procMgr.Manager.AvailablePackagesDic));
			toDelete.AddRange(CubePackageFinder.FindByKey("Broken",procMgr.Manager.AvailablePackagesDic));
			
			Application.Invoke(delegate{
				DeleteLocalPackages_Finished(toDelete.ToArray());
			});
		}
		
		public void DeleteLocalPackages_Finished(CubePackage[] packages)
		{						
			
			lblProgressStat.Text = strLastStatus;
			EnableAllComponents();
			
			if(packages.Length == 0)
				return;
			
			PackageListerDialog listerDialog = new PackageListerDialog(packages,true,Catalog.GetString("Delete packages"),
			                                                           Catalog.GetString(
			                                                           "These downloaded packages are already installed in your system" +
			                                                           " and no longer needed by the cube. Broken packages are also included in the list. Press OK to delete these packages."),
			                                                           "list-information");			
			int result = listerDialog.Run();
			listerDialog.Destroy();
			
			if((ResponseType) result == ResponseType.Ok)
			{
				strLastStatus = lblProgressStat.Text;
				DisableAllComponents();;
				lblProgressStat.Text = Catalog.GetString("Deleting packages...");
				Thread t = new Thread(DeleteLocalPackages_StartDelete_Thread);
				t.Start(packages);
			}
		}
		
		public void DeleteLocalPackages_StartDelete_Thread(object o)
		{
			CubePackage[] pcks = (o as CubePackage[]);
			int count = 0;
			foreach(CubePackage p in pcks)
			{
				Application.Invoke(delegate{
					prgProgress.Fraction = count / pcks.Length;
				});
					
				try
				{			
					if(p.PackageDictionary.ContainsKey("Downloaded"))
					{
						File.Delete(p.GetValue("Downloaded"));
						p.PackageDictionary.Remove("Downloaded");
					}
					else if(p.PackageDictionary.ContainsKey("Broken"))
					{
						File.Delete(p.GetValue("Broken"));
						p.PackageDictionary.Remove("Broken");
					}
				}
				catch(Exception ex)
				{
					Console.WriteLine("Unable to delete "+p.GetValue("Downloaded")+" : "+ex.Message);
				}
				count ++;
			}
			
			foreach(string file in Directory.GetFiles(procMgr.Project.PackagesDirectory))
			{
				try{
					//Delete all non deb files
					if((new FileInfo(file).Extension != ".deb"))
						File.Delete(file);
				}catch{}
			}
			
			foreach(string file in  Directory.GetFiles(procMgr.Project.SharedPackagesDirectory))
			{
				try{
					//Delete all non deb files
					if((new FileInfo(file).Extension != ".deb"))
						File.Delete(file);
				}catch{}
			}
			
			Application.Invoke(delegate{
				DeleteLocalPackages_StartDelete_Finished();
			});
		}
		
		public void DeleteLocalPackages_StartDelete_Finished()
		{
			lblProgressStat.Text = strLastStatus;
			EnableAllComponents();
			RemarkAllPackages_Started();
		}
		
		string ConvertSize(string size)
		{
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
	}
}