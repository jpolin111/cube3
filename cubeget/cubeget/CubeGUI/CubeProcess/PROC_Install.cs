/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using Cube;
using Gtk;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{
		public void VerifyDownloadedDependencies(CubePackage p)
		{
			Thread t = new Thread(VerifyDownloadedDependencies_Thread);
			t.Start(p);
		}
		
		public void VerifyDownloadedDependencies_Thread(object p)
		{
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Verifying downloaded dependencies...");
			});
			
			CubePackage pClone = p as CubePackage;
			CubePackage[] depends = procMgr.GetDependenciesAndDontTreatDownloadedAsInstalled(pClone);
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				VerifyDownloadedDependencies_Finished(depends);
			});
		}
		
		public void VerifyDownloadedDependencies_Finished(CubePackage[] toInstall)
		{
			List<CubePackage> packages = new List<CubePackage>();
			List<CubePackage> packagesNotDownloaded = new List<CubePackage>();
			for(int i = 0; i < toInstall.Length ; i++)
			{
				if(toInstall[i].GetValue("Status") == "-2")
					packages.Add(toInstall[i]);
				else
					packagesNotDownloaded.Add(toInstall[i]);
			}
			
			if(packagesNotDownloaded.Count > 0)
			{
				PackageListerDialog dependencies = new PackageListerDialog(packagesNotDownloaded.ToArray(),false,Catalog.GetString("Unmet Dependencies"),Catalog.GetString("Cannot continue. The following packages are needed to be downloaded in order to install the package."),"list-error-cancel");
				dependencies.ButtonOK.Label = Catalog.GetString("Mark these to download");
				int response = dependencies.Run ();
				dependencies.Destroy();
				
				if((ResponseType) response == ResponseType.Ok)
				{
					foreach(CubePackage p in packagesNotDownloaded)
					{
						if(!lstMarkedPackages.Contains(p))
							lstMarkedPackages.Add(p);						
					}
					UpdateMarkedPacakgesTree();
				}
				
				return;
			}
			else
			{
				MessageDialog dialog = new MessageDialog(this,DialogFlags.Modal,MessageType.Info,ButtonsType.Ok,
				                                         "All package dependencies are downloaded and ready to be installed");
				dialog.Title = "Complete Downloaded Package Dependencies";
				dialog.Run();
				dialog.Destroy();
			}
		}
		
		
		/// <summary>
		/// Install specific package
		/// </summary>
		/// <param name='p'>
		/// Package
		/// </param>
		public void InstallPackagesSingle_Start(CubePackage p)
		{
			Thread t = new Thread(InstallPackagesSingle_Thread);
			t.Start(p);
		}
		
		public void InstallPackagesSingle_Thread(object p)
		{
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Building dependencies...");
			});
			
			CubePackage pClone = p as CubePackage;
			CubePackage[] depends = procMgr.GetDependenciesAndDontTreatDownloadedAsInstalled(pClone);
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				InstallPackages_Finished(depends);
			});
		}
		
		public void InstallPackages_Finished(CubePackage[] toInstall)
		{
			List<CubePackage> packages = new List<CubePackage>();
			List<CubePackage> packagesNotDownloaded = new List<CubePackage>();
			for(int i = 0; i < toInstall.Length ; i++)
			{
				if(toInstall[i].GetValue("Status") == "-2")
					packages.Add(toInstall[i]);
				else
					packagesNotDownloaded.Add(toInstall[i]);
			}
			
			if(packagesNotDownloaded.Count > 0)
			{
				PackageListerDialog dependencies = new PackageListerDialog(packagesNotDownloaded.ToArray(),false,Catalog.GetString("Unmet Dependencies"),Catalog.GetString("Cannot continue. The following packages are needed to be downloaded in order to install the package."),"list-error-cancel");
				dependencies.ButtonOK.Label = Catalog.GetString("Mark these to download");
				int response = dependencies.Run ();
				dependencies.Destroy();
				
				if((ResponseType) response == ResponseType.Ok)
				{
					foreach(CubePackage p in packagesNotDownloaded)
					{
						if(!lstMarkedPackages.Contains(p))
							lstMarkedPackages.Add(p);						
					}
					UpdateMarkedPacakgesTree();
				}
				
				return;
			}
			
			string strMsg = string.Format(Catalog.GetString("You are about to install {0} package(s)"), toInstall.Length);
			PackageListerDialog toInstallDialog = new PackageListerDialog(packages.ToArray(),false,Catalog.GetString("Install marked packages"),strMsg,"list-question");
			ResponseType result = (ResponseType)toInstallDialog.Run();
			toInstallDialog.Destroy();
			if(result == ResponseType.Ok)
			{
				if(!VerifyIfInOriginalComputer())
					return;
				
				CubeConfiguration config = new CubeConfiguration();
			
				if(!config.IsAtLinuxSystem)
				{
					MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				    								Catalog.GetString(
					                                "Your computer is not a valid Linux/Apt based operating system." +
				    								"Installation is not allowed. It may be caused of trying to install" +
				    								"using Windows or Linux Distributions that are not apt/debian based."));
					md.Title = Catalog.GetString("Not in a Linux-Apt based system");
					md.Run();
					md.Destroy();
					return;
				}				
				else if(!procMgr.InOriginalComputer)
				{
					MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
					                             Catalog.GetString(
				                                    "Unable to install packages. Your current computer did not match the project computer. This means that you are" +
				                                    " attempting to install to a different computer system. Please install only to" +
				                                    " your original computer"));
					md.Title = Catalog.GetString("Computer Mismatched");
					md.Run();
					md.Destroy();
					return;				
				}		
				
				DisableAllComponents();
				InstallationDialog inst = new InstallationDialog(packages.ToArray(),procMgr.Project.PackagesDirectory);
				inst.Start();
				inst.Run();
				inst.Destroy();
				EnableAllComponents();
												
				if(inst.Success)
					UpdateCubeStatus_Start(false);
				else if(inst.Result != 2)
				
					UpdateCubeStatus_Start(false);
			}
		}		
		
		/// <summary>
		/// Installs marked to be installed packages
		/// </summary>
		public void InstallPackagesMultiple_Start()
		{
			if(lstMarkedPackagesToInstall.Count == 0)
				return;
			Thread t = new Thread(InstallPackagesMultiple_Thread);
			t.Start(lstMarkedPackagesToInstall);
		}
		
		public void InstallPackagesMultiple_Thread(object lst)
		{			
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Building dependencies...");
			});
			
			List<CubePackage> packages = lst as List<CubePackage>;
			
			CubePackage[] depends = procMgr.GetDependenciesAndDontTreatDownloadedAsInstalled(packages);
			
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				InstallPackages_Finished(depends);
			});
		}
		
		public void InstallPackages_AllCompleteDependencies_Start()
		{
			Thread t = new Thread(InstallPackages_AllCompleteDependencies_Thread);
			t.Start();
		}
		
		public void InstallPackages_AllCompleteDependencies_Thread()
		{
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Finding all downloaded packages with complete downloaded dependencies");
			});
			
			List<CubePackage> depends = new List<CubePackage>();
			List<CubePackage> notMetDepends = new List<CubePackage>();
			List<CubePackage> toInstall = new List<CubePackage>();
			CubePackage[] downloaded = CubePackageFinder.FindByStatus("-2",procMgr.Manager.AvailablePackagesDic);			
			
			foreach(CubePackage d in downloaded)
			{
				if(procMgr.ValidateDownloadedDependencies(d,out depends, out notMetDepends))
					toInstall.Add(d);				
			}
			
			downloaded = null;
			depends.Clear();
			notMetDepends.Clear();
			
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				
				if(toInstall.Count > 0)
				{
					Thread t = new Thread(InstallPackagesMultiple_Thread);
					t.Start(toInstall);
				}
			});
		}		
	}
}

