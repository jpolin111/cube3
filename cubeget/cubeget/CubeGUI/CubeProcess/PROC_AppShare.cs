/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * SearchPackage -- A part of CubeWindow
 * 	Searching of Packages, including listing of displaying of specific package 
 * information to the CubeWindow's PackageInformationWidget are coded here 
 * 
 * Widgets and Dialogs used
 * 	PackageInformationWidget
 *  InformationPromptWidget
 * 
 * */

using System;
using Cube;
using Mono.Unix;
using System.Collections.Generic;
using Gtk;
using System.Threading;
using System.IO;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{
		public void AppShare(CubePackage p)
		{			
			Thread t = new Thread(AppShare_Thread);
			t.Start(p);
		}
		
		public void AppShare_Thread(object p1)
		{
			CubePackage p = (CubePackage)p1;
			string strStatTemp = "";
			
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Building dependencies for App Share using current computer...");
			});
			
			List<CubePackage> lstPassed;
			List<CubePackage> lstFailed;
			
			procMgr.GetValidHostPackages(p,out lstPassed,out lstFailed);
			
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;
				AppShare_Finished(p,lstPassed,lstFailed);
			});
		}
		
		public void AppShare_Finished(CubePackage p,List<CubePackage> lstPassed,List<CubePackage> lstFailed)
		{
			CubeConfiguration config = new CubeConfiguration();
			
			if(lstPassed.Count > 0)
			{
				string msg = "";
				
				msg += "<b>Cube App Share</b> will get all needed packages from your host computer \"" + config.ComputerName 
					+"\" and will be treated as <b>Downloaded</b> if repacked successfully.\n\n";
				
				if(lstFailed.Count == 0)
					msg += "<b>All ("+lstPassed.Count+") package dependencies</b> are available to your host computer and ready to be repacked.";
				else
				{
					msg += "<b>"+ lstPassed.Count + " out of " + (lstPassed.Count + lstFailed.Count) + "</b> package dependencies are available to your host computer.\n\n";
					msg += "<b>"+ lstFailed.Count + " package(s)</b> are <b>not installed here or not compatible</b> to your computer and <b>needs to be downloaded</b>. Click <b>Verify</b> button on the package information to see these packages.";
				}
								
				PackageListerDialog dialog = new PackageListerDialog(lstPassed.ToArray(),true,"Cube Application Share",msg,"list-question");
				dialog.ButtonOK.Label = "Repack Now";
				int res = dialog.Run();	
				dialog.Destroy();
				if((ResponseType)res == ResponseType.Ok)
				{					
					if(!File.Exists("/usr/bin/fakeroot"))
					{
						msg = "The program \"fakeroot\" is needed to be installed first in this host computer in order to repackage apps.\n\n" +
							"To install fakeroot, run \"sudo apt-get install fakeroot\" on this host's terminal (Press CTRL+ALT+T to use the terminal), or download it " +
								"using Cube. Just create a project here in host, open it, then search, download, and install fakeroot.";
						
						MessageDialog err_dialog = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,msg);
						err_dialog.Title = "Unable to use Cube App Share. Program fakeroot not installed.";						
						err_dialog.Run();
						err_dialog.Destroy();
						return;
					}
					RepackageDialog repDialog = new RepackageDialog(lstPassed.ToArray(),procMgr.Project);
					repDialog.Start();
					repDialog.Run();
					repDialog.Destroy();
					RemarkAllPackages_Started();
				}
			}
			else
			{
				MessageDialog dialog = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                         "The application " + p.GetValue("Package") + " is not available in the current computer. " +
				                                         "This may cause of application not installed on the current computer or the application is " +
				                                         "version is not equal to your original computer's target application version");
				dialog.Title = "Application Sharing not possible";
				dialog.Run();
				dialog.Destroy();
			}
		}
	}
}