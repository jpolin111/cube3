/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * UpdateStatus - A part of CubeWindow
 * 	Updating original computer's repositories and updating of cube project's status file and 
 * all related operations are included here
 * 
 * */

using System;
using Cube;
using Gtk;
using System.Threading;
using System.IO;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{		
		string strLastStatus = "";
		bool blnCubeStatusAsk = false;
		
		public void UpdateCubeStatus_Start(bool ask)
		{
			if(!VerifyIfInOriginalComputer())
			{				
				if(downloadRepositoryRequest)
					DownloadRepository();				
				else
					ReloadRepositories_Start();
				return;
			}
			
			blnCubeStatusAsk = ask;
			if(ask)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,
				                                     Catalog.GetString(
				                                     "Updating your cube status will informs the cube about what is" +
				                                     "newly installed on your computer. Do you want to continue?"));
				md.Title = "Update Cube Status";
				int result = md.Run();
				md.Destroy();
			
				if((ResponseType) result == ResponseType.No)
					return;
			}			
			
			DisableAllComponents();
			lblProgressStat.Visible = true;
			prgProgress.Visible = true;
			
			int intMax = 1;
			procMgr.SystemUpdater.ProgressChanged += (sender, e) => {
				Application.Invoke(delegate{
					this.prgProgress.Fraction = Convert.ToInt32(sender)/intMax;});
			};
			
			procMgr.SystemUpdater.ProgressMaxChanged += (sender, e) => {
				intMax = Convert.ToInt32(sender);
			};
			
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Updating cube status...");
			
			Thread t = new Thread(UpdateCubeStatus_Thread);
			t.Start();
		}
		
		public void UpdateCubeStatus_Thread()
		{
			int res = procMgr.UpdateCubeStatus();
			Application.Invoke(delegate{
				UpdateCubeStatus_Finished(res);
			});
		}
		
		public void UpdateCubeStatus_Finished(int result)
		{
			EnableAllComponents();
			lblProgressStat.Text = strLastStatus;
			prgProgress.Visible = false;
			
			if(result == -1 && blnCubeStatusAsk)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "Unable to update cube status. Your current computer did not match the project computer. This means that you are" +
				                                     " attempting to update your cube to a different computer system. Please update only to" +
				                                     " your original computer"));
				md.Title = Catalog.GetString("Target Computer Mismatched");
				md.Run();
				md.Destroy();
			}
			else if(blnCubeStatusAsk && result!=0)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "Unable to copy necessary files. Please make sure that you are in linux system and using apt or " +
				                                     "dpkg package management system."));
				md.Title = Catalog.GetString("Error");
				md.Run();
				md.Destroy();
			}
			
			blnCubeStatusAsk = false;			
			
			if(downloadRepositoryRequest)
				DownloadRepository();				
			else
				ReloadRepositories_Start();
		}
		
		public void UpdateSystemStatus_Start(bool ask)
		{
			if(!VerifyIfInOriginalComputer())
				return;
			
			if(ask)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,
				                                     	Catalog.GetString(
					                                     "Updating system status will update your computer's repositories by your newly" +
					                                     " downloaded repositories from cube. Do you want to continue?"));
				md.Title = "Update System Status";
				int result = md.Run();
				md.Destroy();
			
				if((ResponseType) result == ResponseType.No)
					return;
			}
			
			DisableAllComponents();			
			lblProgressStat.Visible = true;
			prgProgress.Visible = true;
			
			int intMax = 1;
			procMgr.SystemUpdater.ProgressChanged += (sender, e) => {
				Application.Invoke(delegate{
					this.prgProgress.Fraction = Convert.ToInt32(sender)/intMax;});
			};
			
			procMgr.SystemUpdater.ProgressMaxChanged += (sender, e) => {
				intMax = Convert.ToInt32(sender);
			};
			
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Updating system status...");
			
			
			Thread t = new Thread(UpdateSystemStatus_Thread);
			t.Start();
			
		}
		
		public void UpdateSystemStatus_Thread()
		{
			/*
			int res = -3;
			//int res = procMgr.UpdateSystemStatus();
			CubeConfiguration config = new CubeConfiguration();
			Cube.ProcessOutput.Run("gksu","--description=\"Cube Update System\" "+config.BinDirectory+"cube-mono "+config.BinDirectory+"cube-update-sys.exe "+procMgr.Project.ProjectName);
			
			string fexitRes = config.LogsDirectory + "exitcode.cube";
			try
			{				
				if(File.Exists(fexitRes))
				{								
					if(int.TryParse(File.ReadAllText(fexitRes).Trim(),out res))
					{
						
					}
				}
			}
			catch{
			}
			
			try{ File.Delete(fexitRes); }catch{}
			
			Application.Invoke(delegate{
				UpdateSystemStatus_Finished(res);
			});			
			*/
			
			int res = procMgr.UpdateSystemStatus();
			Application.Invoke(delegate{
				UpdateSystemStatus_Finished(res);
			});
		}
		
		public void UpdateSystemStatus_Finished(int result)
		{
			EnableAllComponents();
			lblProgressStat.Text = strLastStatus;
			prgProgress.Visible = false;
			
			if(result==0)
			{
				
			}
			else if(result == -1)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "Unable to update computer system, status. Your current computer did not match the project computer. This means that you are" +
				                                     " attempting to update different computer system. Please update only to" +
				                                     " your original computer"));
				md.Title = Catalog.GetString("Target Computer Mismatched");
				md.Run();
				md.Destroy();
			}
			else if(result == -2)
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "Updating system status requires superuser priviledges. Please run" +
				                                     " Cube System as 'root'"));
				md.Title = Catalog.GetString("Cube needs to become superuser first");
				md.Run();
				md.Destroy();
			}
			else
			{
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "Unable to copy necessary files. Please make sure that you are in linux system and using apt or " +
				                                     "dpkg package management system."));
				md.Title = Catalog.GetString("Error");
				md.Run();
				md.Destroy();
			}
		}
	}
}

