/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * SearchPackage -- A part of CubeWindow
 * 	Searching of Packages, including listing of displaying of specific package 
 * information to the CubeWindow's PackageInformationWidget are coded here 
 * 
 * Widgets and Dialogs used
 * 	PackageInformationWidget
 *  InformationPromptWidget
 * 
 * */

using System;
using Cube;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{
		PackageInformationWidget widPackageInfo = null;
		InformationPromptWidget prompt = new InformationPromptWidget();
		string strLastPackageSearch;
		
		public void SearchPackage(string packageName)
		{			
			this.entSearch.Text = packageName;			
			strLastPackageSearch = packageName;
			btnPackageInfo.Active = true;
			cboSearchType.Active = 0;
			btnList.Active = false;
			
			packageName = packageName.Replace(" ","").Trim();
			
			if(widPackageInfo!=null)
				widPackageInfo.StopAllDownloader();
			
			widPackageInfo = new PackageInformationWidget(this.procMgr);
			prompt = new InformationPromptWidget();
			
			widPackageInfo.MarkPackage += (sender, e) => {AddToMarkedList(sender as CubePackage);};
			widPackageInfo.UnmarkPackage += (sender, e) => {RemoveToMarkedList(sender as CubePackage);};
			widPackageInfo.AppSharePackage += (sender, e) => {AppShare(sender as CubePackage);};
			
			widPackageInfo.MarkPackageToInstall += (sender, e) => AddToMarkedInstalledList(sender as CubePackage);
			widPackageInfo.UnmarkPackageToInstall += (sender, e) => RemoveToMarkedInstalledList(sender as CubePackage);
			
			widPackageInfo.DownloadPackage += (sender, e) => {DownloadPackage(sender as CubePackage);};
			widPackageInfo.InstallPackage += (sender, e) => {InstallPackagesSingle_Start(sender as CubePackage);};
			
			widPackageInfo.Verify += (sender, e) => Verify(sender as CubePackage);
			
			CubePackage pseached = CubePackageFinder.Find(packageName.ToLower(),procMgr.Manager.AvailablePackagesDic);
			if(pseached==null)
			{
				foreach(Gtk.Widget i in hbox8.AllChildren)
					hbox8.Remove(i);				
				
				hbox8.Add(prompt);
				hbox8.ShowAll();
			}
			else
			{
				foreach(Gtk.Widget i in hbox8.AllChildren)
					hbox8.Remove(i);
				
				widPackageInfo.Initialize(pseached,lstMarkedPackages,lstMarkedPackagesToInstall);
				hbox8.Add(widPackageInfo);				
				hbox8.ShowAll();
				widPackageInfo.InitializeEvents(pseached,lstMarkedPackages,lstMarkedPackagesToInstall);
			}
		}
		
		public void AutoSearch()
		{
			if(strLastPackageSearch!=null)
			{
				if(strLastPackageSearch.Length>0)
					SearchPackage(strLastPackageSearch);
			}
			else if(entSearch.Text.Trim().Length > 0)
				SearchPackage(entSearch.Text.Trim());				
		}
		
		public void AddToMarkedList(CubePackage p)
		{
			if(p.GetValue("Status") != "0" || p.GetValue("Status") != "-2")
			{
				lstMarkedPackages.Add(p); 
				UpdateMarkedPacakgesTree();
				SearchPackage(p.GetValue("Package"));
			}
		}
		
		public void AddToMarkedInstalledList(CubePackage p)
		{
			lstMarkedPackagesToInstall.Add(p);
			UpdateMarkedPacakgesInstallTree();
			SearchPackage(p.GetValue("Package"));
		}
		
		public void RemoveToMarkedList(CubePackage p)
		{
			lstMarkedPackages.Remove(p); 
			UpdateMarkedPacakgesTree();
			SearchPackage(p.GetValue("Package"));
		}
		
		public void RemoveToMarkedInstalledList(CubePackage p)
		{
			lstMarkedPackagesToInstall.Remove(p);
			UpdateMarkedPacakgesInstallTree();
			SearchPackage(p.GetValue("Package"));
		}		
		
		public void Verify(CubePackage p)
		{
			VerifyDownloadedDependencies(p);
		}
	}
}

