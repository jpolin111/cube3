/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * Initialization -- A part of CubeWindow
 * 	All loading of resources and startup commands are coded here.
 * 
 * */

using System;
using System.IO;
using Cube;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{		
		CubeProcess procMgr;		
		
		bool systemUpdateRequest = false;		
		bool downloadRepositoryRequest = false;
		bool rooted = false;
		
		// Not set = 0
		// In original = 1
		// Not in original = 2
		int askUserIfInOriginalComputer;
		
		/// <summary>
		/// Startup this instance.
		/// </summary>
		public void Startup()
		{
			//Initializes Resouces, Events, Widgets, etc
			InitializeAllComponents();
			
			//Shows the list panel
			ListPackages_Show();			
			
			//Identify computer. Checks if current computer is the original computer of the project, rooted or not.
			int res = procMgr.GetComputerHostType();			
			
			if(res == 0 || res == 1) //Rooted or not rooted, computer matched
			{
				Console.WriteLine("[Cube] In the original computer");
				
				this.systemUpdateRequest = true;
				
				//A reload repository will be called after every update of status
				//false - not asking the user				
				downloadRepositoryRequest = true;
				UpdateCubeStatus_Start(false);		
			}
			else //Not rooted, other computer. Just download the repositories.
			{
				Console.WriteLine("[Cube] Not in original computer");
				DownloadRepository();
			}			
		}
		
		/// <summary>
		/// Initializes all components.
		/// </summary>
		/// <summary>
		/// Initializes all components.
		/// </summary>
		public void InitializeAllComponents()
		{			
			InitializeMarkedPackagesTree();
			InitializeMarkedPackagesToDownloadTree();
			InitializeEvents();
			InitializeResources();
			InitializeMenuEvents();
		}
		
		/// <summary>
		/// Initializes the events.
		/// </summary>
		/// <summary>
		/// Initializes the events.
		/// </summary>
		public void InitializeEvents()
		{
			this.DeleteEvent += OnDeleteEvent;
			procMgr.Manager.ProgressMaxChanged += (sender, e) => StatusProgressMax(int.Parse(sender.ToString()));
			procMgr.Manager.StatusChanged += (sender, e) => StatusLabelValue(sender as String);
			procMgr.Manager.ProgressChanged += (sender, e) => StatusProgressValue(int.Parse(sender.ToString()));
			procMgr.ProgressChanged += (sender, e) => StatusProgressFraction(double.Parse(sender.ToString()));
						
			this.btnSearch.Clicked += (sender, e) => SearchOrList();
			this.btnClear.Clicked += (sender, e) => ClearMarkedPackages();
			this.btnClearInstall.Clicked += (sender, e) => ClearMarkedPackagesToInstall();
			this.btnDownloadMarked.Clicked += (sender, e) => DownloadPackage(lstMarkedPackages);
			this.btnInstallMarked.Clicked += (sender, e) => InstallPackagesMultiple_Start();
			this.btnReloadRepo.Clicked += (sender, e) => DownloadRepository();
			this.btnMarkAllUpgrades.Clicked += (sender, e) => MarkAllUpdates();
			this.btnMarkAllDownloaded.Clicked += (sender, e) => MarkAllDownloads();			
			
			this.btnPackageInfo.Toggled += delegate(object sender, EventArgs e) {
				if(btnPackageInfo.Active)
				{
					btnList.Active = false;
					if(strLastPackageSearch!=null)
						SearchPackage(strLastPackageSearch);
				}
			};
			
			this.btnList.Toggled += delegate(object sender, EventArgs e) {
				if(btnList.Active)
				{
					ListPackages_Show();
					btnPackageInfo.Active = false;
				}
			};
			
			this.entSearch.Activated += (sender, e) => btnSearch.Click();
		}
		
		/// <summary>
		/// Initializes the menu events.
		/// </summary>
		/// <summary>
		/// Initializes the menu events.
		/// </summary>
		public void InitializeMenuEvents()
		{			
			ExitAction.Activated += (sender, e) => Application.Quit();
			
			MarkAllUpgradablePackagesAction.Activated += (sender, e) => MarkAllUpdates();
			MarkAllDownloadedPackagesAction.Activated += (sender, e) => MarkAllDownloads();
			DownloadMarkedPackagesAction.Activated += (sender, e) => DownloadPackage(lstMarkedPackages);
			InstallMarkedPackagesAction.Activated += (sender, e) => InstallPackagesMultiple_Start();
			CleanDownloadedPackagesAction.Activated += (sender, e) => DeleteLocalPackages_Start();
			FixBrokenDownloadedPackagesAction.Activated += (sender, e) => NotImplementedPrompt(Catalog.GetString("Fixing broken downloaded packages is currently not yet implemented. To fix broken packages, you must redownload it."));
			InstallPackagesWithCompleteDependenciesAction.Activated += (sender, e) => InstallPackages_AllCompleteDependencies_Start();			
			
			AddAptRepositoryAction.Activated += (sender, e) => CubeAddAptRepository(); 
			ReloadRepositoriesAction.Activated += (sender, e) => DownloadRepository();
			RemarkPackagesAction.Activated += (sender, e) => RemarkAllPackages_Started();
			GetHostSRepositoriesAction.Activated += (sender, e) => GetHostRepositories();
			
			
			UpdateCubeProjectStatusAction.Activated += (sender, e) => UpdateCubeStatus_Start(true);
			UpdateOriginalComputerSystemStatusAction.Activated += (sender, e) => UpdateSystemStatus_Start(true);			
			
			RepositoriesAction.Activated += delegate(object sender, EventArgs e) {
				ModifyRepositoryDialog modRep = new ModifyRepositoryDialog(this.procMgr.Project);
				modRep.Run();
				modRep.Destroy();
				
				if(modRep.Saved) ReloadRepositories_Start();
			};;
			
			DownloaderSettingsAction.Activated += (sender, e) => NotImplementedPrompt(Catalog.GetString("Configuring your third-party downloader (Aria2 or Axel) is currently not yet implemented."));
			PreferencesAction.Activated += (sender, e) => {
				PreferencesDialog pref = new PreferencesDialog();
				pref.Run();
				pref.Destroy();
			};
			
			TopicsAction.Activated += (sender, e) => NotImplementedPrompt(Catalog.GetString("Topics for Cube is currently not yet implemented"));
			
			AboutAction.Activated += delegate(object sender, EventArgs e) {
				ShowAbout();
			};
			
			VisitCamicriCubeLaunchpadPageAction.Activated += (sender, e) => System.Diagnostics.Process.Start(CubeSInformation.WEBPAGE);
			SendErrorBugReportAction.Activated += (sender, e) => System.Diagnostics.Process.Start(CubeSInformation.REPORT_BUG);
			AskAQuestionAction.Activated += (sender, e) => System.Diagnostics.Process.Start(CubeSInformation.QUESTION);
			TranslateCubeToYourLanguageAction.Activated += (sender, e) => System.Diagnostics.Process.Start(CubeSInformation.TRANSLATE);
			CheckUpdatesOnFacebookAction.Activated += (sender, e) => System.Diagnostics.Process.Start(CubeSInformation.FACEBOOK);
			
			if(!procMgr.HostAndProjectCompatible)
				GetHostSRepositoriesAction.Sensitive = false;
		}
		
		/// <summary>
		/// Initializes the resources.
		/// </summary>
		/// <summary>
		/// Initializes the resources.
		/// </summary>
		public void InitializeResources()
		{
			imgLogo.Pixbuf = CubeResources.LoadIcon("big","cubelogo",100,100);
			imgReload.Pixbuf = CubeResources.LoadIcon("big","reload");
			imgDownloadMarked.Pixbuf = CubeResources.LoadIcon("big","download");
			imgMarkUpgrade.Pixbuf = CubeResources.LoadIcon("big","mark");
			imgMarkDownloaded.Pixbuf = CubeResources.LoadIcon("big","mark");
			imgInstallMarked.Pixbuf = CubeResources.LoadIcon("big","install");			
			
			this.imgbtnSearch.Pixbuf = CubeResources.LoadIcon("small","edit-find");
			this.imgbtnPackageInfo.Pixbuf = CubeResources.LoadIcon("small","dialog-information");
			this.imgbtnList.Pixbuf = CubeResources.LoadIcon("small","view-list");
		}		
				
		/// <summary>
		/// Given the search entry box value, if value contains *, change seach type to 'list all occurence'
		/// </summary>
		/// <summary>
		/// Searchs the or list.
		/// </summary>
		public void SearchOrList()
		{
			string tosearch = entSearch.Text;
			
			if(!tosearch.Contains(":"))
			{
				if(cboSearchType.ActiveText != "Package")
					tosearch = cboSearchType.ActiveText + ":" + tosearch;
			}
			
			if(tosearch.Contains("*") || tosearch.Contains(":"))
			{
				ListPackages_Show();
				btnPackageInfo.Active = false;
				btnList.Active = true;
				ListPackage_Find(tosearch);
			}
			else
			{
				btnPackageInfo.Active = true;
				btnList.Active = false;
				SearchPackage(tosearch);
			}
		}
		
		/// <summary>
		/// Statuses the progress value.
		/// </summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public void StatusProgressValue(int value)
		{
			Application.Invoke(delegate{
				prgProgress.Fraction = ((float)value/(float)divider);
			});
		}
		
		/// <summary>
		/// Statuses the progress fraction.
		/// </summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public void StatusProgressFraction(double value)
		{
			prgProgress.Fraction = value;
		}
		
		int divider=1;
		/// <summary>
		/// Statuses the progress max.
		/// </summary>
		/// <param name='value'>
		/// Value.
		/// </param>
		public void StatusProgressMax(int value)
		{
			this.divider = value;
		}
		
		/// <summary>
		/// Statuses the label value.
		/// </summary>
		/// <param name='msg'>
		/// Message.
		/// </param>
		public void StatusLabelValue(string msg)
		{
			Application.Invoke(delegate{
				lblProgressStat.Text = msg;
			});
		}
		
		/*
		/// <summary>
		/// Notify the user if the system runs in root (superuser)
		/// </summary>
		/// <returns>
		/// true if rooted
		/// </returns>
		/// <summary>
		/// Checks for root restrictions_ notify.
		/// </summary>
		/// <returns>
		/// The for root restrictions_ notify.
		/// </returns>
		public int CheckForRootRestrictions_Notify()
		{						
			//0 = root
			//1 = root, not in original computer
			//2 = not root, original computer
			
			int host = procMgr.GetComputerHostType();
			if(host == 1) //Root
			{				
				root = true;
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Warning,ButtonsType.Ok,
				                                     Catalog.GetString(
				                                     "You are now running as 'root'. Some cube functionality has been disabled" +
				   	                                 " in this mode to protect the cube system and your project. You are now able" +
				       	                             " to install downloaded packages and update your computer by this superuser privilege."));
				md.Title = Catalog.GetString("You have superuser privileges");
				md.Run();
				md.Destroy();
									
				cubeUpdateRequest = true;
				return 0;
			}	
			else if(host == -2) //Root but not in original computer
			{				
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,
				                                 Catalog.GetString(
			                                     "Unable to run this cube system as root. Your computer did not match the project's target" +
			                                     " computer. This means that you are attempting to run the cube as root in different" +
			                                     " computer. Root privileges enables you to install and update your computer repositories" +
			                                     " and may break computer that doesnt match with your project"));
				md.Title = Catalog.GetString("Computer Mismatched");
				md.Run();
				md.Destroy();
				
				return 1;
			}
			else if(host == 0) //Original Computer
			{
				cubeUpdateRequest = true;
			}
			
			return -1;
		}
		*/
		
		/// <summary>
		/// Applies all restrictions if rooted
		/// </summary>
		/// <summary>
		/// Checks the and apply root restrictions.
		/// </summary>
		public void CheckAndApplyRootRestrictions()
		{
			//If the user is root (rooted), disable components.
			if(rooted)			
			{
				btnReloadRepo.Sensitive = false;
				btnMarkAllUpgrades.Sensitive = false;
				btnDownloadMarked.Sensitive = false;
				ReloadRepositoriesAction.Sensitive = false;
				MarkAllUpgradablePackagesAction.Sensitive = false;
				DownloadMarkedPackagesAction.Sensitive = false;
				FixBrokenDownloadedPackagesAction.Sensitive = false;				
			}
		}
		
		/// <summary>
		/// Nots the implemented prompt.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void NotImplementedPrompt(string message)
		{
			MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Warning,ButtonsType.Ok,message);			
			md.Run();
			md.Destroy();
		}
		
		/// <summary>
		/// Enables all components.
		/// </summary>
		public void EnableAllComponents()
		{
			CheckAndApplyRootRestrictions();
			this.Sensitive = true;
			/*
			hbox1.Sensitive = true;
			hbox2.Sensitive = true;
			hbox3.Sensitive = true;
			hbox4.Sensitive = true;
			hbox5.Sensitive = true;
			hpaned4.Sensitive = true;
			menuMain.Sensitive = true;
			*/
		}
		
		/// <summary>
		/// Disables all components.
		/// </summary>
		public void DisableAllComponents()
		{
			this.Sensitive = false;
			/*
			hbox1.Sensitive = false;
			hbox2.Sensitive = false;
			hbox3.Sensitive = false;
			hbox4.Sensitive = false;
			hbox5.Sensitive = false;		
			hpaned4.Sensitive = false;
			menuMain.Sensitive = false;
			*/
		}	
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{			
			Application.Quit ();
			a.RetVal = true;
		}
		
		public bool VerifyIfInOriginalComputer()
		{
			if(!procMgr.InOriginalComputer)
				return false;
			else if(!procMgr.InOriginalComputerViaHome)
			{
				if(procMgr.Configuration.IsAtLinuxSystem)
				{
					if(askUserIfInOriginalComputer == 0)
					{
						MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,"");
						md.Title = "Now I'm confused =_=";
						md.Markup = "Is this your project's original computer?\n\n" +
							"<b>Might break your system if you don't tell the truth</b>";
						int res = md.Run();
						if((ResponseType) res == ResponseType.Yes)
							askUserIfInOriginalComputer = 1;
						else
							askUserIfInOriginalComputer = 2;
						md.Destroy();
					}
				}
			}
			
			if(askUserIfInOriginalComputer == 1)
				return true;
			else if(askUserIfInOriginalComputer == 2)
				return false;
			
			return true;
		}
		
		public void ShowAbout()
		{
			Gtk.AboutDialog about = new Gtk.AboutDialog();
			about.ProgramName = CubeSInformation.APPLICATION_NAME;
			about.Version = CubeSInformation.APPLICATION_VERSION;
			about.Authors = new string[]{CubeSInformation.AUTHOR};
			about.Copyright = CubeSInformation.COPYRIGHT;
			about.Logo = CubeResources.LoadIcon("big","cubelogo");						
			about.Comments = CubeSInformation.DESCRIPTION;
			about.License = CubeSInformation.LICENSE;
			about.Icon = Gdk.Pixbuf.LoadFromResource ("cubeget.bin.Debug.data.resources.icons.big.cubelogo.ico");			
			about.Run();			
			about.Destroy();
		}
	}
	
	public class CubePackageCount
	{
		int installed = 0;
		int broken = 0;
		int downloaded = 0;
		int upgradable = 0;
		
		public int Installed
		{
			set { this.installed = value; }
			get { return installed; }
		}
		
		public int Broken
		{
			set { this.broken = value; }
			get { return broken; }
		}
		
		public int Downloaded
		{
			set { this.downloaded = value; }
			get { return downloaded; }
		}
		
		public int Upgradable
		{
			set { this.upgradable = value; }
			get { return upgradable; }
		}
	}
}