/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * ListPackages -- A part of CubeWindow
 * 	Listing of occurrence of a package (By word occurrence or category) are coded here.
 * Uses the PackageListerWidget.
 * 
 * */

using System;
using System.Collections.Generic;
using Cube;
using Gtk;
using System.Threading;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{
		PackageListerWidget listerWidget = null;
		
		public void ListPackages_Show()
		{
			if(listerWidget==null)
			{			
				if(btnPackageInfo.Active)
					btnList.Active = false;
				listerWidget = new PackageListerWidget();
				listerWidget.InitializeTree();
				listerWidget.PackageLoadingStarted += HandlePackageLoadingStarted;
				listerWidget.PackageLoadingFinished += HandlePackageLoadingFinished;
				listerWidget.RequestForInstalledPackages += HandleRequestForInstalledPackages;
				listerWidget.RequestForUpgradablePackages += HandleRequestForUpgradablePackages;
				listerWidget.RequestForDownloadedPackages += HandleRequestForDownloadedPackages;
				listerWidget.RequestForBrokenPackages += HandleRequestForBrokenPackages;
				listerWidget.RequestForCategoryFilter += HandleListerWidgetRequestForCategoryFilter;
				listerWidget.RequestForAllPackages += HandleListerWidgetRequestForAllPackages;
				listerWidget.PackageSelected += HandlePackageSelected;
			}
			
			foreach(Gtk.Widget i in hbox8.AllChildren)
				hbox8.Remove(i);			
			hbox8.Add(listerWidget);
			hbox8.ShowAll();
		}

		void HandleListerWidgetRequestForCategoryFilter (object sender, EventArgs e)
		{
			CategoriesDialog catDialog = new CategoriesDialog();
			catDialog.CategorySelected += delegate(object sender2, EventArgs e2) {
				ListPackage_Find("Section:"+catDialog.Category);
			};			
			catDialog.Run();			
		}

		void HandlePackageSelected (object sender, EventArgs e)
		{			
			SearchPackage((sender as CubePackage).GetValue("Package"));
		}
		
		public void ListPackage_Find(string name)
		{									
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Filtering packages. Please wait...");
			DisableAllComponents();
			
			if(name.StartsWith("*") && name.EndsWith("*"))
			{
				name = name.Replace("*","").Trim();
				Thread t = new Thread(ListPackage_FindContains_Thread);
				t.Start(name);
			}
			else if(name.StartsWith("*"))
			{
				name = name.Replace("*","").Trim();
				Thread t = new Thread(ListPackage_FindEnd_Thread);
				t.Start(name);
			}
			else if(name.EndsWith("*"))
			{
				name = name.Replace("*","").Trim();
				Thread t = new Thread(ListPackage_FindStart_Thread);
				t.Start(name);
			}
			else if(name.Contains(":"))
			{
				string[] categ = name.Split(':');
				if(categ.Length==2)
				{
					Thread t = new Thread(ListPackage_FindCategory_Thread);
					t.Start(name);
				}
			}
		}
		
		public void ListPackage_FindCategory_Thread(object name)
		{
			string[] categ = name.ToString().Split(':');
			List<CubePackage> lst = new List<CubePackage> (CubePackageFinder.FindByCategory(categ[0],categ[1],this.procMgr.Manager.AvailablePackagesDic));
			Application.Invoke(delegate{
				ListPackage_Find_Finished(lst);
			});
		}
		
		public void ListPackage_FindStart_Thread(object name)
		{
			List<CubePackage> lst = new List<CubePackage>(CubePackageFinder.FindByPackageName(name.ToString(),"start",this.procMgr.Manager.AvailablePackagesDic));
			Application.Invoke(delegate{
				ListPackage_Find_Finished(lst);
			});
		}
		
		public void ListPackage_FindEnd_Thread(object name)
		{
			List<CubePackage> lst = new List<CubePackage>(CubePackageFinder.FindByPackageName(name.ToString(),"end",this.procMgr.Manager.AvailablePackagesDic));
			Application.Invoke(delegate{
				ListPackage_Find_Finished(lst);
			});
		}
		
		public void ListPackage_FindContains_Thread(object name)
		{
			List<CubePackage> lst = new List<CubePackage>(CubePackageFinder.FindByPackageName(name.ToString(),"contains",this.procMgr.Manager.AvailablePackagesDic));
			Application.Invoke(delegate{
				ListPackage_Find_Finished(lst);
			});
		}
		
		public void ListPackage_Find_Finished(List<CubePackage> lst)
		{
			lblProgressStat.Text = strLastStatus;
			listerWidget.LoadPackages_Start(lst);			
			EnableAllComponents();
		}
				
		void HandlePackageLoadingFinished (object sender, EventArgs e)
		{
			EnableAllComponents();
			lblProgressStat.Text = strLastStatus;
		}

		void HandlePackageLoadingStarted (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Listing requested packages...");
			DisableAllComponents();			
		}
		
		public void ListPackages_Request(object requestType)
		{
			List<CubePackage> lst = new List<CubePackage>();
			string type = requestType.ToString();
			
			if(type == "installed")
				lst = new List<CubePackage>(this.procMgr.Manager.InstalledPackagesDic.Values);
			else if(type == "downloaded")
				lst = new List<CubePackage>(CubePackageFinder.FindByStatus("-2",procMgr.Manager.AvailablePackagesDic));
			else if(type == "upgradable")
				lst = new List<CubePackage>(CubePackageFinder.FindUpdates(procMgr.Manager.AvailablePackagesDic));
			else if(type == "broken")
				lst = new List<CubePackage>(CubePackageFinder.FindByKey("Broken",procMgr.Manager.AvailablePackagesDic));
			else if(type == "all")
			{				
				lst = new List<CubePackage>(procMgr.Manager.AvailablePackagesDic.Values);				
			}
			
			Application.Invoke(delegate{
				ListPackage_Find_Finished(lst);				
			});
		}				

		void HandleRequestForInstalledPackages (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Filtering packages. Please wait...");
			DisableAllComponents();
			Thread t = new Thread(ListPackages_Request);
			t.Start("installed");
		}
		
		void HandleRequestForUpgradablePackages (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Filtering packages. Please wait...");
			DisableAllComponents();
			Thread t = new Thread(ListPackages_Request);
			t.Start("upgradable");
		}

		void HandleRequestForBrokenPackages (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Filtering packages. Please wait...");
			DisableAllComponents();
			Thread t = new Thread(ListPackages_Request);
			t.Start("broken");
		}

		void HandleRequestForDownloadedPackages (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = Catalog.GetString("Filtering packages. Please wait...");
			DisableAllComponents();
			Thread t = new Thread(ListPackages_Request);
			t.Start("downloaded");
		}
		
		void HandleListerWidgetRequestForAllPackages (object sender, EventArgs e)
		{
			strLastStatus = lblProgressStat.Text;
			lblProgressStat.Text = "Loading all packages may take too long. Please wait...";
			DisableAllComponents();
			Thread t = new Thread(ListPackages_Request);
			t.Start("all");
		}
		
	}
}

