/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * Download Repositories or Packages -- A part of CubeWindow
 * 	Downloading of repositories and packages are coded here. All related operations must be
 * included in this file.
 * 
 * Widgets and Dialogs used
 * 	PackageListerDialog
 * 	DownloaderDialog (Aria)
 *  DownloaderDialogAxel (Axel)
 * 
 * */

using System;
using System.IO;
using Cube;
using System.Collections.Generic;
using System.Threading;
using Gtk;
using Mono.Unix;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{		
		public void DownloadPackage(CubePackage p)
		{
			Thread t = new Thread(DownloadPackageSingle_Thread);
			t.Start(p);
		}
		
		public void DownloadPackageSingle_Thread(object p)
		{
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Building dependencies...");
			});
			
			CubePackage pClone = p as CubePackage;
			CubePackage[] depends = procMgr.GetDependencies(pClone);
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				DownloadPackage_Finished(depends);
			});
		}
		
		public void DownloadPackage_Finished(CubePackage[] depends)
		{
			string strDefaultDownloader = CubeSConfigurationManager.GetValue("config","default-downloader");
			if(strDefaultDownloader!=null)
			{
				if(strDefaultDownloader == "axel")
					DownloadPackageAxel_Finished(depends);
				else
					DownloadPackageAria_Finished(depends);
			}
			else
				DownloadPackageAria_Finished(depends);
		}
		
		public void DownloadPackageAxel_Finished(CubePackage[] depends)
		{
			PackageListerDialog listerDialog = new PackageListerDialog(depends,true,Catalog.GetString("To download packages"),string.Format(Catalog.GetString("These {0} package(s) needs to be downloaded also"),depends.Length),"list-question");
			
			int response = listerDialog.Run();
			listerDialog.Destroy();
			listerDialog = null;
			
			if((ResponseType) response == ResponseType.Ok)
			{				
				//DownloaderDialog downloadDialog = new DownloaderDialog("Downloading packages","Downloading selected packages. ");
				DownloaderDialogAxel downloadDialog = new DownloaderDialogAxel("Downloading packages","Downloading selected packages. ");
				downloadDialog.SetPackagesToDownload(depends,procMgr.Project.PackagesDirectory);
				downloadDialog.Finished += (sender, e) => {
					if(lstMarkedPackages.Contains(sender as CubePackage))
					{
						lstMarkedPackages.Remove(sender as CubePackage);
						UpdateMarkedPacakgesTree();
					}
				};
				downloadDialog.Run();				
				downloadDialog.Destroy();
				
				if(!downloadDialog.Success)
					downloadDialog.StopAllDownload();
				
				if(downloadDialog.PackagesFailedToDownload.Length > 0)
				{
					MessageDialog dialog = new MessageDialog(this,DialogFlags.Modal,MessageType.Warning,ButtonsType.Ok,
					                                         Catalog.GetString(
					                                         "Some packages failed to be downloaded. All finished packages will" +
					                                         " be removed to the 'Marked Packages to download' list and all failed" +
					                                         " packages will remain"));
					dialog.Run();
					dialog.Destroy();
					foreach(CubePackage pFailed in downloadDialog.PackagesFailedToDownload)
					{
						if(!lstMarkedPackages.Contains(pFailed))
							lstMarkedPackages.Remove(pFailed);
					}
					UpdateMarkedPacakgesTree();
				}
				
				RemarkAllPackages_Started();
			}
		}
		
		public void DownloadPackageAria_Finished(CubePackage[] depends)
		{
			PackageListerDialog listerDialog = new PackageListerDialog(depends,true,Catalog.GetString("To download packages"),string.Format(Catalog.GetString("These {0} package(s) needs to be downloaded also"),depends.Length),"list-question");
			
			int response = listerDialog.Run();
			listerDialog.Destroy();
			listerDialog = null;
			
			if((ResponseType) response == ResponseType.Ok)
			{				
				DownloaderDialog downloadDialog = new DownloaderDialog(Catalog.GetString("Downloading packages"),Catalog.GetString("Downloading selected packages."));				
				downloadDialog.SetPackagesToDownload(depends,procMgr.Project.PackagesDirectory);
				downloadDialog.Finished += (sender, e) => {
					if(lstMarkedPackages.Contains(sender as CubePackage))
					{
						lstMarkedPackages.Remove(sender as CubePackage);
						UpdateMarkedPacakgesTree();
					}
				};
				downloadDialog.Run();				
				downloadDialog.Destroy();
				
				if(!downloadDialog.Success)
					downloadDialog.StopAllDownload();
				
				if(downloadDialog.PackagesFailedToDownload.Length > 0)
				{
					MessageDialog dialog = new MessageDialog(this,DialogFlags.Modal,MessageType.Warning,ButtonsType.Ok,
					                                         Catalog.GetString(
					                                         "Some packages failed to be downloaded. All finished packages will" +
					                                         " be removed to the 'Marked Packages to download' list and all failed" +
					                                         " packages will remain"));
					dialog.Title = Catalog.GetString("Broken Downloaded Packages Detected");
					dialog.Run();
					dialog.Destroy();
					foreach(CubePackage pFailed in downloadDialog.PackagesFailedToDownload)
					{
						if(!lstMarkedPackages.Contains(pFailed))
							lstMarkedPackages.Remove(pFailed);
					}
					UpdateMarkedPacakgesTree();
				}
				
				RemarkAllPackages_Started();
			}
		}
		
		public void DownloadPackage(List<CubePackage> ps)
		{
			if(ps.Count >0)
			{
				Thread t = new Thread(DownloadPackageMultiple_Thread);
				t.Start(ps);
			}
		}
		
		public void DownloadPackageMultiple_Thread(object p)			
		{
			string strStatTemp = "";
			Application.Invoke(delegate{
				DisableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;				
				strStatTemp = lblProgressStat.Text;
				lblProgressStat.Text = Catalog.GetString("Building dependencies...");
			});
			List<CubePackage> lstToDownload = p as List<CubePackage>;
			CubePackage[] depends = procMgr.GetDependencies(lstToDownload);
			Application.Invoke(delegate{
				EnableAllComponents();
				prgProgress.Visible = false;
				lblProgressStat.Visible = true;
				lblProgressStat.Text = strStatTemp;				
				DownloadPackage_Finished(depends);
			});
		}
		
		public void DownloadRepository()
		{
			//Ask for system update for every download of repositories			
			systemUpdateRequest = true;
			
			//Reset download repository request trigger
			downloadRepositoryRequest = false;
			
			string strDefaultDownloader = CubeSConfigurationManager.GetValue("config","default-downloader");
			if(strDefaultDownloader!=null)
			{
				if(strDefaultDownloader == "axel")
					DownloadRepositoryAxel();
				else
					DownloadRepositoryAria();
			}
			else
				DownloadRepositoryAria();
		}
		
		public void DownloadSpecificRepository(CubeSource[] s, CubeReleaseGPG[] r)
		{
			//Read sources.list and other lists first
			procMgr.Manager.ReadList();
			string strDefaultDownloader = CubeSConfigurationManager.GetValue("config","default-downloader");
			if(strDefaultDownloader!=null)
			{
				if(strDefaultDownloader == "axel")
					DownloadSpecificRepositoryAxel(s,r);
				else
					DownloadSpecificRepositoryAria(s,r);
			}
			else
				DownloadSpecificRepositoryAria(s,r);
		}
		
		public void DownloadRepositoryAria()
		{
			DownloadSpecificRepositoryAria(procMgr.Manager.Sources,procMgr.Manager.Releases);
		}
		
		public void DownloadRepositoryAxel()
		{
			DownloadSpecificRepositoryAxel(procMgr.Manager.Sources,procMgr.Manager.Releases);
		}
		
		public void DownloadSpecificRepositoryAxel(CubeSource[] s, CubeReleaseGPG[] r)
		{
			MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.OkCancel,Catalog.GetString("This will download the latest repositories available to update your package list. Do you want to continue?"));
			md.Title = Catalog.GetString("Update Repository");
			if( (ResponseType)md.Run() == ResponseType.Ok)
			{
				md.Destroy();
				//DownloaderDialog downloadDialog = new DownloaderDialog("Updating your repositories","Downloading necessary repositories. Please wait");
				DownloaderDialogAxel downloadDialog = new DownloaderDialogAxel(Catalog.GetString("Updating your repositories"),Catalog.GetString("Downloading necessary repositories. Please wait"));				
				downloadDialog.Finished += DownloadRepository_Finished;
				downloadDialog.SetSourcesToDownload(procMgr.Project, s,r,procMgr.Project.ListDirectory+"temp"+ System.IO.Path.DirectorySeparatorChar);
				downloadDialog.Run();
				downloadDialog.Destroy();
				if(!downloadDialog.Success)
					downloadDialog.StopAllDownload();
			}
			else
				md.Destroy();
			
			ReloadRepositories_Start();
		}
		
		public void DownloadSpecificRepositoryAria(CubeSource[] s, CubeReleaseGPG[] r)
		{
			MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.OkCancel,Catalog.GetString("This will download the latest repositories available to update your package list. Do you want to continue?"));
			md.Title = Catalog.GetString("Update Repository");
			if( (ResponseType)md.Run() == ResponseType.Ok)
			{
				md.Destroy();
				DownloaderDialog downloadDialog = new DownloaderDialog(Catalog.GetString("Updating your repositories"),Catalog.GetString("Downloading necessary repositories. Please wait"));				
				downloadDialog.Finished += DownloadRepository_Finished;
				downloadDialog.SetSourcesToDownload(s,r,procMgr.Project.ListDirectory+"temp"+ System.IO.Path.DirectorySeparatorChar);
				downloadDialog.Run();
				downloadDialog.Destroy();
				if(!downloadDialog.Success)
					downloadDialog.StopAllDownload();
			}
			else
				md.Destroy();
			
			
			ReloadRepositories_Start();
		}
				
		public void DownloadRepository_Finished (object sender, EventArgs e)
		{
			/*
			 * Extract the downloaded repository's package list and
			 * replace the current repository
			 * */
			if(sender.GetType() == typeof(CubeSource))
			{
				CubeSource source = (sender as CubeSource);
				string filename = procMgr.Project.ListDirectory+"temp"+System.IO.Path.DirectorySeparatorChar+source.Filename;
				string target = procMgr.Project.ListDirectory + source.Filename;
				if(CubeDecompress.Decompress(filename,target))
				{
					try{ File.Delete(filename); }
					catch{}
				}
			}
			else if(sender.GetType() == typeof(CubeReleaseGPG))
			{
				CubeReleaseGPG r = (sender as CubeReleaseGPG);
				string filename = procMgr.Project.ListDirectory+"temp"+System.IO.Path.DirectorySeparatorChar+r.ReleaseFilename;
				string target = procMgr.Project.ListDirectory + r.ReleaseFilename;
				if(!File.Exists(filename))
				{
					filename = procMgr.Project.ListDirectory+"temp"+System.IO.Path.DirectorySeparatorChar+r.ReleaseGPGFilename;
					target = procMgr.Project.ListDirectory + r.ReleaseGPGFilename;
				}
				
				try{
					if(File.Exists(target))
						File.Delete(target);
					File.Move(filename,target);
				}
				catch(Exception ex)
				{
					Console.WriteLine("Cannot copy "+filename+" to "+target+" . "+ex.Message);
				}
			}
		}
	}
}