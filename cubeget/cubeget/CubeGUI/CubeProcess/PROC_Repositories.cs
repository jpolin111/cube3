/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * Reload Repositories -- A part of CubeWindow
 * 	All commands related to loading of repositories and marking for packages are
 * coded here
 * 
 * */

using System;
using System.Threading;
using Gtk;
using Cube;
using Mono.Unix;
using System.IO;
using System.Collections.Generic;

namespace cubeget
{
	public partial class CubeWindow : Gtk.Window
	{
		
		public void ReloadRepositoriesAndMarkPackages()
		{

		}
		
		public void ReloadRepositories_Start()
		{
			DisableAllComponents();
			prgProgress.Visible = true;
			lblProgressStat.Visible = true;
			Thread t = new Thread(ReloadRepositories_Thread);
			t.Start();
		}
		
		public void ReloadRepositories_Thread()
		{
			procMgr.ReloadProject();
			Application.Invoke(delegate{
				ReloadRepositories_Finished();
			});
		}
		
		public void ReloadRepositories_Finished()
		{
			EnableAllComponents();
			prgProgress.Visible = false;
			lblProgressStat.Visible = true;
			lblProgressStat.Text = Catalog.GetString("Checking for upgradable and downloaded packages...");
			UpdatePackageCountStatus();
			
			if(entSearch.Text.Length != 0)
				SearchPackage(entSearch.Text);
			
			if(systemUpdateRequest)
			{
				systemUpdateRequest = false;
				UpdateSystemStatus_Start(true);
			}			
		}		
		
		public void GetHostRepositories()
		{
			int count = 0;
			int minuscount = 0;
			foreach(CubeSource s in procMgr.Manager.Sources)
			{
				string path = UbuntuEssentials.PackageListDirectoryPath + "/" + s.Filename;				
				
				//Skip status file
				if(s.Filename.StartsWith("status"))
				{
					minuscount ++;
					continue;
				}
				
				if(File.Exists(path))
					count ++;
			}
			
			if(count != 0)
			{
				string msg = "";
				int total = (procMgr.Manager.Sources.Length - minuscount);
				if(count == total)
					msg = "<b>All repositories (package list)</b> are available to your host computer.";
				else
					msg = "<b>" + count + " out of " + total + " </b>repositories (package list) are available to you host computer.";
				
				msg += "\n\nMake sure that your host's repositories are <b>newer (more updated)</b> than your project's repositories.";
				msg += "\n\nDo you want to transfer it to your project? ";
				
				MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,"");
				md.Title = "Get Host's Repositories";
				md.Markup = msg;
				int res = md.Run();
				md.Destroy();
				if((ResponseType)res == ResponseType.Yes)
				{
					strLastStatus = lblProgressStat.Text;
					lblProgressStat.Text = Catalog.GetString("Copying host's repositories to your project's repositories");
					DisableAllComponents();
					Thread t = new Thread(GetHostRepositories_Thread);
					t.Start();
				}
			}
		}
		
		public void GetHostRepositories_Thread()
		{
			procMgr.GetHostRepositories();
			Application.Invoke(delegate{
				GetHostRepositories_Finished();	
			});
		}
		
		public void GetHostRepositories_Finished()
		{
			lblProgressStat.Text = strLastStatus;	
			EnableAllComponents();
			ReloadRepositories_Start();
		}
		
		public void RemarkAllPackages_Started()
		{
			DisableAllComponents();
			lblProgressStat.Visible = true;
			lblProgressStat.Text = Catalog.GetString("Marking all downloaded packages...");
			prgProgress.Visible = true;
			procMgr.ProgressChanged += RemarkAllPackages_Progress;	
			
			Thread t = new Thread(RemarkAllPackages_Thread);
			t.Start();
		}
		
		public void RemarkAllPackages_Thread()
		{						
			procMgr.MarkForDownloadedPackages();
			Application.Invoke(delegate{
				RemarkAllPackages_Finished();
			});
		}

		void RemarkAllPackages_Progress (object sender, EventArgs e)
		{
			
			Application.Invoke(delegate{
				try{
					prgProgress.Fraction = (double) sender;
				}catch{}
			});
			
		}
		
		public void RemarkAllPackages_Finished()
		{
			EnableAllComponents();			
			lblProgressStat.Visible = true;
			prgProgress.Visible = false;
			UpdatePackageCountStatus();
			if(entSearch.Text.Length != 0)
				SearchPackage(entSearch.Text);
			else
			{
				foreach(Gtk.Widget i in hbox8.AllChildren)
					hbox8.Remove(i);
			}
		}
		
		public void CubeAddAptRepository()
		{
			AddAptRepositoryDialog addApt = new AddAptRepositoryDialog();
			addApt.Run();			
			addApt.Destroy();
			
			if(addApt.Success)
			{
				AddAptRepositorySelectDialog addAptSelect = new AddAptRepositorySelectDialog(procMgr,addApt.Fetcher);
				addAptSelect.Run();
				addAptSelect.Destroy();
				if(addAptSelect.Success)
				{					
					Console.WriteLine(addAptSelect.AptDebAddress);
					string aptlist = procMgr.Project.AptDirectory + "sources.list";
					string[] aptlines = File.ReadAllLines(aptlist);
					bool save = true;
					foreach(string i in aptlines)
					{
						if(i.StartsWith("deb") && i == addAptSelect.AptDebAddress)
						{							
							MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Error,ButtonsType.Ok,"PPA Address already exist.");
							
							md.Title = Catalog.GetString("Duplicate PPA");
							md.Run();
							md.Destroy();				
							save = false;
							break;
						}
					}
					if(save)
					{
						StreamWriter writer = new StreamWriter(aptlist, true);
						writer.WriteLine("");
						writer.WriteLine("#Camicri Cube Add Apt Repository Entry");
						writer.WriteLine(addAptSelect.AptDebAddress);
						writer.Close();
						
						/*
						MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo,"New repository address successfully added. Do you want to update the repository for the new repository to take effect?");
						md.Title = "Update repository required";
						if(((ResponseType)(md.Run())) == ResponseType.Yes)
							DownloadRepository();
						md.Destroy();
						*/
						
						List<CubeSource> lstSources = new List<CubeSource>();
						procMgr.Manager.ReadList();
						string[] ppa_info = addApt.Fetcher.PPA.Replace("ppa:","").Split('/');
						foreach(CubeSource s in procMgr.Manager.Sources)
						{
							if(s.Link.Contains(ppa_info[0]) && s.Link.Contains(ppa_info[1]) && s.Link.Contains(addAptSelect.CodeName))
								lstSources.Add(s);
						}
						
						lstSources.Add(new CubeSource("","status"));
						
						if(lstSources.Count > 0)
							DownloadSpecificRepository(lstSources.ToArray(),null);
					}
				}
			}
		}
		
		public void UpdatePackageCountStatus()
		{
			int intAvailablePackages = procMgr.Manager.AvailablePackagesDic.Count;
			int intInstalledPackages = procMgr.Manager.InstalledPackagesDic.Count;
			int intUpgradablePackages = CubePackageFinder.FindUpdates(procMgr.Manager.InstalledPackagesDic).Length;
			int intDownloadedNotNeeded = CubePackageFinder.FindByKey("Downloaded",procMgr.Manager.AvailablePackagesDic).Length;
			
			string strCount = "";
			if(intAvailablePackages > 0)
				strCount += string.Format(Catalog.GetString("{0} package(s) available, "),intAvailablePackages);
			if(intInstalledPackages > 0)
				strCount += string.Format(Catalog.GetString("{0} package(s) installed"),intInstalledPackages);
			if(intUpgradablePackages > 0)
				strCount += string.Format(Catalog.GetString(", {0} package(s) upgradable"),intUpgradablePackages);
			if(intDownloadedNotNeeded > 0)
				strCount += string.Format(Catalog.GetString(", {0} package(s) downloaded but not needed"),intDownloadedNotNeeded);
			
			lblProgressStat.Text = strCount;
			
			if(lstMarkedPackagesToInstall.Count > 0)
			{
				for(int i = 0; i < this.lstMarkedPackagesToInstall.Count; i++)
				{
					CubePackage p = CubePackageFinder.Find(lstMarkedPackagesToInstall[i].GetValue("Package"),procMgr.Manager.AvailablePackagesDic);
					if(p.GetValue("Status") == "0")
						lstMarkedPackagesToInstall.RemoveAt(i);
				}
				UpdateMarkedPacakgesInstallTree();
			}
			
			CubePackage[] broken = CubePackageFinder.FindByKey("Broken",procMgr.Manager.AvailablePackagesDic);
			if(broken!=null)
			{
				if(broken.Length > 0)
				{
					lblProgressStat.Text += string.Format(Catalog.GetString(", {0} broken packages"),broken.Length);
					MessageDialog md = new MessageDialog(this,DialogFlags.Modal,MessageType.Warning,ButtonsType.Ok,
					                                     Catalog.GetString(
					                                     "You have broken downloaded packages in your project. This may cause of unfinished download, package mismatch, or package does not meet" +
					                                     " the available package version."));
					md.Title = Catalog.GetString("Broken packages detected");
					md.Run();
					md.Destroy();
				}
			}
			
			AutoSearch();
		}		
	}
}

