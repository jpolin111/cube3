/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Diagnostics;
using System.Threading;
using System.IO;
using Cube;
using Mono.Unix;

namespace cubeget
{
	public class CubeAPTInstaller
	{		
		CubePackage[] packages;		
				
		public event EventHandler Finished;		
		
		string packageDirectory = "";
		string parameters = "--assume-yes --no-download --allow-unauthenticated ";
		string extra_params = "";
		string output_file = "";
		
		string error_message = "";
		bool blnSuccess = false;
		int type = 1; //type 1 = installation error , type 2 = terminal error
		
		public CubeAPTInstaller (CubePackage[] packages, string packagesDir)
		{
			this.packages = packages;
			this.packageDirectory = packagesDir;
			this.Initialize();
		}
		
		public void Initialize()
		{			
			extra_params = CubeSConfigurationManager.GetValue("config","apt-get-parameters");
			if(extra_params!=null)
				parameters = extra_params.Trim() + " ";
			
			parameters += "-o dir::cache::archives=\""+Directory.GetCurrentDirectory()+Path.DirectorySeparatorChar+packageDirectory+"\" install ";
			
			Array.Reverse(packages);
			
			foreach(CubePackage p in packages)
			{
				parameters += p.GetValue("Package") + " ";
			}
		}
		
		public void StartInstallation()
		{
			Thread t;			
			t = new Thread(StartInstallation_Thread);
			t.Start();
		}
		
		public void StartInstallation_Thread()
		{			
			CubeConfiguration config = new CubeConfiguration();
			string fcommand = Directory.GetCurrentDirectory() + config.Slash + config.TemporaryFilesDirectory + "command.sh";
			string fexitRes = Directory.GetCurrentDirectory() + config.Slash + config.LogsDirectory + "exitcode.cube";
			output_file = Directory.GetCurrentDirectory() + config.Slash + config.LogsDirectory + "cube-install-"+DateTime.Now.ToLongDateString()+"-"+DateTime.Now.ToLongTimeString() + ".log";
			output_file = output_file.Replace("@","").Replace(" ","");
			
			StreamWriter write = new StreamWriter(fcommand);
			write.WriteLine("apt-get " + parameters + " | tee \""+output_file+"\"");
			write.WriteLine("echo $? >> "+fexitRes);
			write.Close();
			
			ProcessOutput.Run("chmod","+x "+fcommand);
			
			if(File.Exists(packageDirectory+"lock"))
			{
				try{
					File.Delete(packageDirectory+"lock");
				}catch{}
			}
			
			//ProcessOutput.Run("gksu","--description=\"Cube APT-GET Terminal Install\" "+config.BinDirectory+"cube-mono " + config.BinDirectory+"cube-install.exe sh "+fcommand);
			
			string term = UbuntuEssentials.FindTerminal();
			if(term == null)
			{				
				type = 2;
				error_message = Catalog.GetString("Unable to find terminal to be used. Please check for terminals (ie. gnome-terminal) installed on your computer and add it on cube/data/config/config.cube under \"installer-terminals\" key.");
				if(Finished !=null) Finished(this,EventArgs.Empty);				
				return;
			}
			
			/*
			string cde_path = config.CDEPath;
			if(cde_path!=null)
			{
				Console.WriteLine("[Cube] Installing using CDE redirect");
				ProcessOutput.Run(cde_path,"-f " + config.RootCommand + " " + term + " -e \" "+fcommand+"\"");				
			}
			else
				//ProcessOutput.Run(term,"-e \"sh "+fcommand+"\"");
			*/
			ProcessOutput.Run(config.RootCommand, term + " -e \" "+fcommand+"\"");
			
			try
			{				
				if(File.Exists(fexitRes))
				{					
					int exitCode = -1;
					if(int.TryParse(File.ReadAllText(fexitRes).Trim(),out exitCode))
					{
						if(exitCode == 0)
						{
							if(File.Exists(output_file))
							{
								bool needtoget_detect = false;
								//Check output if apt-get request additional package to be downloaded
								foreach(string s in File.ReadAllLines(output_file))
								{
									if(s.StartsWith("Need to get"))
									{
										needtoget_detect = true;
										//If 0 B (Means all given packages are sufficient enough)
										if(s.Contains("0 B"))
											blnSuccess = true;
										else //Some packages needs to be downloaded/upgraded first
											error_message = Catalog.GetString("APT-GET refused to continue. Some packages are needed to be downloaded in order to install selected package(s). Please check the output to know packages needed and download it first.\n\nThe best way to prevent this to happen again is to always update all of your packages (Using 'Mark all updates' and 'Download all Marked' before installing.");
										break;
									}
								}

								if(!needtoget_detect)
									blnSuccess = true;
							}
							else
								blnSuccess = true;
						}
						else
							error_message = Catalog.GetString("An error occured during the installation. Exit Code : ")+exitCode+".";
					}
					else
						error_message = Catalog.GetString("Unable to parse exit code file.");
				}
				else
					error_message = Catalog.GetString("Unable to determine result.");
				
				try{ File.Delete(fcommand); }catch{}
				try{ File.Delete(fexitRes); }catch{}
				try{ File.Delete(packageDirectory+"lock"); }catch{}
				
			}catch(Exception ex) {
				error_message = Catalog.GetString("Cube Internal Error. Unable to connect to external application. ")+ex.Message;
			}
			
			if(Finished !=null) Finished(this,EventArgs.Empty);
		}
		
		public string ErrorMessage
		{
			get { return this.error_message; }
		}
		
		public bool Success
		{
			get { return this.blnSuccess; }
		}
		
		public int ErrorType
		{
			get { return this.type; }
		}
		
		public string OutputFilePath
		{
			get { return output_file; }
		}
	}
}