/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Collections.Generic;

namespace Cube
{
	public class CubeProjectCreator
	{					
		
		public event EventHandler ProgressMaxChanged;
		//public event EventHandler StatusChanged;
		public event EventHandler ProgressChanged;
		//public event EventHandler ErrorOccured;
		
		CubeProject proj;
		string strProjectParentDir = "";
		
		bool debug = true;
		
		public CubeProjectCreator ()
		{
		}
		
		public CubeProjectCreator (CubeProject proj)
		{
			this.proj = proj;
		}
		
		public CubeProjectCreator (string strProjectParentDir)
		{
			this.strProjectParentDir = strProjectParentDir;
		}
		
		public int CreateProject(bool blnReplace)
		{
			if(ProgressMaxChanged!=null) ProgressMaxChanged(8,EventArgs.Empty);
				
			if(Directory.Exists(proj.ProjectDirectory) && !blnReplace)
				return 1;
			else if(Directory.Exists(proj.ProjectDirectory) && blnReplace)
				DeleteDirectory(proj.ProjectDirectory);
			
			if(ProgressChanged!=null) ProgressChanged(1,EventArgs.Empty);			
			
			if(!InitializeDirectories())
				return 2;
			
			if(ProgressChanged!=null) ProgressChanged(2,EventArgs.Empty);
			
			CubeConfiguration config = new CubeConfiguration();
			if(!config.IsAtLinuxSystem)
				return 3;
			
			if(ProgressChanged!=null) ProgressChanged(3,EventArgs.Empty);
			
			if(!CopyPackageList())
				return 4;
			
			if(ProgressChanged!=null) ProgressChanged(4,EventArgs.Empty);
			
			if(!CopyStatusFile())
				return 5;
			
			if(ProgressChanged!=null) ProgressChanged(5,EventArgs.Empty);
			
			if(!CopySourceFile())
				return 6;
			
			if(ProgressChanged!=null) ProgressChanged(6,EventArgs.Empty);
			
			if(!CopyAptPreferenceFile())
				return 7;
			
			if(ProgressChanged!=null) ProgressChanged(7,EventArgs.Empty);
			
			if(!GenerateProjectInformationFile())
				return 7;
			
			if(ProgressChanged!=null) ProgressChanged(8,EventArgs.Empty);
			return 0;
		}
		
		public CubeProject[] GetProjects()
		{
			if(strProjectParentDir==null)
				return null;
			List<CubeProject> lstProjects = new List<CubeProject>();
			foreach(string strProjectDir in Directory.GetDirectories(strProjectParentDir))
			{
				CubeProject p = GetProjectInfo(strProjectDir+ Path.DirectorySeparatorChar + "info.cube");
				if(p!=null)
					lstProjects.Add(p);
			}
			return lstProjects.ToArray();
		}
		
		public CubeProject GetProjectInfo(string strInfoPath)
		{
			CubeProject p = new CubeProject();
			if(!File.Exists(strInfoPath))
				return null;
			
			foreach(string line in File.ReadAllLines(strInfoPath))
			{
				if(line.StartsWith("Project Version"))
					p.ProjectVersion = line.Split(':')[1].Trim();
				else if(line.StartsWith("Project Name"))
					p.ProjectName = line.Split(':')[1].Trim();
				else if(line.StartsWith("Computer Name"))
					p.ComputerName = line.Split(':')[1].Trim();
				else if(line.StartsWith("Operating System"))
					p.OperatingSystem = line.Split(':')[1].Trim();
				else if(line.StartsWith("Architecture"))
					p.Architecture = line.Split(':')[1].Trim();
				else if(line.StartsWith("Date Created"))
					p.DateCreated = line.Split(":".ToCharArray(),2)[1].Trim();
				else if(line.StartsWith("Release"))
					p.Release = line.Split(':')[1].Trim();
				else if(line.StartsWith("Code Name"))
					p.CodeName = line.Split(':')[1].Trim();
				else if(line.StartsWith("Distribution"))
					p.Distribution = line.Split(':')[1].Trim();
			}
			
			p.ProjectParentDirectory = strProjectParentDir;
			return p;
		}
		
		private bool CopyPackageList()
		{
			if(debug) Console.WriteLine("Copying Package List...");
			foreach(string strListFile in Directory.GetFiles(UbuntuEssentials.PackageListDirectoryPath,"*Packages"))
			{
				try
				{
					if(debug) Console.Write("Copying {0} to {1}..",strListFile,proj.ListDirectory+(new FileInfo(strListFile).Name));
					File.Copy(strListFile,proj.ListDirectory+(new FileInfo(strListFile).Name),true);
					if(debug) Console.WriteLine("OK");
				}
				catch(Exception ex)
				{
					if(debug) Console.WriteLine("FAILED! " +ex.Message);
				}
			}
			if(debug) Console.WriteLine("Copying Package List Complete\n");
			return true;
		}
		
		private bool CopyStatusFile()
		{
			if(debug) Console.Write("Copying Status File...");
			try
			{
				File.Copy(UbuntuEssentials.StatusFilePath,proj.ListDirectory+"status",true);
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("OK");
			return true;
		}
		
		private bool CopySourceFile()
		{
			if(debug) Console.Write("Copying Source File...");
			try
			{				
				File.Copy(UbuntuEssentials.SourcesListFilePath,proj.AptDirectory+"sources.list",true);
				foreach(string strList in Directory.GetFiles(UbuntuEssentials.OtherSourcesListDirectoryPath,"*.list"))
				{
					File.Copy(strList, proj.AptDirectory + new FileInfo(strList).Name,true);
				}
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("OK");
			return true;
		}
		
		private bool CopyAptPreferenceFile()
		{
			if(debug) Console.Write("Copying Apt Preference File...");
			try
			{				
				if(File.Exists(UbuntuEssentials.AptPreferencesFilePath))
					File.Copy(UbuntuEssentials.AptPreferencesFilePath,proj.AptDirectory+"preferences",true);
				else
					if(debug) Console.Write("Not exists. ");
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("OK");
			return true;
		}
		
		private bool DeleteDirectory(string strPath)
		{
			try
			{
				Directory.Delete(strPath,true);
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine(ex.Message);
				return false;
			}
			return true;
		}
		
		private bool InitializeDirectories()
		{
			Directory.CreateDirectory(proj.DataDirectory);
			Directory.CreateDirectory(proj.AptDirectory);
			Directory.CreateDirectory(proj.ListDirectory);
			Directory.CreateDirectory(proj.PackagesDirectory);
			Directory.CreateDirectory(proj.SharedPackagesDirectory);
			Directory.CreateDirectory(proj.ScreenshotsDirectory);
			return true;
		}
		
		private bool GenerateProjectInformationFile()
		{
			try
            {
				CubeConfiguration config = new CubeConfiguration();
                StreamWriter writer = new StreamWriter(proj.ProjectDirectory + "info.cube");
                writer.WriteLine("#Camicri Cube Project Information File");
				writer.WriteLine("Project Version : {0}", proj.ProjectVersion);
                writer.WriteLine("Project Name : {0}", proj.ProjectName);
				writer.WriteLine("Computer Name : {0}", proj.ComputerName);				
                writer.WriteLine("Operating System : {0}", proj.OperatingSystem);
				writer.WriteLine("Distribution : {0}",proj.Distribution);
				writer.WriteLine("Release : {0}",proj.Release);
				writer.WriteLine("Code Name : {0}",proj.CodeName);
                writer.WriteLine("Architecture : {0}", proj.Architecture);
                writer.WriteLine("Date Created : {0}", proj.DateCreated);
                writer.Close();
				
				if(!Directory.Exists(config.CubeLinuxDataDirectory))
					Directory.CreateDirectory(config.CubeLinuxDataDirectory);
				
				string filename = config.CubeLinuxDataDirectory + proj.ProjectName + "_" + proj.DateCreated.Replace("/","-") + "_info.cube";
				File.Copy(proj.ProjectDirectory + "info.cube", filename,true);
					
            }
            catch (Exception ex)
            {
                if(debug) Console.WriteLine(ex.Message);
                return false;
            }
            return true;
		}
	}
}

