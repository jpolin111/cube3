/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Collections.Generic;

namespace Cube
{
	public class UbuntuEssentials
	{
		static string strPackageListDirectoryPath = "/var/lib/apt/lists";
        static string strStatusFilePath = "/var/lib/dpkg/status";
        static string strSourcesListFilePath = "/etc/apt/sources.list";
		static string strOtheSourcesListDirectoryPath = "/etc/apt/sources.list.d/";
		static string strAptPreferencesFilePath = "/etc/apt/preferences";
		static string strAptPreferencesDirectoryPath = "/etc/apt/preferences.d/";
		static string strHomeDirectoryPath = "/home/";
		static string strMD5SumProvider = "/usr/bin/md5sum";
		
		public static string PackageListDirectoryPath
		{
			get { return strPackageListDirectoryPath; }
		}
		
		public static string StatusFilePath
		{
			get { return strStatusFilePath; }
		}
		
		public static string SourcesListFilePath
		{
			get { return strSourcesListFilePath; }
		}
		
		public static string OtherSourcesListDirectoryPath
		{
			get { return strOtheSourcesListDirectoryPath; }
		}

		public static string AptPreferencesFilePath
		{
			get { return strAptPreferencesFilePath; }
		}

		public static string AptPreferencesDirectory
		{
			get { return strAptPreferencesDirectoryPath; }
		}
		
		public static string UbuntuHomeDirectory
		{
			get { return strHomeDirectoryPath;}
		}
		
		public static string MD5SumProvider
		{
			get { return strMD5SumProvider; }
		}
		
		public static string Home
		{
			get { return System.Environment.GetEnvironmentVariable("HOME"); }
		}
		
		public static string FindTerminal()
		{			
			List<string> term_list = new List<string>();

			//Check configuration file if there were installation terminals given.
			string terms = CubeSConfigurationManager.GetValue ("config", "installation-terminals");
			if (terms != null) {
				foreach (string term in terms.Split(';'))
					term_list.Add (term.Trim ());
			} else {
				//Default terminals
				//List according to sequence of availability			
				term_list.Add ("/usr/bin/mate-terminal");
				term_list.Add ("/usr/bin/gnome-terminal");
				term_list.Add ("/usr/bin/xterm");
				term_list.Add ("/usr/bin/xfce4-terminal");
				term_list.Add ("/usr/bin/lxterminal");
				term_list.Add ("/usr/bin/pantheon-terminal");
				term_list.Add ("/usr/bin/konsole");
			}
			
			foreach(string term in term_list)
			{
				if(File.Exists(term))
					return term;
			}
			
			return null;
		}
	}
}