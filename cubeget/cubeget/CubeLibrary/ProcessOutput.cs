/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Diagnostics;
namespace Cube
{
	public class ProcessOutput
	{
		public ProcessOutput ()
		{
		}
        #region [Private Static Global Declarations]
        static Process proc;
        #endregion

        #region [Public Static Methods]

        public static string Read(string strFileName)
        {
            proc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = strFileName;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;			
            startInfo.CreateNoWindow = true;
            proc.StartInfo = startInfo;
            return Start(true);
        }

        public static string Read(string strFileName,string strArgs)
        {
            proc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = strFileName;
            startInfo.Arguments = strArgs;
            startInfo.UseShellExecute = false;			
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
            proc.StartInfo = startInfo;			
            return Start(true);
        }
		
		public static int Run(string strFileName,string strArgs)
        {
            proc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = strFileName;
            startInfo.Arguments = strArgs;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
            proc.StartInfo = startInfo;
            try
            {
                proc.Start();
            }
            catch
            {
                return -1;
            }
			proc.WaitForExit();			
			return proc.ExitCode;
        }
		
		public static int Run(string strFileName,string strArgs,string dir)
        {
            proc = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = strFileName;
            startInfo.Arguments = strArgs;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
			startInfo.WorkingDirectory = dir;
            proc.StartInfo = startInfo;
            try
            {
                proc.Start();
            }
            catch
            {
                return -1;
            }
			proc.WaitForExit();
			return proc.ExitCode;
        }

        #endregion

        #region [Methods for reading process output]

        private static string Start(bool attemptAgainOnce)
        {			
			string strLines = "";
            try
            {
                proc.Start();            

		        while (!proc.StandardOutput.EndOfStream)
				{							
					strLines += proc.StandardOutput.ReadLine()+"\r\n";
				}
				
				proc.StandardOutput.Close();
			}			
            catch
            {
                if(attemptAgainOnce)
				{
					Console.WriteLine("[Cube Process Output] Failed to run external command. Attempting to run again");
					return Start(false);
				}
				else
				{
					Console.WriteLine("[Cube Process Output] Failed to run external command.");
					return null;
				}
            }	
			return strLines.Trim();
        }
        #endregion		
	}
}