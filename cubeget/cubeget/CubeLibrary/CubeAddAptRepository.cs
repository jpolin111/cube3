// 
//  AddAptRepositoryFetcher.cs
//  
//  Author:
//       Jake Capangpangan <camicrisystems@gmail.com>
// 
//  Copyright (c) 2014 Camicri Systems
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
using System;
using System.IO;
using System.Collections.Generic;
using Aria;
using Cube;
using Mono.Unix;

namespace cubeget
{
	public class CubeAddAptRepositoryFetcher
	{
		public event EventHandler GetRepositoriesSuccess;
		public event EventHandler GetRepositoriesFailed;
		
		CubeConfiguration config = new CubeConfiguration();
		
		string strError;
		List<string> lstSeries;
		string debAddressFormat = null;
		string ppa = null;		
		string title = null;
		string launchpad_format = "https://launchpad.net/~{0}/+archive/{1}";		
		
		public CubeAddAptRepositoryFetcher ()
		{
		}
		
		public void StartRepositoryPPAFetch(string ppa)
		{		
			lstSeries = new List<string>();
			debAddressFormat = null;
			this.ppa = ppa.Trim();
			
			if(!(ppa.StartsWith("ppa") && ppa.Contains("/")))
			{
				strError = Catalog.GetString("Unable to continue. Invalid PPA Format.");
				if(GetRepositoriesFailed!=null)
					GetRepositoriesFailed(this,EventArgs.Empty);
				return;
			}
			
			string[] ppa_info = ppa.Replace("ppa:","").Split('/');
			
			AriaData data = new AriaData();
			data.URL = String.Format(launchpad_format,ppa_info[0],ppa_info[1]);
			data.File_Name = "PPATempInfo.txt";
			data.File_Path = config.TemporaryFilesDirectory;
			data.Is_Asynchronous = true;
			data.Delete_Existing = true;
			data.Arguments = "--check-certificate=false";
			
			AriaDownloader ariadl = new AriaDownloader(data);
			
			ariadl.Finished += HandleAriadlFinished;
			ariadl.ErrorOccured += HandleAriadlErrorOccured;
			
			ariadl.StartDownload();
		}

		void HandleAriadlErrorOccured (object sender, EventArgs e)
		{
			strError = Catalog.GetString("Unable to access PPA address. Please ensure that you have internet connection or the PPA is valid.");
			if(GetRepositoriesFailed!=null)
				GetRepositoriesFailed(this,EventArgs.Empty);
		}

		void HandleAriadlFinished (object sender, EventArgs e)
		{
			string path = config.TemporaryFilesDirectory + "PPATempInfo.txt";			
			
			try
			{
				StreamReader reader = new StreamReader(path);
				bool blnCodenameRead = false;
				bool blnTitleRead = false;
				
				while(!reader.EndOfStream)
				{
					string line = reader.ReadLine();	
					if(!blnTitleRead)
					{
						if(line.Trim().StartsWith("<title>"))
						{
							title = line.Replace("<title>","").Replace("</title>","").Trim();
							blnTitleRead = true;
						}
					}
					if(line.StartsWith("<option selected=\"selected\""))
					{		
						line = reader.ReadLine();
						while (line.StartsWith("<option"))
						{
							lstSeries.Add(line.Substring(15).Split('\"')[0].Trim());
							line = reader.ReadLine();
						}
						blnCodenameRead = true;
					}
					else if(blnCodenameRead)
					{
						if(line.StartsWith("deb"))
						{
							debAddressFormat = "deb " + line.Split('>')[1].Split('<')[0] + " {0} ";
							line = reader.ReadLine();
							debAddressFormat += line.Split('>')[2].Split('<')[0].Trim();
							break;
						}
					}
				}
				reader.Close();
				if(GetRepositoriesSuccess!=null)
					GetRepositoriesSuccess(this,EventArgs.Empty);
				File.Delete(path);
			}catch
			{
				strError = Catalog.GetString("Parsing failed.");
				GetRepositoriesFailed(this,EventArgs.Empty);
			}
		}
		
		public string[] Series
		{
			get { return this.lstSeries.ToArray(); }
		}
		
		public string DebAddressFormat
		{
			get { return this.debAddressFormat; }
		}
		
		public string ErrorMessage
		{
			get { return strError; }
		}
		
		public string Title
		{
			get { return title; }
		}
		
		public string PPA
		{
			get { return ppa; }
		}
	}
}

