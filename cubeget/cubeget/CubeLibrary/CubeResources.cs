/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using Gdk;
using Gtk;
using System.IO;

namespace Cube
{
	public class CubeResources
	{
		public CubeResources ()
		{
			
		}		
				
		public static Pixbuf LoadIcon(string type, string strIconName)
		{
			CubeConfiguration config = new CubeConfiguration();
			string strFindDir = config.IconsDirectory + type.Trim();
			
			string[] filematched = Directory.GetFiles(strFindDir,strIconName.Trim()+".*");
			if(filematched!=null)
			{
				if(filematched.Length>0)
				{
					return new Pixbuf(filematched[0]);
				}
			}
			return null;
		}
		
		public static Pixbuf LoadIcon(string type, string strIconName, int width, int height)
		{
			CubeConfiguration config = new CubeConfiguration();
			string strFindDir = config.IconsDirectory + type.Trim();
			
			string[] filematched = Directory.GetFiles(strFindDir,strIconName.Trim()+".*");
			if(filematched!=null)
			{
				if(filematched.Length>0)
				{
					return new Pixbuf(filematched[0],width,height);
				}
			}
			return null;
		}
		
		public static void ApplyGtkTheme(string themeDirectoryName)
		{
			CubeConfiguration config = new CubeConfiguration ();
			string theme = config.ThemesDirectory + themeDirectoryName + config.Slash + "gtkrc";
			
			if(File.Exists(theme))
			{				
				Gtk.Rc.AddDefaultFile (theme);
				Gtk.Rc.Parse (theme);
			}
			else
				Console.WriteLine("[RESOURCE] Unable to find 'gtkrc' file in the requested theme. Default theme applied");
		}
	}
}

