/*
 * Aria2c Downloader Interface Library
 * (c) Camicri Systems
 * 
 * Library for C# that provide controls to aria2c downloader (Linux and Windows)
 * */

using System;
using System.Diagnostics;
using System.IO;
//using System.Windows.Forms;
using System.Threading;
using o = System.Console;

namespace Aria
{
	public class AriaDownloader
	{		
		private AriaData data;
		private Thread thAria;
		private Process aria;
		public string ErrorMessage = "";

		private bool debug = false;
		
		//Event Handlers
		public event EventHandler DownloadStarted;
		public event EventHandler ProgressChanged;
		public event EventHandler Finished;
		public event EventHandler ErrorOccured;
		
		//Invoker Delegate Callbacks
		private delegate void ProgressChangedCallback(object sender, EventArgs e);
		private delegate void DownloadStartedCallback(object sender, EventArgs e);
		private delegate void FinishedCallback(object sender, EventArgs e);
		private delegate void ErrorOccuredCallback(object sender, EventArgs e);
				
		private string strSizeDownloaded = "";
		private string strSizeTotal = "";
		private int intProgress = 0;
		private string strTimeRemain = "";
		private bool blnError;
		
		
		//Constructor
		public AriaDownloader(AriaData data)
		{
			this.data = data;			
		}		
		
		//Method to start download
		public void StartDownload()
		{
			if (debug)
				o.WriteLine ("[Aria2] Starting download");
			if(data.Delete_Existing) //If delete existing
				DeleteExisting(); //Delete target file
			if(data.Is_Asynchronous) //If download in separate thread
			{
				if (debug)
					o.WriteLine ("[Aria2] Starting async");
				//Create thread for download
				thAria = new Thread(Download);
				thAria.Start();
			}
			else
				Download(); //Normally starts the download
		}
		
		//Method to delete existing file
		private void DeleteExisting()
		{
			try
			{
				if (debug)
					o.WriteLine ("[Aria2] File already exist.");
				string file = data.File_Path + '/' + data.File_Name;
				if(File.Exists(file))
					File.Delete(file);				
				if (debug)
					o.WriteLine ("[Aria2] Deleted");
			}
			catch(Exception ex)
			{
				if (debug)
					o.WriteLine ("[Aria2] Unable to delete : " + ex.Message);
			}
		}
		
		public void Kill()
		{
			if (debug)
				o.WriteLine ("[Aria2] Suicide");
			if(aria!=null)
			{
				try{
					aria.Kill();
					if (debug)
						o.WriteLine ("[Aria2] Suicide Complete");
				}
				catch(Exception ex){
					if (debug)
						o.WriteLine ("[Aria2] Unable to kill self : "+ex.Message);
				}				
			}
		}
		
		//Download Method
		public void Download()
		{
			if (debug)
				o.WriteLine ("[Aria2] Thread started");
			blnError = false;
			aria = new Process();
			
			ProcessStartInfo aria_info = new ProcessStartInfo();
			
			string aria2c = data.Application_Path.Length!=0?data.Application_Path+System.IO.Path.DirectorySeparatorChar+data.Application_Name:data.Application_Name;
			
			if(!File.Exists(aria2c))
			{
				Console.WriteLine("Using local aria2c");
				aria2c = "aria2c"; //default
			}
			else
				Console.WriteLine("Using static aria2c at "+aria2c);
			
			aria_info.FileName = aria2c;
			aria_info.Arguments = data.Arguments.Trim();
            aria_info.UseShellExecute = false;
            aria_info.RedirectStandardOutput = true;
            aria_info.CreateNoWindow = true;

			if (debug)
				o.WriteLine ("[Aria2] PARAM Filename : "+aria_info.FileName);
			if (debug)
				o.WriteLine ("[Aria2] PARAM Working Directory : "+aria_info.WorkingDirectory);
			if (debug)
				o.WriteLine ("[Aria2] PARAM Arguments : "+aria_info.Arguments);

			aria.StartInfo = aria_info;			
				
			try
			{
				if(aria.Start())
				{
					OnDownloadStarted();
				}
				else
				{
					if (debug)
						o.WriteLine ("[Aria2] Internal Error : Failed to start");
					blnError = true;
					ErrorMessage = "There was a problem with the downloader";
					OnErrorOccured();
					return;
				}
			
				while (!aria.StandardOutput.EndOfStream)
    	        {
					string strLine = aria.StandardOutput.ReadLine();
					Console.Write("[ARIA2]"+strLine+"\r");
					AnalyzeOutput(strLine);				
				}
				aria.WaitForExit();
				int intExitCode = aria.ExitCode;
				if(intExitCode!=0&&!blnError)
				{
					ErrorMessage = "Download Aborted! Exit Code: "+ aria.ExitCode;
					blnError = true;
					OnErrorOccured();
					if (debug)
						o.WriteLine ("[Aria2] TRY : Download Aborted! Exit Code : "+aria.ExitCode);
				}
			}
			catch
			{
				if (debug)
					o.WriteLine ("[Aria2] CATCH : Download Aborted! Failed to get the exit code");
				ErrorMessage = "Download Aborted. Failed to get the exit code";
				blnError = true;
				OnErrorOccured();
			}
		}
		
		//Analyze and extract data from aria2c output
		public void AnalyzeOutput(string strLine)
		{
			
			if(strLine.StartsWith("["))
			{				
				string[] strFrags = strLine.Split(' ');
				foreach(string fragment in strFrags)
				{
					if(fragment.StartsWith("SIZE"))
					{
						string strSize = fragment.Replace("SIZE:","");
						string[] strSizeStat = strSize.Split('/');
						strSizeDownloaded = strSizeStat[0];
						if(!strSizeStat[1].Contains("("))
							strSizeTotal = strSizeStat[1];
						else
						{
							string[] strSizeStat_2 = strSizeStat[1].Split('(');
							strSizeTotal = strSizeStat_2[0];
							intProgress = Convert.ToInt32(strSizeStat_2[1].Replace(")","").Replace("%","").Trim());
						}
					}
					else if(fragment.StartsWith("ETA"))
					{
						strTimeRemain = fragment.Replace("ETA:","").Replace("]","");
					}
				}				
				OnProgressChanged();
			}
			else if(strLine.Contains("NOTICE"))
			{
				if(strLine.Contains("Download complete"))
				{
					if (debug)
						o.WriteLine ("[Aria2] Download completed");
					intProgress = 100;
					strTimeRemain = "00s";
					strSizeDownloaded = strSizeTotal;
					OnProgressChanged();
					OnFinished();
				}
			}
			else if(strLine.Contains("ERROR"))
			{	
				if (debug)
					o.WriteLine ("[Aria2] An error Occured");
				/*
				Gtk.Application.Invoke(delegate{
				ErrorMessage = ""+aria.ExitCode;
				});
				*/
				blnError = true;
				OnErrorOccured();
			}
		}
		
		#region [Event Handler Methods]
		protected virtual void OnProgressChanged()
		{
			if(ProgressChanged==null)
				return;
			/*
			if(data.Invoker !=null)
				data.Invoker.Invoke(new ProgressChangedCallback(ProgressChangedM), new object[]{this,EventArgs.Empty});
			else*/
				ProgressChangedM(this,EventArgs.Empty);
		}
		
		protected virtual void OnDownloadStarted()
		{
			if(DownloadStarted==null)
				return;
			/*
			if(data.Invoker !=null)
				data.Invoker.Invoke(new DownloadStartedCallback(DownloadStartedM), new object[]{this,EventArgs.Empty});
			else*/
				DownloadStartedM(this,EventArgs.Empty);
		}
		
		protected virtual void OnFinished()
		{
			if(Finished==null)
				return;
			/*
			if(data.Invoker !=null)
				data.Invoker.Invoke(new FinishedCallback(FinishedM), new object[]{this,EventArgs.Empty});
			else*/
				FinishedM(this,EventArgs.Empty);
		}
		
		protected virtual void OnErrorOccured()
		{
			if(ErrorOccured==null)
				return;
			/*
			if(data.Invoker !=null)
				data.Invoker.Invoke(new ErrorOccuredCallback(ErrorOccuredM), new object[]{this,EventArgs.Empty});
			else*/
				ErrorOccuredM(this,EventArgs.Empty);
		}		
		
		private void ProgressChangedM(object sender, EventArgs e)
		{
			ProgressChanged(sender,e);
		}
		
		private void DownloadStartedM(object sender, EventArgs e)
		{
			DownloadStarted(sender,e);
		}
		
		private void FinishedM(object sender, EventArgs e)
		{
			Finished(sender,e);
		}
		
		private void ErrorOccuredM(object sender, EventArgs e)
		{
			ErrorOccured(sender,e);				
		}
					
		#endregion
		
		#region [Properties]		
		
		public string CompleteFilePath
		{
			get { return data.File_Path + '/' + data.File_Name; }
		}
		
		public string FilePath
		{
			get { return data.File_Path; }
		}
		
		public string FileName
		{
			get { return data.File_Name; }
		}
		
		public int Progress
		{
			get { return intProgress; }
		}
		
		public string TotalSize
		{
			get { return strSizeTotal; }
		}
		
		public string CurrentSize
		{
			get { return strSizeDownloaded; }
		}
		
		public string TimeRemaining
		{
			get { return strTimeRemain; }
		}
		
		public string Error_Message
		{
			get { return ErrorMessage; }
		}
		
		public bool Is_Error_Occured
		{
			get { return blnError; }
		}
		
		#endregion
	}
	
	public class AriaData
	{
		string strParam = "";
		string strAppPath = "";
		string strAppName = "aria2c";
		string strFilename = "";
		string strFilepath = "";
		bool blnDeleteExisting = false;
		bool blnAsync = false;
		bool blnLinux = false;
		bool blnDisplayOutput = false;

		//Form frmInvoker = null;
		
		public AriaData()
		{
			
		}
		
		public bool DisplayOutput
		{
			set { blnDisplayOutput = value;}
			get { return blnDisplayOutput; }
		}
		
		public bool Is_Linux
		{
			set { blnLinux = value; }
			get { return blnLinux; }
		}
		
		public string HTTP_Proxy
		{
			set { strParam += string.Format(" --http-proxy='{0}' ",value); }
		}
		
		public string HTTP_Username
		{
			set { strParam += String.Format(" --http-user='{0}' ",value); }
		}
		
		public string HTTP_Password
		{
			set { strParam += String.Format(" --http-passwd='{0}' ",value); }
		}
		
		public int Connections
		{
			set { strParam += String.Format(" -x {0} " ,value); }
		}
		
		public string File_Name
		{
			set { 
				strParam += String.Format(" -o {0} " ,value); 
				strFilename = value; 
			}
			get { return strFilename; }
		}
		
		public string File_Path
		{
			set { 	
				strParam += String.Format(" -d {0} " ,value); 
				strFilepath = value;
			}
			get { return strFilepath; }
		}
		
		public string Full_File_Path
		{
			get { return (strFilepath + System.IO.Path.DirectorySeparatorChar + strFilename).Trim(); }
		}
		
		public string URL
		{
			set { strParam += " " + value + " "; }	
		}		
		
		public string Arguments
		{
			get { return strParam; }
			set { strParam += " " + value + " "; }
		}
		
		public string Application_Name
		{
			set { strAppName = value; }
			get { return strAppName; }
		}
		
		public string Application_Path
		{
			set { strAppPath = value; }
			get { return strAppPath; }
		}
		
		public bool Is_Asynchronous
		{
			set { blnAsync = value; }
			get { return blnAsync; }
		}
		
		public bool Delete_Existing
		{
			set { blnDeleteExisting = value; }
			get { return blnDeleteExisting; }
		}
		/*
		public Form Invoker
		{
			set { frmInvoker = value; }
			get { return frmInvoker; }
		}
		*/
	}
}