/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
namespace Cube
{
	public class Debian
	{
		public Debian ()
		{
		}
		
		public static string GetName(string strDepend)
        {
            if (!strDepend.Contains("("))
                return strDepend.Trim();
            return strDepend.Remove(strDepend.IndexOf('(')).Trim();
        }

        public static string GetVersion(string strDepend)
        {
            strDepend = strDepend.Trim();
            string strVer = "";
            if (strDepend.Contains("("))
            {
                strVer = strDepend.Split(" ".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries)[1].Replace("(", "").Replace(")", "").Trim();
                strVer = strVer.Split(' ')[1];
            }
            return strVer;
        }

        public static string GetEqualityOperation(string strDepend)
        {
            strDepend = strDepend.Trim();
            string strVer = "";
            if (strDepend.Contains("("))
            {
                strVer = strDepend.Split(" ".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries)[1].Replace("(", "").Replace(")", "").Trim();
                strVer = strVer.Split(' ')[0];
            }
            return strVer;
        }
	}
}

