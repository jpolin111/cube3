/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Collections.Generic;

namespace Cube
{
	class CubeSourcesManager
	{
		string strSourceListPath;
		string strArchitecture;
		string strOtherListDir;		
		List<CubeSource> lstSources = new List<CubeSource>();
		List<CubeReleaseGPG> lstReleases = new List<CubeReleaseGPG>();
		List<CubeAptPreferencesConstraints> constraints = new List<CubeAptPreferencesConstraints> ();
		
		public CubeSourcesManager(string listPath, string strOtherListDir, string arch)
		{
			this.strSourceListPath = listPath;
			this.strOtherListDir = strOtherListDir;
			this.strArchitecture = arch;
		}

		public bool ScanAptPreferences()
		{
			constraints = new List<CubeAptPreferencesConstraints> ();
			//if (!File.Exists (strOtherListDir + "preferences"))
			//	return false;

			foreach (string strPref in Directory.GetFiles(strOtherListDir,"*pref*")) {

				StreamReader reader = new StreamReader (strPref);
				string pac = null, pin = null, pri = null;

				while (!reader.EndOfStream) {
					string sl = reader.ReadLine ();

					if (sl.Contains ("Package: "))
						pac = sl.Replace ("Package: ", "").Trim ();
					else if (sl.StartsWith ("Pin: "))
						pin = sl.Replace ("Pin: ", "").Trim ();
					else if (sl.StartsWith ("Pin-Priority: "))
						pri = sl.Replace ("Pin-Priority: ", "").Trim ();
					else if (sl.Trim ().Length == 0) {
						if (pin != null && pac != null && pri != null)
							constraints.Add (new CubeAptPreferencesConstraints (pac, pin, pri));
						pac = null;
						pin = null;
						pri = null;
					}
					 
					constraints.Sort (new ComparerConstraints ());
				}
				if (pin != null && pac != null && pri != null) {
					constraints.Add (new CubeAptPreferencesConstraints (pac, pin, pri));
					constraints.Sort (new ComparerConstraints ());
				}
			}
			return true;
		}
		
		public bool ScanAllSources()
		{
			dic = new Dictionary<string, string> ();
			if(!ScanSources(strSourceListPath))
				return false;
			
			foreach(string strList in Directory.GetFiles(strOtherListDir,"*.list"))
			{
				if(!ScanSources(strList))
					return false;
			}			
			return true;
		}

		Dictionary<string,string> dic = new Dictionary<string, string> ();
		Dictionary<string,string> dicR = new Dictionary<string, string>();
		public bool ScanSources(string source)
		{
			if(!File.Exists(source))
				return false;
		
			StreamReader reader = new StreamReader(source);
			CubeReleaseGPG rel = new CubeReleaseGPG();
			
			while(!reader.EndOfStream)
			{
				string strSource = reader.ReadLine().Trim();
				
				if(strSource.StartsWith("#"))
					continue;
				if(!(strSource.Length > 0))
					continue;
				if(strSource.StartsWith("deb-src"))
					continue;
				
				if(strSource.Contains("#"))
					strSource = strSource.Split('#')[0].Trim();
				
				strSource = strSource.Replace("deb ", "").Replace("\"","").Trim();
				
				strSource = strSource.Replace("http://","").Trim();	
				
				string strMainURL = strSource.Split(' ')[0];
				if(!strSource.Trim().Equals(strMainURL.Trim()))
					strSource = strSource.Replace(strMainURL,"").Trim();
				string[] strSourceSplit = strSource.Split(" ".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
				
				if(!strMainURL.EndsWith("/"))
					strMainURL += "/";

				string strMainSource = strMainURL + "dists/" + strSourceSplit[0];
				string strRelease = strSourceSplit[0];
				
				if(!dicR.ContainsKey(strMainSource))
				{
					dicR.Add(strMainSource,"");
					rel = new CubeReleaseGPG(strMainSource);
					lstReleases.Add(rel);
				}
								
				for(int i = 1 ; i < strSourceSplit.Length ; i++)
				{										
					strSource = "http://"+strMainSource + "/" + strSourceSplit[i] + "/" + this.strArchitecture + "/Packages.gz";
					if(!dic.ContainsKey(strSource))
					{
						dic.Add(strSource,"");
						lstSources.Add(new CubeSource(strSource,"http://"+strMainURL,strRelease,rel.ReleaseFilename));
					}
				}
			}

			reader.Close();
			return true;
		}

		public void SortSources()
		{
			List<CubeSource> lstNewSources = new List<CubeSource> ();

			foreach (CubeAptPreferencesConstraints c in constraints) 
			{
				for(int i=0;i<lstSources.Count;i++)				
				{								
					CubeSource s = lstSources[i];
					if(c.Pin.Contains("Release"))
					{
						if(string.Compare(s.Release.Trim(),c.Pin.Split(' ')[1].Trim(),true) ==  0)
						{				
							s.Constraint = c;
							if(!lstNewSources.Contains(s))
								lstNewSources.Add(s);
							lstSources.RemoveAt(i);
							i--;
						}
					}
					else if(c.Pin.Contains("Origin"))
					{
						if (s.PPA == c.Pin.Split (' ') [1].Trim ()) {
							s.Constraint = c;
							if(!lstNewSources.Contains(s))
								lstNewSources.Add(s);
							lstSources.RemoveAt(i);
							i--;
						}
						else if(s.Origin.ToLower().Contains(c.Pin.Split(' ')[1]))
						{
							s.Constraint = c;
							if(!lstNewSources.Contains(s))
								lstNewSources.Add(s);
							lstSources.RemoveAt(i);
							i--;
						}
					}
					else if(c.Pin.Contains("Origin-URL"))
					{
						if(string.Compare(s.Origin.Trim(),c.Pin.Split(' ')[1].Trim(),true) ==  0)
						{
							s.Constraint = c;
							if(!lstNewSources.Contains(s))
							lstNewSources.Add(s);
							lstSources.RemoveAt(i);
							i--;
						}
					}
				}
			}
			
			for(int i=0;i<lstSources.Count;i++)
			{
				CubeSource s = lstSources[i];
				if(s.Release.Contains("updates"))
				{					
					if(!lstNewSources.Contains(s))
						lstNewSources.Add(s);
					lstSources.RemoveAt(i);
					i--;
				}
			}

			if (lstSources.Count > 0) 
				lstNewSources.AddRange (lstSources.ToArray());

			lstSources.Clear ();
			lstSources.InsertRange (0, lstNewSources);			
		}
		
		public List<CubeSource> SourcesList
		{
			get { return lstSources; }
		}

		public CubeAptPreferencesConstraints[] Constraints
		{
			get { return constraints.ToArray(); }
		}
		
		public CubeSource[] Sources
		{
			get { return lstSources.ToArray(); }
		}
		
		public CubeReleaseGPG[] Releases
		{
			get { return lstReleases.ToArray(); }
		}
		
		public CubeSource AddSource
        {
			set { lstSources.Add(value); }
        }
	}

}

