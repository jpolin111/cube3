/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Collections.Generic;

namespace Cube
{	
	public class CubeAptPreferencesConstraints
	{
		List<string> lstPackages = new List<string>();
		string strPin;
		int intPriority;

		public CubeAptPreferencesConstraints(string strPackage, string strPin, string strPrior)
		{
			strPackage = strPackage.Trim ();
			string[] split;
			if (strPackage.Contains (" ")) {
				split = strPackage.Split (' ');
				foreach (string s in split)
					lstPackages.Add (s.Trim ());				
			} else
				lstPackages.Add (strPackage);

			if (strPin.Contains ("release o=")) {
				strPin = "Origin " + strPin.Replace ("release o=", "").Trim ();
				if (strPin.Contains ("LP-PPA")) //Convert to Launchpad PPA address
					strPin = strPin.Replace ("LP-PPA", "ppa.launchpad.net");
			}
			else if (strPin.Contains ("release a="))
				strPin = "Release " + strPin.Replace ("release a=", "").Trim ();
			else if (strPin.Contains ("origin"))
				strPin = "Origin-URL " + strPin.Replace ("origin", "").Trim ();

			this.strPin = strPin;

			this.intPriority = Convert.ToInt32 (strPrior.Trim ());
		}

		public string[] Packages
		{
			get { return lstPackages.ToArray(); }
		}

		public string Pin
		{
			get { return strPin; }
		}

		public int Priority
		{
			get { return intPriority; }
		}
	}
}

