/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
namespace Cube
{
	public class CubeSource
	{
		string strLink;
		string strFilename;
		string strMainUrl;
		string strRelease;
		string strOrigin;		
		string strReleaseFilename;
		string strPPA;
		
		CubeAptPreferencesConstraints constraint;

		public CubeSource (string lnk, string main, bool bln)
		{
			this.strLink = lnk;
			this.strMainUrl = main;
			this.strFilename = lnk.Replace("http://", "").Replace("http:/", "").Replace(".gz", "").Replace("/", "_").Trim();
		}
		
		public CubeSource(string lnk, string main, string rel,string releaseFilename)
		{
			this.strLink = lnk;
			this.strMainUrl = main;
			this.strFilename = lnk.Replace("http://", "").Replace("http:/", "").Replace(".gz", "").Replace("/", "_").Trim();
			this.strRelease = rel;
			this.strOrigin = lnk.Replace ("http://", "").Replace ("http:/", "").Split ('/') [0];
			this.strReleaseFilename = releaseFilename;
			this.strPPA = lnk.Replace ("http://", "").Replace ("http:/", "").Split (new string[]{ "/ubuntu/dists" }, StringSplitOptions.RemoveEmptyEntries) [0].Replace("/","-").Trim ();
		}
		
		public CubeSource(string lnk, string fname)
		{
			this.strLink = lnk;
			this.strFilename = fname;
		}

		public string PPA
		{
			get { return strPPA; }
		}

		public string ReleaseFilename
		{
			get {return strReleaseFilename;}
		}
		
		public string Link
		{
			get { return strLink; }
		}
		
		public string Filename
		{
			get { return strFilename; }
		}
		
		public string URL
		{
			get { return strMainUrl; }
		}

		public string Release
		{
			get { return strRelease; }
		}

		public string Origin
		{
			get { return strOrigin; }
		}

		public CubeAptPreferencesConstraints Constraint
		{
			set { this.constraint = value; }
			get { return constraint; }
		}
	}
}

