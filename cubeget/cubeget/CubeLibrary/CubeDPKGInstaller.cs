/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Diagnostics;
using System.Threading;
using System.IO;
using Mono.Unix;

namespace Cube
{
	public class CubeDPKGInstaller
	{		
		CubePackage[] packages;
		CubeConfiguration config = new CubeConfiguration();
				
		public event EventHandler Finished;		
		
		string strParams = "-i";
		string packageDirectory;
		string output_file = "";
		
		string error_message = "";
		bool blnSuccess = false;
		int type = 1; //type 1 = installation error , type 2 = terminal error
		
		public CubeDPKGInstaller (CubePackage[] packages, string packageDirectory)
		{
			this.packages = packages;
			this.packageDirectory = packageDirectory;
			Initialize();
		}
		
		public void Initialize()
		{
			Array.Reverse(packages);
			
			string strParamPath = "";
			foreach(CubePackage p in packages)
			{
				strParamPath += " '" + Directory.GetCurrentDirectory() + config.Slash + p.GetValue("Filepath") + "'";
			}			
			
			string extra_params = CubeSConfigurationManager.GetValue("config","dpkg-parameters");
			if(extra_params!=null)
				strParams += " " + extra_params;
			
			strParams += strParamPath;
		}
		
		public void StartInstallation()
		{
			Thread t = new Thread(Start_Thread);
			t.Start();
		}
		
		private void Start_Thread()
		{						
			string fcommand = Directory.GetCurrentDirectory() + config.Slash + config.TemporaryFilesDirectory + "command.sh";
			string fexitRes = Directory.GetCurrentDirectory() + config.Slash + config.LogsDirectory + "exitcode.cube";
			output_file = Directory.GetCurrentDirectory() + config.Slash + config.LogsDirectory + "cube-install-"+DateTime.Now.ToLongDateString()+"-"+DateTime.Now.ToLongTimeString() + ".log";
			output_file = output_file.Replace("@","").Replace(" ","");
			
			StreamWriter write = new StreamWriter(fcommand);
			write.WriteLine("dpkg " + strParams + " | tee \""+output_file+"\"");			
			write.WriteLine("echo $? >> "+fexitRes);
			write.Close();
			
			ProcessOutput.Run("chmod","+x "+fcommand);
			
			if(File.Exists(packageDirectory+"lock"))
			{
				try{
					File.Delete(packageDirectory+"lock");
				}catch{}
			}
				
			//ProcessOutput.Run("gksu","--description=\"Cube DPKG Terminal Install\" " + config.BinDirectory+"cube-mono " + config.BinDirectory+"cube-install.exe sh "+fcommand);
			
			string term = UbuntuEssentials.FindTerminal();
			if(term == null)
			{
				type = 2;
				error_message = Catalog.GetString("Unable to find terminal to be used. Please check for terminals (ie. gnome-terminal) installed on your computer and add it on cube/data/config/config.cube under \"installer-terminals\" key.");
				if(Finished !=null) Finished(this,EventArgs.Empty);				
				return;
			}
			
			/*
			string cde_path = config.CDEPath;
			if(cde_path!=null)
			{
				Console.WriteLine("[Cube] Installing using CDE redirect");
				ProcessOutput.Run(cde_path,"-f " + config.RootCommand + " " + term + " -e \" "+fcommand+"\"");
				
			}
			else*/
				ProcessOutput.Run(config.RootCommand, term + " -e \" "+fcommand+"\"");
			
			try
			{				
				if(File.Exists(fexitRes))
				{					
					int exitCode = -1;
					if(int.TryParse(File.ReadAllText(fexitRes).Trim(),out exitCode))
					{
						if(exitCode == 0)
							blnSuccess = true;
						else
							error_message = Catalog.GetString("An error occured during the installation. Exit Code : ")+exitCode+".";
					}
					else
						error_message = Catalog.GetString("Unable to parse exit code file.");
				}
				else
					error_message = Catalog.GetString("Unable to determine result.");
				
			}catch(Exception ex) {
				error_message = Catalog.GetString("Cube Internal Error. Unable to connect to external application. ")+ex.Message;
			}
			
			try{ File.Delete(fcommand); }catch{}
			try{ File.Delete(fexitRes); }catch{}
			
			if(Finished !=null) Finished(this,EventArgs.Empty);
		}
		
		public string ErrorMessage
		{
			get { return this.error_message; }
		}
		
		public bool Success
		{
			get { return this.blnSuccess; }
		}
		
		public int ErrorType
		{
			get { return this.type; }
		}
		
		public string OutputFilePath
		{
			get { return output_file; }
		}
	}
}