/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;

namespace Cube
{
	public class CubeProject
	{
		string strProjName;
		string strOS;
		string strArch;
		string strDateCreated;
		
		string strProjParentDir;
		string strProjDir;		
		string strCompName;	
		
		string strRelease;
		string strCodeName;
		string strDistribution;
		string strVersion;
		
		public CubeProject ()
		{
			
		}
		
		public string ComputerName
		{
			set { strCompName = value; }
			get { return strCompName; }
		}
		
		public string ProjectName
		{
			set { strProjName = value;}
			get { return strProjName; }
		}
			
		public string OperatingSystem
		{
			set { strOS = value; }
			get { return strOS; }
		}
		
		public string Architecture
		{
			set { strArch = value ; }
			get { return strArch; }
		}
		
		public string DateCreated
		{
			set { strDateCreated = value; }
			get { return strDateCreated ; }
		}
		
		public string ProjectDirectory
		{
			get { return strProjDir; }
		}
		
		public string ProjectParentDirectory
		{
			set { 
				strProjParentDir = value;
				strProjDir = value + strProjName + Path.DirectorySeparatorChar;
			}
			get { return strProjParentDir; }
		}
		
		public string DataDirectory
		{
			get { return strProjDir + "data" + Path.DirectorySeparatorChar; }
		}
		
		public string ListDirectory
		{
			get { return DataDirectory + "list" + Path.DirectorySeparatorChar; }
		}
		
		public string AptDirectory
		{
			get { return DataDirectory + "apt" + Path.DirectorySeparatorChar; }
		}
		
		public string PackagesDirectory
		{
			get { return DataDirectory + "packages" + Path.DirectorySeparatorChar; }
		}
		
		public string SharedPackagesDirectory
		{
			get { return DataDirectory + "app-share-packages" + Path.DirectorySeparatorChar; }
		}
		
		public string ScreenshotsDirectory
		{
			get { return DataDirectory + "screenshots" + Path.DirectorySeparatorChar; }
		}
		
		public string Distribution
		{
			set { strDistribution = value; }
			get { return strDistribution; }
		}
		
		public string Release
		{
			set { strRelease = value; }
			get { return strRelease; }
		}
		
		public string CodeName
		{
			set { strCodeName = value; }
			get { return strCodeName; }
		}
		
		public string ProjectVersion
		{
			set { strVersion = value; }
			get { return strVersion; }
		}
	}
}

