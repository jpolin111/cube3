/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Collections.Generic;
namespace Cube
{
	public class CubePackage
    {
		private Dictionary<string, string> packageDic = new Dictionary<string, string>();
		
		public string GetValue(string strKey)
        {
            string strTemp = "";
            packageDic.TryGetValue(strKey, out strTemp);
            return strTemp;
        }
		
		public void AddToPackageInfo(string strKey, string strVal)
        {
            packageDic.Add(strKey, strVal);
        }
		
		public void UpdateValue(string strKey, string strVal)
		{			
			if(packageDic.ContainsKey(strKey))
				packageDic.Remove(strKey);
			packageDic.Add(strKey,strVal);
		}
		
		public Dictionary<string, string> PackageDictionary
		{
			get { return packageDic; }
		}
    }

}

