/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Collections.Generic;

namespace Cube
{
	public class CubeSystemUpdater
	{
		CubeProject p;
		bool debug = true;
		
		public event EventHandler ProgressMaxChanged;		
		public event EventHandler ProgressChanged;		
		
		CubeConfiguration config = new CubeConfiguration();
		
		int intMax = 0;
		
		public CubeSystemUpdater (CubeProject p)
		{
			this.p = p;
		}
		
		public int UpdateCube()
		{
			CubeConfiguration config = new CubeConfiguration();
			if(ProgressMaxChanged!=null) ProgressMaxChanged(1,EventArgs.Empty);
			if(config.ComputerName != "root")
			{
				if(p.ComputerName != config.ComputerName)
					return -1;
				else if (p.OperatingSystem.Trim() != config.OperatingSystem.Trim())
					return -1;
			}
			else if(!Directory.Exists(UbuntuEssentials.UbuntuHomeDirectory+p.ComputerName))
				return -1;			
			else if (p.OperatingSystem.Trim() != config.OperatingSystem.Trim())
				return -1;
			
			if(!CopyStatusFile())
				return -2;
			if(ProgressMaxChanged!=null) ProgressMaxChanged(1,EventArgs.Empty);
			return 0;
		}
		
		public int UpdateSystem()
		{
			CubeConfiguration config = new CubeConfiguration();			
			
			if(!Directory.Exists(UbuntuEssentials.UbuntuHomeDirectory+p.ComputerName))
				return -1;
			
			if(p.OperatingSystem.Trim() != config.OperatingSystem.Trim())
				return -1;
			
			if(config.ComputerName != p.ComputerName)
				return -1;
			
			
			if(!CopyAllBackToSystem())
				return -3;			
			
			/*
			if(!CopySourceFile())
				return -4;
				*/
			
			if(ProgressChanged!=null) ProgressChanged(intMax+1,EventArgs.Empty);
			
			/*
			if(!CopyAptPreferenceFile())
				return -5;			
				*/
			
			if(ProgressChanged!=null) ProgressChanged(intMax+2,EventArgs.Empty);
			
			//ChangeDirectoryPermission("/etc/apt","+rw");
			
			return 0;
		}
		
		private bool CopyStatusFile()
		{
			if(debug) Console.Write("[Cube] Copying Status File...");
			try
			{
				File.Copy(UbuntuEssentials.StatusFilePath,p.ListDirectory+"status",true);
				ChangePermission(p.ListDirectory+"status");
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("OK");
			return true;
		}
		
		public bool CopyAllBackToSystem()
		{
			if(debug) Console.WriteLine("[Cube] Copying Package List...");		
			
			List<string> lstReps = new List<string>();
			
			lstReps.AddRange(Directory.GetFiles(p.ListDirectory,"*Packages"));
			lstReps.AddRange(Directory.GetFiles(p.ListDirectory,"*Release"));
			lstReps.AddRange(Directory.GetFiles(p.ListDirectory,"*Release.gpg"));
			intMax = lstReps.Count;
			if(ProgressMaxChanged!=null) ProgressMaxChanged(lstReps.Count+2,EventArgs.Empty);			
			
			string copy_format = "cp -f -v \"{0}\" \"{1}\"";
			string copy_command_file = Directory.GetCurrentDirectory() + config.Slash + config.TemporaryFilesDirectory + "copy_to_system.sh";
			
			if(File.Exists(copy_command_file))
				File.Delete(copy_command_file);
			
			StreamWriter writer = new StreamWriter(copy_command_file);
			
			//Copy all list files back to system
			foreach(string strListFile in lstReps)
			{				
				string strSysFile = UbuntuEssentials.PackageListDirectoryPath+"/"+(new FileInfo(strListFile).Name);
				string strListFileComplete = Directory.GetCurrentDirectory() + config.Slash + strListFile;
				
				writer.WriteLine(string.Format(copy_format,strListFileComplete,strSysFile));				
			}
			
			//Copy sources.list and other sources back to system
			string sources_list = Directory.GetCurrentDirectory() + config.Slash + p.AptDirectory+"sources.list";
			writer.WriteLine(string.Format(copy_format,sources_list,UbuntuEssentials.SourcesListFilePath));
			
			foreach(string strList in Directory.GetFiles(p.AptDirectory,"*.list"))
			{
				string strListComplete = Directory.GetCurrentDirectory() + config.Slash + strList;
				writer.WriteLine(string.Format(copy_format,strListComplete,UbuntuEssentials.OtherSourcesListDirectoryPath + new FileInfo(strList).Name));
			}
			
			//Copy apt-preferences file back to system			
			if(File.Exists(p.AptDirectory+"preferences"))
			{
				string apt_preferences = Directory.GetCurrentDirectory() + config.Slash + p.AptDirectory + "preferences";
				writer.WriteLine(string.Format(copy_format,apt_preferences,UbuntuEssentials.AptPreferencesFilePath));
			}
			
			//Make system apt folder read and writer
			writer.WriteLine("chmod -R +rw /etc/apt");			
			
			writer.Close();
			
			ChangePermission(copy_command_file,"+x");
			ProcessOutput.Run(config.RootCommand,copy_command_file);
			
			if(debug) Console.WriteLine("[Cube] Copying Package List Complete\n");
			return true;
		}
		
		/*
		private bool CopySourceFile()
		{
			if(StatusChanged!=null) StatusChanged("Copying Source File",EventArgs.Empty);
			if(debug) Console.Write("Copying Source File...");
			try
			{				
				File.Copy(p.AptDirectory+"sources.list",UbuntuEssentials.SourcesListFilePath,true);
				foreach(string strList in Directory.GetFiles(p.AptDirectory,"*.list"))
				{
					File.Copy(strList, UbuntuEssentials.OtherSourcesListDirectoryPath + new FileInfo(strList).Name,true);
				}
				ChangePermission(UbuntuEssentials.SourcesListFilePath);
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("[Cube] FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("[Cube] OK");
			return true;
		}
		
		private bool CopyAptPreferenceFile()
		{
			if(StatusChanged!=null) StatusChanged("Copying Apt Preference File...",EventArgs.Empty);
			if(debug) Console.Write("Copying Apt Preference File...");
			try
			{				
				if(File.Exists(p.AptDirectory+"preferences"))
				{
					File.Copy(p.AptDirectory+"preferences",UbuntuEssentials.AptPreferencesFilePath,true);
					ChangePermission(UbuntuEssentials.AptPreferencesFilePath);
				}
				else
					if(debug) Console.Write("Not exists. ");
			}
			catch(Exception ex)
			{
				if(debug) Console.WriteLine("[Cube] FAILED! " +ex.Message);
				return false;
			}
			if(debug) Console.WriteLine("[Cube] OK");
			return true;
		}
		
		*/
		
		private void ChangePermission(string file)
		{
			try
			{
				CubeConfiguration config = new CubeConfiguration();
				if(config.ComputerName == "root")
					System.Diagnostics.Process.Start("sudo","chmod +rw \"" + file+"\"");
				else
					System.Diagnostics.Process.Start("chmod","+rw " + file);
			}
			catch{}
		}
		
		private void ChangePermission(string file,string mode)
		{
			try
			{
				CubeConfiguration config = new CubeConfiguration();
				if(config.ComputerName == "root")
					System.Diagnostics.Process.Start("sudo","chmod " + mode + " \"" + file +"\"");
				else
					System.Diagnostics.Process.Start("chmod",mode + " \"" + file +"\"");
			}
			catch{}
		}
		
		private void ChangeDirectoryPermission(string dir, string mode)
		{
			try
			{
				CubeConfiguration config = new CubeConfiguration();
				if(config.ComputerName == "root")
					System.Diagnostics.Process.Start("sudo","chmod -R " + mode + " \"" + dir+"\"");
				else
					System.Diagnostics.Process.Start("chmod","-R "+ mode + " " + dir+"\"");
			}
			catch{}
		}
	}
}

