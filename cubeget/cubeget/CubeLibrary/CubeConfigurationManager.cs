/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using Cube;
using System.Collections.Generic;

namespace Cube
{
	public class CubeConfigurationManager
	{
		string configFullPath;
		string configName;
		string configDir;
		SortedList<string,string> configData;
		
		public CubeConfigurationManager (string configDir)
		{
			this.configDir = configDir;
		}
		
		public bool CreateConfiguration(string configName)
		{
			this.configName = configName;
			configFullPath = this.configDir + Path.DirectorySeparatorChar + this.configName+ ".cube";
			
			try
			{
			if(File.Exists(configFullPath))
				File.Delete(configFullPath);
			}catch
			{
				return false;
			}
			
			StreamWriter writer = new StreamWriter(configFullPath,true);
			writer.WriteLine("#Cube Configuration File");
			writer.Close();
			return true;
		}
		
		public bool OpenConfiguration(string name)
		{
			this.configName = name;
			configData = new SortedList<string, string>();
			configFullPath = this.configDir + Path.DirectorySeparatorChar +name+ ".cube";
			if(!File.Exists(configFullPath))
				return false;
			
			StreamReader reader = new StreamReader(configFullPath);
			while(!reader.EndOfStream)
			{
				string line = reader.ReadLine().Trim();
				if(line.StartsWith("#") || line.Length==0)
					continue;
				
				string[] lineSplit = line.Split('\t');
				if(lineSplit.Length>1)
				{
					string key = lineSplit[0].Trim();
					string value = "";
					for(int i = 1; i<lineSplit.Length; i++)
						value += lineSplit[i].Trim();
					
					if(!configData.ContainsKey(key))
						configData.Add(key,value);
				}
			}
			reader.Close();
			return true;
		}
		
		public string GetValue(string key)
		{
			if(configData==null)
				return null;
			if(configData.ContainsKey(key))
				return configData[key];
			else
				return null;
		}
		
		public bool AddValue(string key,string value,bool replace)
		{
			if(configData==null)
				return false;
			
			if(configData.ContainsKey(key))
			{
				if(replace)
					configData.Remove(key);
				else
					return false;
			}			
			
			configData.Add(key,value);			
			return true;
		}
		
		public bool RemoveKey(string key)
		{
			if(configData==null)
				return false;
			
			if(configData.ContainsKey(key))
			{
				configData.Remove(key);
			}
			return true;
		}
		
		public bool Save()
		{
			if(configData==null)
				return false;
			
			if(CreateConfiguration(this.configName))
			{
				StreamWriter writer = new StreamWriter(configFullPath,true);
				for(int i = 0; i < configData.Keys.Count; i++)
					writer.WriteLine("{0}\t{1}",configData.Keys[i],configData.Values[i]);
				writer.Close();
				return true;
			}
			return false;
		}
		
		public void Close()
		{
			if(configData==null)
				return;
			
			configData.Clear();
		}
	}
	
	public class CubeSConfigurationManager
	{		
		public static string GetValue(string configName, string key)
		{
			CubeConfiguration config = new CubeConfiguration();
			CubeConfigurationManager cmgr = new CubeConfigurationManager(config.ConfigurationDirectory);
			if(!cmgr.OpenConfiguration(configName))
				return null;			
			return cmgr.GetValue(key);
		}
		
		public static bool SetValue(string configName, string key, string value)
		{
			CubeConfiguration config = new CubeConfiguration();
			CubeConfigurationManager cmgr = new CubeConfigurationManager(config.ConfigurationDirectory);
			if(!cmgr.OpenConfiguration(configName))
				return false;			
			cmgr.AddValue(key,value,true);
			return cmgr.Save();
		}
		
		public static bool CreateConfiguration(string configName)
		{
			CubeConfiguration config = new CubeConfiguration();
			CubeConfigurationManager cmgr = new CubeConfigurationManager(config.ConfigurationDirectory);
			return cmgr.CreateConfiguration(configName);				
		}		
	}
}

