/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.IO.Compression;
namespace Cube
{
	public class CubeDecompress
	{
		public static bool Decompress(string strOrig, string strTarget)
		{
			if(!File.Exists(strOrig))
				return false;
			if(File.Exists(strTarget))
			{
				try{ File.Delete(strTarget); }
				catch{return false;}
			}
			
			try
			{
				FileInfo fileToDecompress = new FileInfo(strOrig);
				using (FileStream originalFileStream = fileToDecompress.OpenRead())
	            {					
					string newFileName = strTarget;
					using (FileStream decompressedFileStream = File.Create(newFileName))
	                {
    	                using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
        	            {
            	            decompressionStream.CopyTo(decompressedFileStream);
                	        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
							return true;
	                    }
    	            }
				}
			}
			catch{}
			return false;
		}
	}
}
