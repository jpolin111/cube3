/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Collections.Generic;
using o = System.Console;
using Cube;
using Mono.Unix;

namespace Cube
{
	public class CubeProjectManager
	{
		public event EventHandler ProgressMaxChanged;
		public event EventHandler StatusChanged;
		public event EventHandler ProgressChanged;
		//public event EventHandler ErrorOccured;
		
		CubeProject proj;
		CubeProcess proc;
		CubeSourcesManager srcmgr;			

		SortedList<string, CubePackage> dicInstalled = new SortedList<string, CubePackage>();
		SortedList<string, CubePackage> dicAvailable = new SortedList<string, CubePackage>();
		
		string[] strAcceptedCategories = new string[]
		{
        	"Package","Version","Description","Depends","Filename","Size","Installed-Size","Recommends","Pre-Depends","MD5sum","Section","Status"
		};
		
		bool debug = false; //Set true to be verbose
		bool rdepends = false ; //Set true to check reverse dependencies
		bool hostcompatible = false;
		string strToFind = "";
		
		public CubeProjectManager(CubeProject p, CubeProcess proc)
		{
			this.proc = proc;
			this.hostcompatible = proc.HostAndProjectCompatible;
			this.proj = p;			
			
			ReadList();
			
			rdepends = GetEnableReverseDependency();
			
			InitializeProjectDirectories();
		}
		
		public bool GetEnableReverseDependency()
		{
			string isrdep = CubeSConfigurationManager.GetValue("config","enable-reverse-dependency-check");
			if(isrdep != null)
				if(isrdep == "true")
				{					
					Console.WriteLine("[Cube] Reverse Dependency Checking enabled.");
					return true;
				}
			return false;
		}
		
		public CubeProjectManager(CubeProject p)
		{			
			this.proj = p;			
			ReadList();
			rdepends = GetEnableReverseDependency();			
			InitializeProjectDirectories();
		}
		
		public void InitializeProjectDirectories()
		{
			//Create missing directories			
			CubeConfiguration config = new CubeConfiguration();
			if(!Directory.Exists(proj.PackagesDirectory))
			{				
				Directory.CreateDirectory(proj.PackagesDirectory);				
			}
			if(!Directory.Exists(proj.SharedPackagesDirectory))
			{
				Directory.CreateDirectory(proj.SharedPackagesDirectory);
			}
			if(!Directory.Exists(proj.ListDirectory+config.Slash+"temp"))
			{				
				Directory.CreateDirectory(proj.ListDirectory+config.Slash+"temp");			
			}
		}
		
		public void ReadList()
		{
			//Get all available sources
			srcmgr = new CubeSourcesManager(this.proj.AptDirectory+"sources.list",this.proj.AptDirectory,this.proj.Architecture);
			srcmgr.ScanAllSources();
			//If there is a constraint/rule in apt preferences, sort the sources according to priority
			if(srcmgr.ScanAptPreferences())
				srcmgr.SortSources();
			
			//Add the "status" or the list of installed packages in the sources
			srcmgr.AddSource = new CubeSource("","status");
			
			if(this.hostcompatible)
			{
				Console.WriteLine("[Cube Project] Project and Host Compatible. Now adding Host status to repositories");
				srcmgr.AddSource = new CubeSource("","status-host");
			}
		}
		
		public void ScanList()
		{
			ReadList();
			DebianCompare cmp = new DebianCompare();

			if(ProgressMaxChanged!=null) ProgressMaxChanged(srcmgr.Sources.Length,EventArgs.Empty);
				
			int intSourcesCounter = 0;
			if(StatusChanged!=null) StatusChanged(Catalog.GetString("Reading project repositories..."),EventArgs.Empty);
			bool blnSkipCons = false; //set true to skip specific apt preference constraint (Preferences)
			bool blnForce = false; //set true to force a package to be inserted in the list. Highest priority (Preferences)
			
			dicInstalled.Clear();
			dicAvailable.Clear();			
			
			dicInstalled = new SortedList<string, CubePackage>();
			dicAvailable = new SortedList<string, CubePackage>();

			foreach(CubeSource s in srcmgr.Sources)
			{				
				CubePackage p = new CubePackage();
				string strCurrentListPath = proj.ListDirectory + s.Filename;
				if(s.Filename == "status-host")
				{
					strCurrentListPath = UbuntuEssentials.StatusFilePath;
					Console.WriteLine("[Cube Project] Host's status file is being scanned");
				}
				
				if(debug) o.Write("[SOURCES MGR] Loading {0}...",s.Link);
				
				/*
				if(s.Filename.Contains("backports"))
				{
					if(debug) o.WriteLine("[SOURCES MGR] Skipped");
					intSourcesCounter++;
					continue;
				}
				*/
				
				if(!File.Exists(strCurrentListPath))
				{
					if(debug) o.WriteLine("[SOURCES MGR] FAILED : Unable to locate file " + strCurrentListPath);
					intSourcesCounter++;
					continue;
				}
				
				bool blnSkip = false; //set true to skip not installed package in status file to be added on the list
				bool blnRemoveStatus = false; //set true to remove the 'status' key entry (to prevent duplication)
				blnSkipCons = false; 

				if(debug) o.Write("[PACKAGE MGR] Getting all lines...");
				string[] strLines = File.ReadAllLines(strCurrentListPath); //Get bulk of lines for each list file
				if(debug) o.WriteLine("OK");
				
				//foreach entries in the list file
				foreach(string strLine in strLines)
				{
					//Init
					blnSkipCons = false;		
					blnForce = false;
					
					if (strLine.StartsWith(" "))
						continue;
					
					//Parse the line to extract record in the file.
					string[] strSplit = strLine.Split(":".ToCharArray(), 2);
                    if (CheckIfIncluded(strSplit[0].Trim()))
                    {		
						//If the package in the 'status' file status' is not-installed
						//Mark it to be skipped, not going to be added to the installed list
						if(strSplit[0].Trim() == "Status" && s.Filename.StartsWith("status"))
						{
							if(!strSplit[1].Trim().Contains("installed"))
								blnSkip = true;
							else
								blnRemoveStatus = true;
						}
						else if(strSplit[0].Trim() == "Filename")
							p.AddToPackageInfo(strSplit[0].Trim(), s.URL+strSplit[1].Replace("\'", "").Trim());
						else
							p.AddToPackageInfo(strSplit[0].Trim(), strSplit[1].Replace("\'", "").Trim());
					}
										
					if (strLine.Trim().Length == 0) //Save package here
					{					
						if(s.Filename.StartsWith("status") && !blnSkip) //Installed List, Skip = false
						{
							if(blnRemoveStatus) //Remove 'Status' key duplication
							{
								if(p.PackageDictionary.ContainsKey("Status"))
									p.PackageDictionary.Remove("Status");
								blnRemoveStatus = false;
							}
							try
							{	
                            	if(s.Filename == "status")
								{
                                	dicInstalled.Add(p.GetValue("Package"), p);
									dicInstalled[p.GetValue("Package")].AddToPackageInfo("Installed","True");
								}
								else if(s.Filename == "status-host")
								{
									if(dicInstalled.ContainsKey(p.GetValue("Package")))
									{
										dicInstalled[p.GetValue("Package")].AddToPackageInfo("Host Version",p.GetValue("Version"));
									}
									else
									{
										p.AddToPackageInfo("Host Version",p.GetValue("Version"));
										dicInstalled.Add(p.GetValue("Package"), p);
									}
								}
							}
							catch{
								o.WriteLine("[CUBE PROJECT MANAGER] Warning : Duplicate installed package information for " + p.GetValue("Package"));
							}
                            
						}
						else if(s.Filename != "status") //Repository List, not in 'status' file list
						{
							#region [Check for Apt Preference Constraint]
							//Check for Source Apt Preference constraint
							if(s.Constraint != null)
							{
								//Foreach target package in source constraint
								foreach(string str in s.Constraint.Packages)
								{		
									if(str == "*") //If all packages
									{
										blnForce = true;
										blnSkipCons = false;
										break;
									}
									else if( ! str.Contains("*")) //If not all packages. Exact package name
									{
										if(!str.Trim().Equals(p.GetValue("Package"))) //If just specific package
										{
											blnSkipCons = true;
										}
										else
										{
											blnForce = true;
											blnSkipCons = false;
											break;
										}
									}
									else if(str.Trim().StartsWith("*") && str.Trim().EndsWith("*")) //If contains
									{
										if(!p.GetValue("Package").Contains(str.Replace("*","").Trim()))
										{
											blnSkipCons = true;
										}
										else
										{											
											blnForce = true;
											blnSkipCons = false;
											break;
										}
									}
									else if(str.Trim().StartsWith("*")) //If ends with
									{
										if(!p.GetValue("Package").EndsWith(str.Replace("*","").Trim()))
										{
											blnSkipCons = true;
										}
										else
										{
											if(debug) Console.WriteLine(p.GetValue("Package"));											
											blnForce = true;
											blnSkipCons = false;
											break;
										}
									}
									else if(str.Trim().EndsWith("*")) //If starts with
									{
										if(!p.GetValue("Package").StartsWith(str.Replace("*","").Trim()))
										{
											blnSkipCons = true;
										}
										else
										{
											if(debug) Console.WriteLine(p.GetValue("Package"));
											blnForce = true;
											blnSkipCons = false;
											break;
										}
									}								
								}
							}
							
							#endregion
							
							try{
								if(blnSkipCons) //If skip constraint, do not add to the list
								{
									p=new CubePackage();
									blnSkipCons = false;
									continue;
								}
								else if(blnForce) //If forced
								{
									blnForce = false;
									blnSkipCons = false;                                    
									p.AddToPackageInfo("Forced",""); //Tag it as 'Forced', if compared to duplicate package, this is prioritized
									dicAvailable.Add(p.GetValue("Package"),p);
								}
								else
								{
									p.AddToPackageInfo("Repository",s.Link);
									dicAvailable.Add(p.GetValue("Package"),p);
								}
							}
							catch{
								//If package duplication occurs, latest package is the priority
								//But if the package is forced, it is the top priority than latest
                                try
                                {
                                    CubePackage temp = new CubePackage();
                                    dicAvailable.TryGetValue(p.GetValue("Package"), out temp);
                                    if (temp != null)
                                    {
                                        string strTemp = "";
                                        if (!temp.PackageDictionary.TryGetValue("Forced", out strTemp))
                                        {
                                            if (cmp.Compare(p.GetValue("Version"), temp.GetValue("Version")) == 1)
                                            {
                                                dicAvailable.Remove(p.GetValue("Package"));
                                                dicAvailable.Add(p.GetValue("Package"), p);
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                    o.WriteLine("[CUBE PROJECT MANAGER] Warning : Duplicate available package information for " + p.GetValue("Package"));
                                }
							}
						}
						
						p = new CubePackage();
						blnSkip = false;
					}					
				}
				
				strLines = null;
				if(debug) o.WriteLine("[PACKAGE MGR] OK");				
				if(debug) o.WriteLine("[PACKAGE MGR] Total Packages Count : {0}",dicAvailable.Count);
				if(debug) o.WriteLine("[PACKAGE MGR] Total Installed Packages Count : {0}",dicInstalled.Count);
				intSourcesCounter++;
				
				if(StatusChanged!=null) StatusChanged(string.Format(Catalog.GetString("{0} package(s) fetched. Reading project repositories..."),dicAvailable.Count),EventArgs.Empty);
				if(ProgressChanged!=null) ProgressChanged(intSourcesCounter,EventArgs.Empty);								
			}			
			MarkInstalledUpgradablePackages();
		}
				
		bool CheckIfIncluded(string strCategory)
        {
            bool blnAccepted = false;

            if (Array.IndexOf<string>(strAcceptedCategories, strCategory) != -1)
                blnAccepted = true;

            return blnAccepted;
        }
		
		/// <summary>
		/// Marks all packages status as installed, to be upgraded, available or 
		/// newer than repository
		/// </summary>
		void MarkInstalledUpgradablePackages()
		{			
			if(debug) o.Write("[PACKAGE MGR] Marking Installed and Upgradable Packages...");
			
			List<string> lstToRemove = new List<string>();
			foreach(string package in dicInstalled.Keys)
			{				
				CubePackage p1;
				CubePackage p2; 
				dicInstalled.TryGetValue(package,out p1);
				DebianCompare cmp = new DebianCompare();								
				
				if(dicAvailable.TryGetValue(package,out p2))
				{
					/*
					 * Status -1 : Installed < Repository
					 * Staus   0 : Installed = Repository
					 * Staus   1 : Installed > Repository
					 * 
					 * */					
					
					//If it is tag as installed in original computer
					if(p1.PackageDictionary.ContainsKey("Installed"))
					{						
						int status = cmp.Compare(p1.GetValue("Version"),p2.GetValue("Version"));
						if(status == 1) status = 0; //If newer than repository, treat it as installed always
						p1.AddToPackageInfo("Status",status.ToString());
						p1.AddToPackageInfo("Repository Version",p2.GetValue("Version"));						
						p1.PackageDictionary.Remove("Installed"); //Remove excess tags
						
						p2.AddToPackageInfo("Status",status.ToString());
						p2.AddToPackageInfo("Installed Version",p1.GetValue("Version"));
						
						//Record the host version, if any
						if(p1.PackageDictionary.ContainsKey("Host Version"))
						{
							p2.AddToPackageInfo("Host Version",p1.GetValue("Host Version"));
						}
					}
					//If it is not installed in original computer but installed to host computer
					else if(p1.PackageDictionary.ContainsKey("Host Version"))
					{						
						//Record the host version
						p2.AddToPackageInfo("Host Version",p1.GetValue("Host Version"));						
						//Remove the package from installed list
						lstToRemove.Add(p1.GetValue("Package"));
					}
				}
			}
			
			//Remove all requested packages
			foreach(string pRemove in lstToRemove)
				dicInstalled.Remove(pRemove);
			
			if(debug) o.WriteLine("[PACKAGE MGR] OK");			
			if(StatusChanged!=null) StatusChanged(Catalog.GetString("Loading packages. Please wait..."),EventArgs.Empty);
		}
				
		/// <summary>
		/// Gets the package dependencies.
		/// </summary>
		/// <param name='p'>
		/// Package Name
		/// </param>
		/// <param name='dicDepends'>
		/// List of early detected dependencies.
		/// </param>
		/// <param name='treatDownloadedAsInstalled'>
		/// Also treat downloaded packages as installed packages
		/// </param>
		
		public void GetPackageDependencies(CubePackage p, ref SortedList<string,CubePackage> dicDepends, bool treatDownloadedAsInstalled, bool treatHostAvailableAsInstalled)
		{
			string strFullDepends = "";				
			
			rdepends = GetEnableReverseDependency();
			if(rdepends)
			{
				DebianCompare cmp = new DebianCompare();
				//Find reverse dependencies of this package
				CubePackage[] rdep = CubePackageFinder.FindReverseDepends(p.GetValue("Package"),dicInstalled);
				if(rdep!=null)
				{				
					foreach(CubePackage pt in rdep)
					{
						if(!dicDepends.ContainsKey(pt.GetValue("Package")))
						{
							CubePackage pt2 = CubePackageFinder.Find(pt.GetValue("Package"),dicAvailable);						
							if(pt2!=null)
							{		
								string depends = "";
								string val = "";
								if(pt2.PackageDictionary.TryGetValue("Depends",out val))
									depends += val;
								if(pt2.PackageDictionary.TryGetValue("Pre-Depends",out val))
									depends += ","+val;
								if(pt2.PackageDictionary.TryGetValue("Recommends",out val))
									depends += ","+val;
								
								//Split dependency into two (using the package name)
								//From : dep1 (>=ver), dep2, package, dep3
								//To : dep1 (>=ver), dep2
								//To : ,dep3
								//Check the second segment if it starts with (
								//Starts with ( means that the package requires specific version (ie. package(=version))
								//And package without ( means that package doesnt require any version (ie. package )
								string[] vals = (". "+depends.Replace(","," ")+" .").Replace(" "+p.GetValue("Package"),"%").Split('%');
								
								if(vals.Length > 1)
								{
									//Check for upgradable packages and package that requires specific version only
									if(vals[1].Trim().StartsWith("(") && ((pt2.GetValue("Status").Equals("-1") || (pt2.GetValue("Status").Equals("-2") && !treatDownloadedAsInstalled))))
									{
										string package = p.GetValue("Package") + " " + vals[1].Split(')')[0]+")";
										if(!cmp.CompareDeb(p.GetValue("Installed Version"),Debian.GetVersion(package),Debian.GetEqualityOperation(package)))
											dicDepends.Add(pt2.GetValue("Package"),pt2);
									}
								}
							}
						}
					}			
					rdep = null;
				}	
			}
			
			if(p.GetValue("Depends")==null)
				return;
			
			strFullDepends = p.GetValue("Depends");
			
			if(p.GetValue("Pre-Depends") != null)
				strFullDepends += "," + p.GetValue("Pre-Depends");
			
			if(p.GetValue("Recommends") != null)
				strFullDepends += "," + p.GetValue("Recommends");
			
			string[] strDepends = strFullDepends.Split(",".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
			List<string> lstWOR = new List<string>();
			List<string> lstOR = new List<string>();
			
			foreach(string str in strDepends)
			{
				if(str.Contains("|"))
				{
					lstOR.Add(str);
				}
				else
					lstWOR.Add(str);
			}
						
			if(lstWOR.Count > 0)
			{
				foreach(string strDep in lstWOR)
				{		
					GetDependencyOne(strDep, ref dicDepends, treatDownloadedAsInstalled, treatHostAvailableAsInstalled);
				}
			}
		
			if(lstOR.Count > 0)
			{
				if(p.GetValue("Package") == "phonon")
				{
					Console.WriteLine("HERE");
				}
				foreach(string strDep in lstOR)
				{					
					string[] str = strDep.Split('|');
					//List<string> list = new List<string>();
					//List<string> notinstalled = new List<string>();
					string strOnly = null;
					foreach(string strDepOr in str)
					{						
						//Prioritize the installed/upgradable packages
						CubePackage l = CubePackageFinder.Find(Debian.GetName(strDepOr),dicAvailable);
						if(l!=null)
						{
							if(l.GetValue("Status") == "0" || l.GetValue("Status") == "-1" || l.GetValue("Status") == "-2" || (l.PackageDictionary.ContainsKey("Host Version") && treatHostAvailableAsInstalled))
							{
								strOnly = strDepOr;
								break;
							}
							else
								strOnly = strDepOr;
						}
					}
					
					if(strOnly != null)
					{
						GetDependencyOne(strOnly, ref dicDepends,treatDownloadedAsInstalled, treatHostAvailableAsInstalled);						
					}					
				}
			}
		}
		
		public bool GetDependencyOne(string strDep, ref SortedList<string,CubePackage> dicDepends, bool treatDownloadedAsInstalled, bool treatHostAvailableAsInstalled)
		{
			if(debug) o.WriteLine("[PACKAGE MGR] Getting dependency of {0}",Debian.GetName(strDep));
			strDep = strDep.Trim();
			DebianCompare cmp = new DebianCompare();
			if (strDep.Trim().Length == 0)
                return false;
						
			CubePackage p = CubePackageFinder.Find(Debian.GetName(strDep.Trim()), dicAvailable);
			
			if(p == null)
			{
				if(debug) o.WriteLine("[PACKAGE MGR] Unable to find package {0}",Debian.GetName(strDep.Trim()));
				return false;
			}
			if(debug) o.WriteLine("[PACKAGE MGR] Is available?{0}",(p == null)?"YES":"NO");
			
			if(p.GetValue("Status") == "0" || p.GetValue("Status") == "-1" || (p.GetValue("Status") == "-2" && treatDownloadedAsInstalled)) //If installed or upgradable
			{	
				if(debug) o.WriteLine("[PACKAGE MGR] Checking Installed Version Compatibility");
				if(debug) o.WriteLine("[PACKAGE MGR] With Installed Version {0} vs {1}",p.GetValue("Installed Version"),Debian.GetVersion(strDep));
				
				if(p.GetValue("Status") == "-2")
					return true;

				bool res = cmp.CompareDeb (p.GetValue ("Installed Version"), Debian.GetVersion (strDep), Debian.GetEqualityOperation (strDep));

				if(res == true) //if package is installed
				{
					if(debug) o.WriteLine("[PACKAGE MGR] OK");
					return true;
				}
				else if(p.GetValue("Status") == "-1") //if package is upgradable
				{
					if(debug) o.WriteLine("[PACKAGE MGR] Checking Latest Version Compatibility");
					if(debug) o.WriteLine("[PACKAGE MGR] With Updated Version {0} vs {1}",p.GetValue("Version"),Debian.GetVersion(strDep));
					if(cmp.CompareDeb(p.GetValue("Version"),Debian.GetVersion(strDep),Debian.GetEqualityOperation(strDep)))
					{
						if(debug) o.WriteLine("[PACKAGE MGR] OK");
						
						if(!dicDepends.ContainsKey(p.GetValue("Package")))
						{			
							if(debug) o.WriteLine("[PACKAGE MGR] Added");
							dicDepends.Add(p.GetValue("Package"),p);
							GetPackageDependencies(p,ref dicDepends,treatDownloadedAsInstalled,treatHostAvailableAsInstalled);
						}
					}
				}

				else
				{
					/*
					 * This segment will be reached when the current package dependency
					 * is installed in the system but does not meet the current package
					 * version requirements.
					 * 
					 * Package version violations may also caused by different package
					 * versioning system used by the installed and the current package
					 * 
					 * For example, a package A has 'precise-update' version while
					 * package B used 'precise' version or 'current' version, which
					 * is another version format, but has the same version equivalent
					 * to the previous one
					 * 
					 * This may also caused by a package that depends to a package that
					 * needs to be upgraded to satisfy its version dependency needs
					 * 
					 * The system automatically skips the current package for now as an
					 * alternative solution, but may cause dependency issues.
					 * 
					 * Noted by : Cube Camicri Systems
					 * */
					
					o.WriteLine("[PACKAGE MGR] -------------------");
					o.WriteLine("[PACKAGE MGR] Package Violation : " + p.GetValue("Package"));
					o.WriteLine("[PACKAGE MGR] Current Version {0} vs. Requested Version {1}",p.GetValue("Version"),Debian.GetVersion(strDep));
					o.WriteLine("[PACKAGE MGR] Equality Operation {0}",Debian.GetEqualityOperation(strDep));
					o.WriteLine("[PACKAGE MGR] Repository : {0}" , p.GetValue("Repository"));
					o.WriteLine("[PACKAGE MGR] -------------------");
				}
			}
			else
			{
				if(debug) o.WriteLine("[PACKAGE MGR] Not installed nor updated package for this");
				if(debug) o.WriteLine("[PACKAGE MGR] Adding {0} to the dependency list",p.GetValue("Package"));
				if(!dicDepends.ContainsKey(p.GetValue("Package")))
				{							
					if(debug) o.WriteLine("[PACKAGE MGR] Added");					
					dicDepends.Add(p.GetValue("Package"),p);
					GetPackageDependencies(p,ref dicDepends,treatDownloadedAsInstalled,treatHostAvailableAsInstalled);
				}
				if(debug) o.WriteLine("[PACKAGE MGR] Already in the dependency list. Skipped");
			}
			return true;
		}
		
		public SortedList<string,CubePackage> AvailablePackagesDic
		{
			get { return dicAvailable; }
		}
		
		public SortedList<string,CubePackage> InstalledPackagesDic
		{
			get { return dicInstalled; }
		}
						
		public CubeSource[] Sources
		{
			get { return srcmgr.Sources; }
		}
		
		public CubeReleaseGPG[] Releases
		{
			get { return srcmgr.Releases; }
		}

		public CubeAptPreferencesConstraints[] AptConstraints
		{
			get { return srcmgr.Constraints; }
		}
		
		public bool FindPackage(CubePackage p)
		{
			if(p.GetValue("Package") == strToFind)
				return true;
			return false;
		}
		
		public bool HostCompatible
		{
			set { this.hostcompatible = value; }
		}
	}
}