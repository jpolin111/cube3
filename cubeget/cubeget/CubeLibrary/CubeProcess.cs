/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * CubeProcess (MAIN)
 * 	The main controller of the whole cube. Performs almost all operations.
 * 
 * */

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using Aria;
using o=System.Console;

namespace Cube
{
	public class CubeProcess
	{
		CubeProjectCreator creator;
		CubeConfiguration config = new CubeConfiguration();
		CubeProject[] projects;
		CubeProject proj;
		CubeProjectManager mgr;
		CubeSystemUpdater sysUpdate;
		
		public event EventHandler OutputChanged;
		public event EventHandler ProgressChanged;
		
		bool blnHostAndProjCompatible = false;
		bool blnInOriginalComputerViaHome = false;
		bool blnInOriginalComputer = false;
		
		SortedList<string,CubePackage> depends = new SortedList<string, CubePackage>();
		
		public CubeProcess ()
		{
			
		}
		
		public bool LoadProjects()
		{
			config = new CubeConfiguration();
			creator = new CubeProjectCreator(config.ProjectDirectory);
			projects = creator.GetProjects();
			return true;
		}
		
		public int FindProject(string projName)
		{
			if(projects==null)
				return -1;
			
			int index = -1;
			
			for(int i = 0; i < projects.Length; i++)
			{
				if(projects[i].ProjectName == projName)
				{
					index = i;
					break;
				}
			}
			
			return index;
		}
		
		public bool OpenProject(int index)
		{
			proj = projects[index];
			
			mgr = new CubeProjectManager(proj);					
			mgr.StatusChanged += OutputChanged;
			ReloadProject();
			return true;
		}
		
		public void ReloadProject()
		{			
			mgr.ScanList();
			MarkForDownloadedPackages();			
		}
		
		public int CreateProject(string projName)
		{
			CubeProject proj = new CubeProject();
			proj.Architecture = config.Architecture;
			proj.ComputerName = config.ComputerName;
			proj.OperatingSystem = config.OperatingSystem;
			proj.ProjectName = projName.Trim();
			proj.DateCreated = DateTime.Now.ToString();
			proj.ProjectParentDirectory = config.ProjectDirectory;
			proj.Release = config.GetSpecificLSBReleaseInfo("-r");
			proj.CodeName = config.GetSpecificLSBReleaseInfo("-c");
			proj.Distribution = config.GetSpecificLSBReleaseInfo("-i");
			proj.ProjectVersion = CubeSInformation.APPLICATION_VERSION;
			creator = new CubeProjectCreator(proj);
			if(ProgressChanged!=null)
				creator.ProgressChanged += ProgressChanged;
			
			int res = creator.CreateProject(false);
			if(res==0)
				this.Project = proj;
			return res;
		}
		
		public int GetComputerHostType()
		{
			//-2 - root, computer mismatched
			//-1 - not root, computer mismatched 
			// 0 - not root, computer matched
			// 1 - root, computer matched
			
			if(config == null)
				config = new CubeConfiguration();
			
			if(config.ComputerName == "root")
			{
				if(config.OperatingSystem.Trim() == proj.OperatingSystem.Trim())
				{
					if(Directory.Exists(UbuntuEssentials.UbuntuHomeDirectory+proj.ComputerName))
						return 1;
				}
				else
					return -2;
			}
			else if(config.ComputerName == proj.ComputerName)
			{
				if(config.OperatingSystem.Trim() == proj.OperatingSystem.Trim())
					if(Directory.Exists(UbuntuEssentials.UbuntuHomeDirectory+proj.ComputerName))
						return 0;
			}
			return -1;
		}
		
		public void MarkForRepackedPackages()
		{
			o.WriteLine();
			o.WriteLine("[Cube] Checking previously repacked packages...");
			
			int count = 0;
			int total = Directory.GetFiles(proj.SharedPackagesDirectory,"*deb",SearchOption.TopDirectoryOnly).Length;			
			
			string[] files = Directory.GetFiles(proj.SharedPackagesDirectory,"*deb",SearchOption.TopDirectoryOnly);
			
			bool blnFoundBroken = false;
			
			foreach(string file in files)
			{					
				if(ProgressChanged!=null)
				{
					ProgressChanged(count/total,EventArgs.Empty);
				}
				
				//Ensure that .deb packages will be processed
				if((new FileInfo(file).Extension != ".deb"))
					continue;
				
				string[] package = new FileInfo(file).Name.Split('_');				
				
				CubePackage p = CubePackageFinder.Find(package[0].Trim(),mgr.AvailablePackagesDic);
				if(p!=null)
				{				
					//Prioritize Downloaded than Repacked
					if(p.GetValue("Status") == "-2")
						continue;
					
					string versionDownloaded = package[1].Trim();
					string versionLatest = p.GetValue("Version");
					if(versionLatest.Contains(":"))
						versionLatest = versionLatest.Split(':')[1].Trim();
						
					//If downloaded version is already installed
					if(p.GetValue("Status") == "0")
					{
						//Tag it as Downloaded
						//Package is now not needed
						if(!p.PackageDictionary.ContainsKey("Downloaded"))
						{
							p.AddToPackageInfo("Downloaded",file);
							p.AddToPackageInfo("Old Status",p.GetValue("Status"));							
						}
					}
					//If downloaded version is the latest version and it is not installed
					else if(versionDownloaded == versionLatest)
					{						
						p.AddToPackageInfo("Old Status",p.GetValue("Status")); //Remember old status
						p.UpdateValue("Status","-2"); //Mark it as downloaded
						p.UpdateValue("Filepath",file);
						//Mark it as repacked
						p.AddToPackageInfo("Repacked","True");
					}
					else if(p.GetValue("Status") != "0")
					{							
						blnFoundBroken = true;
						o.WriteLine("[Cube] Broken package detected : "+p.GetValue("Package"));
						//p.UpdateValue("Status","-3"); //Mark it as broken
						p.AddToPackageInfo("Broken",file);
					}
				}
				else
				{
					blnFoundBroken = true;
					o.WriteLine("[Cube] Broken package detected : "+p.GetValue("Package"));
					//p.UpdateValue("Status","-3"); //Mark it as broken
					p.AddToPackageInfo("Broken",file);
				}
				count++;
			}
			
			if(blnFoundBroken)
				o.WriteLine("[Cube] Some repacked packages were broken. It may be caused of unfinished or interrupted downloading, or the package is older than the repository version");
			else
				o.WriteLine("[Cube] Checking complete. No broken packages found");
		}
		
		public void MarkForDownloadedPackages()
		{	
			o.WriteLine();
			o.WriteLine("[Cube] Checking previously downloaded packages...");
			bool blnFoundBroken = false;						
			
			int count = 0;
			int total = Directory.GetFiles(proj.PackagesDirectory,"*deb",SearchOption.TopDirectoryOnly).Length;
			
			Stack<string> stkList = new Stack<string>();
			string[] files = Directory.GetFiles(proj.PackagesDirectory,"*deb",SearchOption.TopDirectoryOnly);
			DebianCompare debcmp = new DebianCompare();
			
			//Remove multiple packages (same packages, different versions) to the list
			//Prioritize the latest version
			if(files.Length > 0)			
				stkList.Push(files[0]);
			for(int i = 1; i < files.Length; i++)
			{
				string[] f1 = new FileInfo(stkList.Peek()).Name.Split('_');
				string[] f2 = new FileInfo(files[i]).Name.Split('_');
					
				//If same package name
				if(f1[0] == f2[0])
				{					
					//If the previous version is lower than the current
					if(debcmp.Compare(f1[1],f2[1]) < 0)
					{
						//Replace previous to current						
						string todelete = stkList.Pop();
						try{
							File.Delete(todelete);
						}catch{}
						stkList.Push(files[i]);
					}
				}	
				else
					stkList.Push(files[i]);
			}
			
			foreach(string file in stkList)
			{	
				if(ProgressChanged!=null)
				{
					ProgressChanged(count/total,EventArgs.Empty);
				}
				
				//Ensure that .deb packages will be processed
				if((new FileInfo(file).Extension != ".deb"))
					continue;
				
				string[] package = new FileInfo(file).Name.Split('_');				
				
				CubePackage p = CubePackageFinder.Find(package[0].Trim(),mgr.AvailablePackagesDic);
				if(p!=null)
				{		
					if(p.PackageDictionary.ContainsKey("Downloaded"))
					{					
						p.PackageDictionary.Remove("Downloaded");
						p.UpdateValue("Status",p.GetValue("Old Status"));
						p.PackageDictionary.Remove("Old Status");
					}
					
					if(p.PackageDictionary.ContainsKey("Repacked"))
						p.PackageDictionary.Remove("Repacked");
					
					if(p.PackageDictionary.ContainsKey("Broken"))
					{
						p.PackageDictionary.Remove("Broken");
					}					
										
					string sum = null;
					/*
					if(this.config.IsAtLinuxSystem && File.Exists(UbuntuEssentials.MD5SumProvider))
					{
						sum = ProcessOutput.Read("md5sum",file);
						if(sum !=null)
							sum = sum.Split(' ')[0];
					}					
					*/
					if(sum == null)
					{
						FileStream stream = new FileStream(file,FileMode.Open);
						sum = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(stream)).Replace("-","").ToLower().Trim();
						stream.Close();	
					}
					
					if(sum == p.GetValue("MD5sum")) //If downloaded file passed the MD5sum check
					{											
						string versionDownloaded = package[1].Trim();
						string versionLatest = p.GetValue("Version");
						if(versionLatest.Contains(":"))
							versionLatest = versionLatest.Split(':')[1].Trim();
						
						//If downloaded version is already installed
						if(p.GetValue("Status") == "0")
						{
							//Tag it as Downloaded
							//Package is now not needed
							if(!p.PackageDictionary.ContainsKey("Downloaded"))
							{
								p.AddToPackageInfo("Downloaded",file);
								p.AddToPackageInfo("Old Status",p.GetValue("Status"));
							}
						}
						//If downloaded version is the latest version and it is not installed
						else if(versionDownloaded == versionLatest)
						{
							p.UpdateValue("Status","-2");//Mark it as downloaded
							p.UpdateValue("Filepath",file);
						}
						else if(p.GetValue("Status") != "0")
						{							
							blnFoundBroken = true;
							o.WriteLine("[Cube] Broken package detected : "+p.GetValue("Package"));
							//p.UpdateValue("Status","-3"); //Mark it as broken
							p.AddToPackageInfo("Broken",file);
						}
					}
					else
					{
						blnFoundBroken = true;
						o.WriteLine("[Cube] Broken package detected : "+p.GetValue("Package"));
						//p.UpdateValue("Status","-3"); //Mark it as broken
						p.AddToPackageInfo("Broken",file);
					}
				}	
				count++;
			}
			
			if(blnFoundBroken)
				o.WriteLine("[Cube] Some downloaded packages were broken. It may be caused of unfinished or interrupted downloading, or the package is older than the repository version");
			else
				o.WriteLine("[Cube] Checking complete. No broken packages found");
			
			MarkForRepackedPackages();
		}
		
		public bool ValidateDownloadedDependencies(CubePackage p, out List<CubePackage> depends, out List<CubePackage> notMet)
		{							
			depends = new List<CubePackage>();
			notMet = new List<CubePackage>();
			depends.AddRange(GetDependenciesAndDontTreatDownloadedAsInstalled(p));			
						
			foreach(CubePackage dep in depends)
			{
				if(dep.GetValue("Status") != "0" && dep.GetValue("Status") != "-2" && dep.GetValue("Status") != "1")
					notMet.Add(dep);
			}
			
			if(notMet.Count > 0)
				return false;
			return true;
		}
		
		public bool ValidateDownloadedDependencies(List<CubePackage> p, out List<CubePackage> depends, out List<CubePackage> notMet)
		{							
			depends = new List<CubePackage>();
			notMet = new List<CubePackage>();
			depends.AddRange(GetDependenciesAndDontTreatDownloadedAsInstalled(p));
						
			foreach(CubePackage dep in depends)
			{
				if(dep.GetValue("Status") != "0" && dep.GetValue("Status") != "-2" && dep.GetValue("Status") != "1")
					notMet.Add(dep);
			}
			
			if(notMet.Count > 0)
				return false;
			return true;
		}
		
		public bool DownloadPackages(CubePackage[] pdep)
		{			
			if(OutputChanged!=null)
			{
				OutputChanged("".PadRight(80,' '),EventArgs.Empty);
				OutputChanged("[Aria Downloader] Initializing...",EventArgs.Empty);
			}

			CubeConfiguration config = new CubeConfiguration();
			AriaData[] data = new AriaData[pdep.Length];
			
			for(int i =0 ; i < pdep.Length; i++)
			{				
				data[i] = new AriaData();					
				data[i].File_Path = Project.PackagesDirectory;				
				data[i].URL = pdep[i].GetValue("Filename");
				data[i].Is_Asynchronous = false;
				data[i].DisplayOutput = false;

				if(!config.IsAtLinuxSystem)
				{
					data[i].Application_Name = "aria2c";
					data[i].Application_Path = config.ThirdPartyDirectory + "aria2";
				}
			}
			
			AriaDownloader aria;
			int intCount = 0;
			bool blnErrorOccured = false;
			string output = "[ARIA Downloading : {0}/{1}] {2} {3} {4}";
			
			foreach(AriaData d in data)
			{									
				aria = new AriaDownloader(d);
				
				aria.ProgressChanged += (sender, e) => {
					if(OutputChanged!=null)
					{
						string stat = string.Format(output,intCount+1,data.Length,pdep[intCount].GetValue("Package"),ConvertSize(pdep[intCount].GetValue("Size")),(sender as AriaDownloader).Progress+"%");
						OutputChanged("".PadRight(79,' '),EventArgs.Empty);
						OutputChanged(stat,EventArgs.Empty);						
					}
				};
								
				aria.StartDownload();
				if(aria.Is_Error_Occured)
				{
					if(OutputChanged!=null)
					{
						string err = String.Format("[ARIA Failed : {0}",pdep[intCount].GetValue("Package"));
						OutputChanged("".PadRight(80,' '),EventArgs.Empty);
						OutputChanged(err,EventArgs.Empty);
						OutputChanged(aria.Error_Message+"\n",EventArgs.Empty);
					}					
					blnErrorOccured = true;
				}
				intCount++;
			}
			
			if(OutputChanged!=null)
			{
				OutputChanged("".PadRight(80,' '),EventArgs.Empty);
				if(!blnErrorOccured)
					OutputChanged("[Aria Downloader] All downloads finished successfully\n",EventArgs.Empty);
				else
					OutputChanged("[Aria Downloader] Some packages failed to be downloaded. Try to redownload the package to see the packages\n",EventArgs.Empty);
			}
			
			return ! blnErrorOccured;
		}
		
		public bool DownloadRepositories(CubeSource[] s, CubeReleaseGPG[] rel)
		{	
			string strDownloadDir = Project.ListDirectory+"temp"+ System.IO.Path.DirectorySeparatorChar;
			bool blnErrorOccured = false;
			if(OutputChanged!=null)
			{
				OutputChanged("".PadRight(80,' '),EventArgs.Empty);
				OutputChanged("[Aria Downloader] Initializing...",EventArgs.Empty);
			}
			
			CubeConfiguration config = new CubeConfiguration();
			AriaData[] data = new AriaData[s.Length-1];
			AriaDownloader aria;
			
			if(OutputChanged!=null)
			{
				OutputChanged("Downloading Releases finished.\n",EventArgs.Empty);
				
				OutputChanged("".PadRight(80,'-'),EventArgs.Empty);
				OutputChanged("Downloading repositories...",EventArgs.Empty);
				OutputChanged("".PadRight(80,'-'),EventArgs.Empty);
			}			
			
			//Source Lists
			for(int i = 0; i < s.Length-1; i++)
			{				
				data[i] = new AriaData();				
				data[i].File_Path = strDownloadDir;
				data[i].File_Name = s[i].Filename;
				data[i].URL = s[i].Link;
				data[i].Delete_Existing = true;
				data[i].Is_Asynchronous = false;

				if(!config.IsAtLinuxSystem)
				{
					data[i].Application_Name = "aria2c";
					data[i].Application_Path = config.ThirdPartyDirectory + "aria2";
				}
				
				if(OutputChanged!=null)
				{									
					OutputChanged(String.Format("[ARIA Downloading {0}/{1}] "+s[i].Link,i+1,s.Length-1),EventArgs.Empty);
				}
				
				aria = new AriaDownloader(data[i]);
				aria.StartDownload();
				
				if(aria.Is_Error_Occured)
				{
					blnErrorOccured = true;
					if(OutputChanged!=null)
					{										
						OutputChanged("[ARIA FAILED] "+s[i].Link+"\n",EventArgs.Empty);
					}
				}
				else
				{
					CubeSource source = s[i];
					string filename = Project.ListDirectory+"temp"+System.IO.Path.DirectorySeparatorChar+source.Filename;
					string target = Project.ListDirectory + source.Filename;
					o.WriteLine();
					if(CubeDecompress.Decompress(filename,target))
					{
						try{ File.Delete(filename); }
						catch{}
					}
					else
						blnErrorOccured = true;
				}
				if(OutputChanged!=null)
					OutputChanged("".PadRight(80,'-'),EventArgs.Empty);
			}
			return blnErrorOccured;
		}
		
		public CubePackage[] GetDependencies(List<CubePackage> pcs)
		{
			return GetDependencies(pcs,true,false);
		}
		
		public CubePackage[] GetDependencies(CubePackage p)
		{			
			depends = new SortedList<string, CubePackage>();
			return GetDependencies(new List<CubePackage>(new CubePackage[]{p}),true,false);
		}
		
		public CubePackage[] GetDependenciesAndDontTreatDownloadedAsInstalled(List<CubePackage> pcs)
		{
			depends = new SortedList<string, CubePackage>();
			return GetDependencies(pcs,false,false);			
		}
		
		public CubePackage[] GetDependenciesAndDontTreatDownloadedAsInstalled(CubePackage p)
		{
			depends = new SortedList<string, CubePackage>();
			return GetDependencies(new List<CubePackage>(new CubePackage[]{p}),false,false);
		}
		
		public CubePackage[] GetDependencies(CubePackage p, bool treatDownloadedAsInstalled, bool treatHostAvailableAsInstalled)
		{
			depends = new SortedList<string, CubePackage>();
			return GetDependencies(new List<CubePackage>(new CubePackage[]{p}),treatDownloadedAsInstalled,treatHostAvailableAsInstalled);
		}
		
		public CubePackage[] GetDependencies(List<CubePackage> pcs, bool treatDownloadedAsInstalled, bool treatHostAvailableAsInstalled)
		{
			depends = new SortedList<string, CubePackage>();
			foreach(CubePackage p in pcs)
			{
				if(!depends.ContainsKey(p.GetValue("Package")))
				{
					depends.Add(p.GetValue("Package"),p);			
					mgr.GetPackageDependencies(p,ref depends,treatDownloadedAsInstalled,treatHostAvailableAsInstalled);
				}
			}
								
			List<CubePackage> lst = new List<CubePackage>();
			for(int i = 0; i < depends.Count; i++)
				lst.Add(depends.Values[i]);
			
			return lst.ToArray();
		}		
		
		public void GetValidHostPackages(CubePackage p, out List<CubePackage> lstPassed, out List<CubePackage> lstFailed)
		{
			List<CubePackage> deps = new List<CubePackage>(GetDependencies(p,true,true));
			lstPassed = new List<CubePackage>();
			lstFailed = new List<CubePackage>();
			foreach(CubePackage pks in deps)
			{
				if(pks.PackageDictionary.ContainsKey("Host Version"))
				{
					if(pks.GetValue("Version") == pks.GetValue("Host Version"))
						lstPassed.Add(pks);
					else
						lstFailed.Add(pks);
				}
				else
					lstFailed.Add(pks);
			}
			//this.debRecover.GetValidHostPackages(deps,out lstPassed, out lstFailed);
		}		
		
		private bool CheckInOriginalComputer()
		{
			CubeConfiguration config = new CubeConfiguration();			
			
			if(!Directory.Exists(UbuntuEssentials.UbuntuHomeDirectory+Project.ComputerName))
				return false;
			
			if(Project.OperatingSystem.Trim() != config.OperatingSystem.Trim())
				return false;
			
			if(Project.Architecture.Trim() != config.Architecture)
				return false;
			
			if(config.ComputerName != "root")
			{
				if(config.ComputerName != Project.ComputerName)
					return false;
			}

			return true;
		}		
		
		public bool GetHostRepositories()
		{
			if(HostAndProjectCompatible)
			{
				foreach(CubeSource s in Manager.Sources)
				{
					string path = UbuntuEssentials.PackageListDirectoryPath + "/" + s.Filename;
					string proj_list_path = this.proj.ListDirectory + s.Filename;
					
					if(File.Exists(path))
					{
						try{
							Console.WriteLine("Copying {0} to {1}",path,proj_list_path);
							File.Copy(path,proj_list_path,true);
						}catch{}
					}
				}
				return true;
			}			
			return false;
		}
		
		public string ConvertSize(string size)
		{
			if(size==null)
				return "undefined";
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
				
		public void InitializeHostAndProject()
		{
			if(config.IsAtLinuxSystem)
			{				
				blnInOriginalComputer = CheckInOriginalComputer();
				blnInOriginalComputerViaHome = HasHomeProjectInformation();			
				blnHostAndProjCompatible = CheckHostAndProjectCompatibility();
			}
		}
		
		private bool CheckHostAndProjectCompatibility()
		{
			string host_arch = config.Architecture;
			
			if(host_arch == null)
			{
				//Failed to get the config architecture
				Console.WriteLine("[Cube Host Compatibility] Unable to detect host architecture. Trying again...");
				host_arch = config.Architecture; //Try again
				if(host_arch!=null)
					Console.WriteLine("[Cube Host Compatibility] Host architecture detected");
				else
					Console.WriteLine("[Cube Host Compatibility] Host architecture not detected.");
			}
			
			if(host_arch != Project.Architecture)
				return false;
			
			string host_dist = config.GetSpecificLSBReleaseInfo("-i");
			string host_rel = config.GetSpecificLSBReleaseInfo("-r");
			
			if(host_dist.Equals(Project.Distribution,StringComparison.OrdinalIgnoreCase))
			{
				if(host_rel == Project.Release)
				{
					if(!InOriginalComputer)
					{
						Console.WriteLine("[Cube App Share] App Share Feature is now enabled for this computer and your project");
						return true;
					}
					else
						Console.WriteLine("[Cube App Share] In the original computer. App Sharing not allowed.");
				}
			}
			else
			{
				Console.WriteLine("[Cube App Share] Different Distribution Detected. Will check for compatibility.");
				if(File.Exists(config.AppShareConfigurationFile))
				{					
					int index1= 0;
					int index2 = 1;
					string[] content = File.ReadAllLines(config.AppShareConfigurationFile);
					for(int i = 0; i < content.Length; i++) 
					{
						string line = content[i];
						
						if(line.StartsWith("#"))
							continue;
												
						if(line.StartsWith(">")) //Get the first distribution entry
						{
							if(line.Contains("="))
							{
								Console.WriteLine("[Cube App Share] Testing at "+line);
								bool cont = false;
								string[] dists = line.Replace(">","").Split('=');								
								if(dists[0].Trim().Equals(host_dist) && dists[1].Trim().Equals(Project.Distribution))
									cont = true;
								else if(dists[1].Trim().Equals(host_dist,StringComparison.OrdinalIgnoreCase) && dists[0].Trim().Equals(Project.Distribution,StringComparison.OrdinalIgnoreCase))
								{
									index1 = 1;
									index2 = 0;
									cont = true;
								}								
								
								if(cont)
								{
									Console.WriteLine("[Cube App Share] Entry allowed for "+host_dist+" and "+ Project.Distribution);
									while(!line.Contains("<end") && i < content.Length)
									{
										++i;
										line = content[i];
										
										if(line.Contains(":"))
										{
											//Compare host's release vs project's release based on the apt-share compatibility table
											Console.WriteLine("[Cube App Share] Comparing Release "+line.Split(':')[index1]+" and "+line.Split(':')[index2]);
											if((host_rel == line.Split(':')[index1].Trim()) && (Project.Release == line.Split(':')[index2].Trim()))
											{
												Console.WriteLine("[Cube App Share] App Share Feature is now enabled for this computer and your project");
												return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			Console.WriteLine("[Cube App Share] App Share Feature is not allowed in this computer");
			return false;
		}
		
		private bool HasHomeProjectInformation()
		{			
			CubeProjectCreator creator = new CubeProjectCreator();
			if(Directory.Exists(config.CubeLinuxDataDirectory))
			{
				foreach(string info in Directory.GetFiles(config.CubeLinuxDataDirectory,"*info.cube"))
				{
					CubeProject p = creator.GetProjectInfo(info);
					if(this.Project.ProjectVersion == p.ProjectVersion &&
					   this.Project.ComputerName == p.ComputerName &&
					   this.Project.ProjectName == p.ProjectName &&
					   this.Project.OperatingSystem == p.OperatingSystem &&
					   this.Project.Architecture == p.Architecture &&
					   this.Project.DateCreated == p.DateCreated)
						return true;					   
				}
			}
			
			return false;
		}
		
		public bool HostAndProjectCompatible
		{
			get {return blnHostAndProjCompatible;}
		}
		
		public bool InOriginalComputer
		{
			get { return blnInOriginalComputer; }
		}
		
		public bool InOriginalComputerViaHome
		{
			get { return blnInOriginalComputerViaHome; }
		}
		
		public int UpdateSystemStatus()
		{
			return sysUpdate.UpdateSystem();
		}
		
		public int UpdateCubeStatus()
		{
			return sysUpdate.UpdateCube();
		}
				
		public CubeSystemUpdater SystemUpdater
		{
			get { return sysUpdate; }
		}
		
		public CubeProjectManager Manager
		{
			get { return mgr; }
		}
		
		public CubeProject[] Projects
		{
			get { return projects; }
		}
		
		public CubeProject Project
		{
			get { return proj; }
			set { 
				proj = value; 
				InitializeHostAndProject();
				mgr = new CubeProjectManager(proj,this);				
				sysUpdate = new CubeSystemUpdater(proj);					
			}
		}
		
		public CubeConfiguration Configuration
		{
			get 
			{
				if(config==null)
					config = new CubeConfiguration();
				return config;
			}			
		}
	}
}
