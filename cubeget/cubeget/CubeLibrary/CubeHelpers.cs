/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using Gtk;
using System.Collections.Generic;

namespace Cube
{
	public class TreeHelper
	{
		ListStore lstrStore;
		TreeView tree;
		TreeColumnHelper[] cols;
		
		public TreeHelper(TreeView tree,ListStore store, TreeColumnHelper[] cols)
		{
			this.tree = tree;
			this.lstrStore = store;			
			this.cols = cols;
		}
		
		public TreeView Initialize()
		{	
			tree.Model = lstrStore;
			for(int i=0;i < lstrStore.NColumns; i++)
			{				
				tree.AppendColumn(cols[i].Column);				
			}						
			tree.ShowAll();
			return tree;
		}
	}
	
	public class TreeColumnHelper
	{
		string strColName;
		string strType;
		int colIndex;
		bool blnVisible;	
		TreeViewColumn col;
		CellRenderer renderer;
		
		public TreeColumnHelper(string colname, int colindex, string type, bool visible)
		{
			this.strColName = colname;
			this.strType = type;
			this.blnVisible = visible;
			this.colIndex = colindex;
			col = new TreeViewColumn();			
			Initialize();
		}		
		
		public void Initialize()
		{
			col.Title = this.strColName;
			col.Visible = this.blnVisible;
			col.Resizable = true;			
			col.Clickable = true;			
			
			if(strType == "text")
			{
				renderer = new CellRendererText();
				col.PackStart(renderer,true);
				col.AddAttribute(renderer,"text",colIndex);
			}
			else if(strType == "toggle")
			{
				renderer = new CellRendererToggle();
				col.PackStart(renderer,true);
				col.AddAttribute(renderer,"active",colIndex);
			}
			else if(strType == "progress")
			{
				renderer = new CellRendererProgress();
				col.PackStart(renderer,true);
				col.AddAttribute(renderer,"value",colIndex);
			}
			else if(strType == "combo")
			{
				renderer = new CellRendererCombo();
				col.PackStart(renderer,true);
				col.AddAttribute(renderer,"active",colIndex);
			}
			else if(strType == "pixbuf")
			{
				renderer = new CellRendererPixbuf();
				col.PackStart(renderer,true);
				col.AddAttribute(renderer,"pixbuf",colIndex);
			}
		}
		
		public CellRenderer Renderer
		{
			get { return renderer; }
		}
		
		public TreeViewColumn Column
		{
			get { return col;}
		}
		
		public string ColumnName
		{			
			set { strColName = value; }
			get { return strColName; }
		}
		
		public string Type
		{
			get { return strType; }
		}
		
		public bool Visible
		{
			set { blnVisible = value; }
			get { return blnVisible; }
		}
	}

}

