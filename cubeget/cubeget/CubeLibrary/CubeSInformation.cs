/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;

namespace Cube
{
	public class CubeSInformation
	{
		public static string APPLICATION_NAME = "Camicri Cube";
		public static string APPLICATION_VERSION = "1.0.9.3-nightly-0.1";
		public static string RELEASE_DATE = "January 11, 2015";
		public static string AUTHOR = "Jake Capangpangan <camicrisystems@gmail.com>";
		public static string COMPANY = "Camicri Systems";
		public static string DESCRIPTION = "Portable package manager for Linux.\nDownload new repositories and applications, and install or update it offline.";
		public static string COPYRIGHT = "Copyright © 2010-2015 Jake Capangpangan\nCamicri Systems Philippines";
		public static string LICENSE = 
			"Camicri Cube is free software; you can redistribute it and/or modify it under the terms of\n"
		  + "the GNU General Public License as published by the Free Software Foundation; either\n"
		  + "version 2 of the License, or (at your option) any later version.\n"
		  + "\n"
		  + "Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY\n"
		  + "WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS\n"
		  + "FOR A PARTICULAR PURPOSE. See the GNU General Public License for more\n"
		  + "details.\n"
		  + "\n"
		  + "You should have received a copy of the GNU General Public License along with Camicri\n"
		  + "Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,\n"
		  + "Boston, MA 02110-1301 USA\n";
		public static string TRANSLATION 
			= "Jake Capangpangan (Filipino)/n" +
				"Rizal Muttaqin (Indonesian)/n" +
				"Aleksey Kabanov (Russian)/n" +
				"Salih Ünver (Turkish)";
		
		public static string FACEBOOK = "https://www.facebook.com/camicrisystems";
		public static string WEBPAGE = "http://www.launchpad.net/camicricube";
		public static string REPORT_BUG = "http://bugs.launchpad.net/camicricube/+filebug";
		public static string QUESTION = "https://answers.launchpad.net/camicricube/+addquestion";
		public static string TRANSLATE = "https://translations.launchpad.net/camicricube";
	}
}