/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using o = System.Console;
namespace Cube
{
	public class CubeConfiguration
	{
		string strProjectDir = "projects" + Path.DirectorySeparatorChar;
		string strDataDir = "data" + Path.DirectorySeparatorChar;		
		string strInternalCubeDir = "/opt/cube/";
		
		public CubeConfiguration()
		{
			if(!Directory.Exists(strProjectDir) || !Directory.Exists(strDataDir))
			{
				if(IsAtLinuxSystem)
				{
					if(Directory.Exists(strInternalCubeDir))
					{
						strDataDir = strInternalCubeDir+strDataDir;
						strProjectDir = System.Environment.GetEnvironmentVariable("HOME") + "/cube-" +strProjectDir;
					}
				}
			}
			
			if(!Directory.Exists(strProjectDir))
				Directory.CreateDirectory(strProjectDir);			
		}		
		
		public char Slash
		{
			get { return Path.DirectorySeparatorChar; }
		}
		
		public string ProjectDirectory
		{
			get { return strProjectDir; }
		}
		
		public string DataDirectory
		{
			get { return strDataDir;}
		}
		
		public string BinDirectory
		{
			get { return strDataDir + "bin" + Slash; }
		}
		
		public string ResourcesDirectory
		{
			get { return strDataDir + "resources" + Slash; }
		}
		
		public string LogsDirectory
		{
			get { return strDataDir + "logs" + Slash; }
		}
		
		public string ConsoleDirectory
		{
			get { return strDataDir + "console" + Slash;}
		}
		
		public string ThemesDirectory
		{
			get { return ResourcesDirectory + "themes" + Slash; }
		}

		public string ThirdPartyDirectory
		{
			get { return strDataDir + "third-party" + Slash; }
		}
		
		public string TemporaryFilesDirectory
		{
			get { return strDataDir + "temp" + Slash; }
		}
		
		public string ConfigurationDirectory
		{
			get { return strDataDir + "config" + Slash; }
		}

		public string IconsDirectory
		{
			get { return ResourcesDirectory + "icons" + Slash; }
		}
		
		public bool IsAtLinuxSystem
		{
			get { return Directory.Exists("/etc/apt");	}
		}		
		
		public string HomeDirectory
		{
			get { return UbuntuEssentials.Home + Slash; }
		}
		
		public string CubeLinuxDataDirectory
		{
			get { return HomeDirectory + ".camicri-cube/";}
		}		
		
		public string ComputerName
		{
			get {
				if(IsAtLinuxSystem)
				{
					string name = null;
					name = System.Environment.GetEnvironmentVariable("USER");
					if(name != null)
						return name;
				}
				return ProcessOutput.Read("whoami");
			}
		}
		
		public string CDEPath
		{
			get{
				string cde_location_path = TemporaryFilesDirectory + "cde_location.cube";
				if(File.Exists(cde_location_path))
				{
					string path = File.ReadAllText(cde_location_path).Trim();
					if(path.Length > 0 && File.Exists(path))
						return path;					
				}
				return null;
			}
		}
		
		public string RootCommand
		{
			get
			{
				return BinDirectory + "root";
			}
		}
		
		public string[] GetLSBReleaseInfo
		{
			get
			{
				string output = null;
				output = ProcessOutput.Read("lsb_release","-id -d -r -c");
				if(output != null)					
					return output.Split('\n');
				return null;
			}
		}
		
		public string GetSpecificLSBReleaseInfo(string param)
		{
			string output = null;
			output = ProcessOutput.Read("lsb_release",param);
			if(output != null)
				return output.Split(':')[1].Trim();
			return null;
		}
		
		public string CategoriesConfigurationFile
		{
			get{ return ConfigurationDirectory + "categories.cube";}
		}
		
		public string AppShareConfigurationFile
		{
			get{ return ConfigurationDirectory + "app-share.cube"; }
		}

		public string Architecture
		{			
			get {
				string arch = ProcessOutput.Read("uname","-i");
				if(arch == "x86_64")
					arch = "amd64";
				else if(arch == "i686")
					arch = "i386";
				else if(arch == null)
					return null;
				return "binary-" + arch;
			}
		}
			
		public string OperatingSystem
		{
			get {return ProcessOutput.Read("cat", "/etc/issue.net");}
		}
	}
}