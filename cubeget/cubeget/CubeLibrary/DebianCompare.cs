/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Cube
{
	public class DebianCompare
	{
		public  int Compare(string sv1, string sv2)
		{
			if(sv1==null || sv2==null)
			{
				Console.WriteLine("No Version!");
				return 0;
			}
			
			
			if(sv1.Length > sv2.Length)
				sv2.PadRight(sv1.Length,' ');
			else if(sv2.Length > sv1.Length)
				sv1.PadLeft(sv2.Length,' ');
			
			
			VersionExtract v1 = new VersionExtract(sv1);
			VersionExtract v2 = new VersionExtract(sv2);
			
			if(v1.Epoch > v2.Epoch)
				return 1;
			if(v1.Epoch < v2.Epoch)
				return -1;
			
			int intResult = CompareVersion(v1.Version,v2.Version);
			if(intResult!=0)
				return intResult;
			else
				return CompareVersion(v1.Revision,v2.Revision);
		}
		
		private  int CompareVersion(string s1, string s2)
		{
			if(s1 == null && s2 == null)
				return 0;			

			List<VersionType> lstType1 = new List<VersionType>();
			List<VersionType> lstType2 = new List<VersionType>();
			string str1="";
			string str2="";
			
			foreach(char ch in s1)
				lstType1.Add(new VersionType(ch));
			foreach(char ch in s2)
				lstType2.Add(new VersionType(ch));
			
			int i = 0;			
			
			while(i < lstType1.Count && i < lstType2.Count)
			{
				str1="";
				str2="";				
				
				if(String.Compare(lstType1[i].Type,lstType2[i].Type)!=0)
				{										
					if(lstType1[i].Order > lstType2[i].Order)
						return 1;
					else
						return -1;					
				}
				int j = i;
				string strCurrType = lstType1[i].Type;
				while(j < lstType1.Count && lstType1[j].Type == strCurrType)
					j+=1;
				
				for(int intCtr = i; intCtr<j; intCtr++)
					str1 += lstType1[intCtr].Value;
				
				j=i;
				
				while(j < lstType2.Count && lstType2[j].Type == strCurrType)
					j+=1;				
				
				for(int intCtr = i; intCtr<j; intCtr++)
					str2 += lstType2[intCtr].Value;
				
				i=j;
				
				if(strCurrType == "digit" && str1.Length != str2.Length)
				{
					int int1 = Convert.ToInt32(str1);
					int int2 = Convert.ToInt32(str2);
					
					if (int1 > int2)
						return 1;
					else
						return -1;
				}
				
				int result = string.Compare(str1,str2);				
				
				if(result!=0)
					return result;
			}			
			
			if(s1.CompareTo(s2)==1)
				return 1;
			else if (s1.CompareTo(s2)==-1)
				return -1;
			return 0;
		}
		
		public bool CompareDeb(string strVar1, string strVar2, string strEqualityOperation)
        {
			int intCompareVal = Compare(strVar1,strVar2);
            bool blnResult = false;
            if (strEqualityOperation.Length == 0)
                blnResult = true;
            else if (strEqualityOperation == "=")
            {
                if (intCompareVal == 0)
                    blnResult = true;
            }
            else if (strEqualityOperation == ">>")
            {
                if (intCompareVal > 0)
                    blnResult = true;
            }
            else if (strEqualityOperation == "<<")
            {
                if (intCompareVal < 0)
                    blnResult = true;
            }
            else if (strEqualityOperation == ">=")
            {
                if (intCompareVal >= 0)
                    blnResult = true;
            }
            else if (strEqualityOperation == "<=")
            {
                if (intCompareVal <= 0)
                    blnResult = true;
            }
            return blnResult;
        }
	}
	
	public class VersionExtract
	{
		int intEpoch;
		string strVersion;
		string strRevision;	
		
		public VersionExtract(string version)
		{
			string[] strEpoch = version.Split(':');
			
			if(strEpoch.Length>1)
			{
				int intValue = Convert.ToInt32(strEpoch[0]);
				if(intValue>0 && intEpoch <10)
					intEpoch = intValue;
				else
					return; //Invalid epoch data				
			}
			else
			{
				this.intEpoch = 0;
				strEpoch[0] = "0";
				version = "0:"+version;
			}
			
			//get the second segment after the first ':', then split by '-'
			string[] strVersion = version.Split(":".ToCharArray(),2)[1].Split('-');
			if(strVersion.Length>1)
			{
				this.strRevision = strVersion[strVersion.Length-1];
				this.strVersion = version.Split(":".ToCharArray(),2)[1]; //get the second segment of the original version
			}
			else
			{
				this.strRevision = null;
				this.strVersion = strVersion[0];
			}
		}
		
		public int Epoch
		{
			get {return intEpoch; }
		}
		
		public string Version
		{
			get { return strVersion; }
		}
		
		public string Revision
		{
			get { return strRevision; }
		}
	}
	
	public class VersionType
	{		
		char ch;
		
		public VersionType(char ch)
		{
			this.ch = ch;
		}
		
		public char Value
		{
			get { return ch; }
		}
		
		public string Type
		{
			get {
				if(char.IsLetter(ch))
					return "alpha";
				else if(char.IsDigit(ch))
					return "digit";
				else if(ch == '~')
					return "tilde";
				else
					return "delimit";
			}
		}
		
		public int Order
		{
			get{
				if(ch == '~')
					return -1;
				else if(char.IsDigit(ch))
					return 0;
				else if(ch.Equals(null))
					return 0;
				else if(char.IsLetter(ch))
					return (int)ch;
				else
					return (int)ch + 256;
			}
		}
	}
}