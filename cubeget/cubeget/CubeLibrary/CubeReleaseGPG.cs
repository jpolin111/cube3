/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.IO;
using System.Security.Cryptography;

namespace Cube
{
	public class CubeReleaseGPG
	{
		string strRelease;
		string strReleaseGPG;
		string strReleaseFilename;
		string strReleaseGPGFilename;
		string strMainSource;
		
		public CubeReleaseGPG () {}
		
		public CubeReleaseGPG (string strMainSource)
		{
			this.strRelease = "http://"+strMainSource + "/Release";
			this.strReleaseGPG = "http://"+strMainSource + "/Release.gpg";
			this.strMainSource = strMainSource;
			this.strReleaseFilename = strRelease.Replace("http://", "").Replace("http:/", "").Replace("/", "_").Trim();
			this.strReleaseGPGFilename = strReleaseGPG.Replace("http://", "").Replace("http:/", "").Replace("/", "_").Trim();
		}
		
		public string MainSource
		{
			get { return strMainSource;}
		}
		
		public string ReleaseLink
		{
			get { return strRelease; }
		}
		
		public string ReleaseGPGLink
		{
			get { return strReleaseGPG; }
		}
		
		public string ReleaseFilename
		{
			get { return strReleaseFilename; }
		}
		
		public string ReleaseGPGFilename
		{
			get { return strReleaseGPGFilename; }
		}
		
		public static bool CheckIfSourceUpdated(CubeProject proj, CubeSource s)
		{			
			if(!File.Exists(proj.ListDirectory + s.ReleaseFilename))
				return false;
		
			StreamReader reader = new StreamReader(proj.ListDirectory + s.ReleaseFilename);
			
			bool blnMD5SumLine = false;
			while(!reader.EndOfStream)
			{				
				string txt = reader.ReadLine().Trim();
				
				if(txt.Contains("MD5Sum") && !blnMD5SumLine)
				{
					blnMD5SumLine = true;
					continue;
				}
				
				if(blnMD5SumLine && txt.Contains(":"))
					break;				
								
				if(blnMD5SumLine)				
				{
					string[] line = txt.Split(" ".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
					
					if(s.Link.Contains(line[2])) //Check matched source file
					{
						string sourceFile = proj.ListDirectory + s.Filename;
						
						if(!File.Exists(sourceFile))
							return false;
						
						FileStream stream = new FileStream(sourceFile,FileMode.Open);
						string sum = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(stream)).Replace("-","").ToLower().Trim();
						
						if(sum == line[0]) //Check MD5Sum
							return true;
						return false;
					}
				}				
			}
			
			reader.Close();
			return false;
		}
	}	
}

