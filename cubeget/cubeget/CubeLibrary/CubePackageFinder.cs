/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Collections.Generic;
namespace Cube
{
	public class CubePackageFinder
	{
		public static int FindIndex(string strPackageName,List<CubePackage> lst)
		{
			CubePackage p = new CubePackage();
			p.AddToPackageInfo("Package", strPackageName);
			int index = Array.BinarySearch<CubePackage>(lst.ToArray(),p,new Comparer());
			if(index>=0)
			{
				return index;
			}
			return -1;
		}
		
		public static CubePackage Find(int intIndex, SortedList<string,CubePackage> dic)
		{
			CubePackage p;
			try
			{
				string s = dic.Keys[intIndex];
				if(!dic.TryGetValue(s,out p))
					return null;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}
			return p;
		}
		
		public static CubePackage Find(string strPackageName,List<CubePackage> lst)
		{
			int intIndex = FindIndex(strPackageName,lst);
			if(intIndex!=-1)
				return lst[intIndex];
			return null;
		}
		
		public static CubePackage Find(string strPackageName, SortedList<string,CubePackage> dic)
		{
			CubePackage p;
			dic.TryGetValue(strPackageName,out p);
			return p;
		}
		
		public static CubePackage[] FindUpdates(List<CubePackage> lstInstalled)
		{
			return Array.FindAll<CubePackage>(lstInstalled.ToArray(), Updates);
		}
		
		public static CubePackage[] FindUpdates(SortedList<string,CubePackage> dicInstalled)
		{
			
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicInstalled.Keys)
			{
				CubePackage p;
				string status;
				dicInstalled.TryGetValue(package, out p);				
				if(p.PackageDictionary.TryGetValue("Status",out status))
				{
					if(status == "-1")
						lstPackages.Add(p);
				}
			}
			return lstPackages.ToArray();
		}
		
		public static CubePackage[] FindByStatus(string pStatus, SortedList<string,CubePackage> dicInstalled)
		{
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicInstalled.Keys)
			{
				CubePackage p;
				string status;
				dicInstalled.TryGetValue(package, out p);
				if(p.PackageDictionary.TryGetValue("Status",out status))
				{
					if(status == pStatus)
						lstPackages.Add(p);
				}
			}
			return lstPackages.ToArray();
			
		}
		
		public static CubePackage[] FindByKey(string key, SortedList<string,CubePackage> dicAvailable)
		{
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicAvailable.Keys)
			{
				CubePackage p;				
				dicAvailable.TryGetValue(package, out p);
				if(p.PackageDictionary.ContainsKey(key))
					lstPackages.Add(p);
			}
			return lstPackages.ToArray();
		}
		
		public static CubePackage[] FindByPackageName(string pName,string pType, SortedList<string,CubePackage> dicAvailable)
		{
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicAvailable.Keys)
			{
				CubePackage p;
				string name;
				dicAvailable.TryGetValue(package, out p);
				if(p.PackageDictionary.TryGetValue("Package",out name))
				{	
					if(pType == "start")
					{
						if(name.StartsWith(pName))
							lstPackages.Add(p);
					}
					else if(pType == "end")
					{
						if(name.EndsWith(pName))
							lstPackages.Add(p);
					}
					else if(pType == "contains")
					{
						if(name.Contains(pName))
							lstPackages.Add(p);
					}
				}
			}
			return lstPackages.ToArray();			
		}
		
		public static CubePackage[] FindByCategory(string strkey,string strval, SortedList<string,CubePackage> dicAvailable)
		{
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicAvailable.Keys)
			{
				CubePackage p;
				string val;
				dicAvailable.TryGetValue(package, out p);
				if(p.PackageDictionary.TryGetValue(strkey,out val))
				{					
					if(val.ToLower().Contains(strval.ToLower()))
					{
						lstPackages.Add(p);
					}
				}				
			}
			return lstPackages.ToArray();
		}
		
		public static CubePackage[] FindReverseDepends(string strPackage, SortedList<string,CubePackage> dicAvailable)
		{
			List<CubePackage> lstPackages = new List<CubePackage>();
			foreach(string package in dicAvailable.Keys)
			{
				CubePackage p;
				string val;
				string depends="";
				
				dicAvailable.TryGetValue(package, out p);
								
				if(p.PackageDictionary.TryGetValue("Depends",out val))
					depends += val;
				if(p.PackageDictionary.TryGetValue("Pre-Depends",out val))
					depends += ","+val;
				if(p.PackageDictionary.TryGetValue("Recommends",out val))
					depends += ","+val;
								
				depends = " "+depends.Replace(","," ")+" ";
				if(depends.Contains(" "+strPackage+" "))
				{
					lstPackages.Add(p);
				}
			}
			return lstPackages.ToArray();
		}
		
		private static bool Updates(CubePackage p)
        {
            if(p.GetValue("Status") == "-1")
				return true;
			return false;
        }
	}
}