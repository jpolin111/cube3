/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using Cube;
using System.Collections.Generic;
using System.IO;

namespace Cube
{	
	public class CubeDebRecover
	{
		string app = "fakeroot";
		string param;
		
		public CubeDebRecover()
		{
			CubeConfiguration config = new CubeConfiguration();
			this.param = "-u \"" + Directory.GetCurrentDirectory() + config.Slash + config.BinDirectory + "dpkg-repack\" --root=/ ";
		}
		
		public bool RecoverDeb(CubePackage p,string dir)
		{
			Console.WriteLine("[Cube App Share] Repacking " + p.GetValue("Package"));
			return this.RecoverDeb(new List<CubePackage>(new CubePackage[]{p}),dir);
		}
		
		public bool RecoverDeb(List<CubePackage> lstPks, string dir)
		{
			if(!Directory.Exists(dir))
				Directory.CreateDirectory(dir);
			
			string debs = "";
			
			foreach(CubePackage p in lstPks)
				debs += p.GetValue("Package").ToLower()+ " ";
			
			string param = this.param + debs;
			
			int res = ProcessOutput.Run(app,param,dir);
			if(res == 0)
				return true;
			return false;
		}	
	}
}