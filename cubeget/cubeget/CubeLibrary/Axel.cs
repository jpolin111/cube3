/*
 * Axel Downloader Interface Library
 * (c) Camicri Systems
 * 
 * Library for C# that provide controls to axel downloader (Linux and Windows)
 * */

using System;
using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Axel
{
	public class AxelData
	{
		string strFileName="";
		string strFileDirectory="";
		string strApplicationName = "axel";
		string strApplicationDirectory="";
		
		string strURL;		
		
		string strArguments="";
		string strAdditionalArguments="";		
		
		bool blnDeleteExisting = false;
		bool blnIsAsynchronous = false;
		
		int intConnections=10;
		
		public string ApplicationName
		{
			set { strApplicationName = value; }
			get { return strApplicationName; }
		}
		
		public string ApplicationDirectory
		{
			set { strApplicationDirectory = value;}
			get { return strApplicationDirectory; }
		}
		
		public string FullApplicationPath
		{
			get { return strApplicationDirectory + Path.DirectorySeparatorChar + strApplicationName;}
		}
		
		public string FileName
		{
			set { strFileName = value;}
			get { return strFileName; }
		}
		
		public string FileDirectory
		{
			set { strFileDirectory = value;}
			get { return strFileDirectory; }
		}
		
		public string FullFilePath
		{
			get {
				if(FileDirectory.Length>0)
					return FileDirectory+ Path.DirectorySeparatorChar + FileName;
				else
					return FileName;
			}
		}
		
		public string URL
		{
			set { strURL = value; }
			get { return strURL; }
		}		
		
		public string AdditionalArguments
		{
			set { strAdditionalArguments = value;}
			get { return strAdditionalArguments; }
		}
		
		public int Connections
		{
			set { intConnections = value; }
			get { return intConnections; }
		}
		
		public string Arguments
		{
			get
			{				
				if(FileName.Length>0)
					strArguments += " --output='"+FullFilePath+"'";
				if(Connections!=0)
					strArguments += " --num-connections="+intConnections;
				if(AdditionalArguments.Length>0)
					strArguments += " " + AdditionalArguments;
				strArguments += " " + URL;
				return strArguments;
			}
			
			set
			{
				strArguments += " " + value + " " ;
			}
		}
		
		public bool DeleteExisting
		{
			set { blnDeleteExisting = value;}
			get { return blnDeleteExisting;}
		}
		
		public bool IsAsynchronous
		{
			set { blnIsAsynchronous = value;}
			get { return blnIsAsynchronous; }
		}
		
	}
	
    public class AxelDownloader
    {        
		AxelData data;
		        
        public bool blnErrorOccured = false;
        public int MaxConnections;

        int intProgressPercent;
        string strTransferRate = "";
        string strTotalByteSize = "";
        string strState = "";        
		        
		//Form Invoker;
        Exception ex;
        Process p;
        Thread tExec;		

        public event EventHandler ProgressChanged;
        public event EventHandler StateChanged;
        public event EventHandler Initialized;
        public event EventHandler DownloadStarted;
        public event EventHandler ErrorOccured;
        public event EventHandler Finished;

        delegate void ProgressCallback(object sender, EventArgs e);
        delegate void InitializedCallback(object sender, EventArgs e);
        delegate void DownloadStartedCallback(object sender, EventArgs e);
        delegate void ErrorOccuredCallback(object sender, EventArgs e);
        delegate void FinishedCallback(object sender, EventArgs e);
        delegate void StateChangedCallback(object sender, EventArgs e);

        public AxelDownloader(AxelData data)
        {
			this.data = data;
        }

        #region [Methods called to trigger an event]

        protected virtual void OnInitialized()
        {
			/*
            if (Invoker != null)
            {
                if(Initialized != null)
                    Invoker.Invoke(new InitializedCallback(InitializedM), new object[] { this, EventArgs.Empty });
            }
            else*/
                InitializedM(this, EventArgs.Empty);
        }

        protected virtual void OnDownloadStarted()
        {
			/*
            if (Invoker != null)
            {
                if(DownloadStarted != null)
                    Invoker.Invoke(new DownloadStartedCallback(DownloadStartedM), new object[] { this, EventArgs.Empty });
            }
            else*/
                DownloadStartedM(this, EventArgs.Empty);
        }

        protected virtual void OnProgressChanged()
        {
			/*
            if (Invoker != null)
            {
                if(ProgressChanged != null)
                    Invoker.Invoke(new ProgressCallback(ProgressChangedM), new object[] { this, EventArgs.Empty });
            }
            else*/
                ProgressChangedM(intProgressPercent, EventArgs.Empty);
        }

        protected virtual void OnStateChanged()
        {
			/*
            if (Invoker != null)
            {
                if(StateChanged != null)
                    Invoker.Invoke(new StateChangedCallback(StateChangedM), new object[] { this, EventArgs.Empty });
            }
            else*/
                StateChangedM(strState, EventArgs.Empty);
        }

        protected virtual void OnFinished()
        {
			/*
            if (Invoker != null)
            {
                if(Finished != null)
                    Invoker.Invoke(new FinishedCallback(FinishedM), new object[] { this, EventArgs.Empty });
            }
            else*/
                FinishedM(this, EventArgs.Empty);
        }

        protected virtual void OnErrorOccured()
        {
            blnErrorOccured = true;
			/*
            if (Invoker != null)
            {
                if (ErrorOccured != null)
                    Invoker.Invoke(new ErrorOccuredCallback(ErrorOccuredM), new object[] { this, EventArgs.Empty });
            }
            else*/
                ErrorOccuredM(ex.Message, EventArgs.Empty);

            if (tExec != null)
            {
                if (tExec.IsAlive)
                    tExec.Abort();
            }
        }

        #endregion

        #region [Event Methods]

        private void InitializedM(object sender, EventArgs e)
        {
            if(Initialized!=null)
                Initialized(sender, e);
        }
        
        private void DownloadStartedM(object sender, EventArgs e)
        {
            if(DownloadStarted!=null)
                DownloadStarted(sender, e); 
        }

        private void ProgressChangedM(object sender, EventArgs e)
        {
            if(ProgressChanged!=null)
                ProgressChanged(sender, e);
        }

        private void StateChangedM(object sender, EventArgs e)
        {
            if(StateChanged!=null)
                StateChanged(sender, e);
        }

        private void ErrorOccuredM(object sender, EventArgs e)
        {       
            if(ErrorOccured!=null)
                ErrorOccured(sender, e);
        }

        private void FinishedM(object sender, EventArgs e)
        {
            if(Finished!=null)
                Finished(sender, e);
            p.StandardOutput.Close();
            p.Dispose();

            if (tExec != null)
            {				
                if (tExec.IsAlive)
                    tExec.Abort();
            }
        }

        #endregion

        #region [Accessor Methods]

        public AxelData Data
        {
			get { return data;}
        }

        public int Progress
        {
			get {return intProgressPercent;}
        }

        public string TotalSize
        {
			get {return strTotalByteSize;}
        }

        public string State
        {
			get {return strState;}
        }

        public string TransferRate
        {
			get{return strTransferRate;}
        }

        public string ErrorMessage
        {
			get{
            if (ex != null)
                return ex.Message;
            else
                return "No Errors";
			}
		}        

        #endregion
		/*
        public Form AxelInvoker
        {
			set{this.Invoker = value;}
        }
        */
		
        #region [Download Methods]

        public void StartDownloadThread()
        {
			
            if (data.IsAsynchronous)
            {
                if (tExec != null)
                {
                    if (tExec.IsAlive)
                        tExec.Abort();
                }
                tExec = new Thread(StartDownload);
                tExec.Start();
            }
            else
                StartDownload();
        }

        private void StartDownload()
        {			
            OnDownloadStarted();
            p = new Process();

            blnErrorOccured = false;

			if(data.FileName!=null)
			{
				if(data.DeleteExisting)
				{
					if (File.Exists(data.FullFilePath))
						DeleteFile(data.FullFilePath);
				}
			}            

            ProcessStartInfo startInfo = new ProcessStartInfo();
			
			if(File.Exists(data.FullApplicationPath))
			{
				startInfo.FileName = data.FullApplicationPath;
				Console.WriteLine("Using static axel in "+startInfo.FileName);
			}
			else
			{				
				startInfo.FileName = "axel"; //Default
				Console.WriteLine("Using local axel");
			}
			
			if(!Directory.Exists(data.FileDirectory))
				Directory.CreateDirectory(data.FileDirectory);
			
            startInfo.Arguments = data.Arguments;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.CreateNoWindow = true;
			
			//Change working directory to File directory if
			//file name is not given
			if(data.FileDirectory.Length > 0 && data.FileName.Length==0)
				startInfo.WorkingDirectory = data.FileDirectory;
			
			Console.WriteLine("[AXEL] "+data.Arguments);

            p.StartInfo = startInfo;
            try
            {
                p.Start();
            }
            catch (Exception ex)
            {
                this.ex = ex;
                OnErrorOccured();
                return;
            }

            while (!p.StandardOutput.EndOfStream)
            {
                string strLine = p.StandardOutput.ReadLine();
				Console.Write("[AXEL]"+strLine+"\r");
                if (AnalyzeOutput(strLine))
                    return;
            }
			Console.WriteLine();
			
			p.Dispose();
            
            this.ex = new Exception("Failed to download file, server no response, please check for your internet connection or please try again later.\r\nLast Message : "+strState);
            OnErrorOccured();
        }

        public bool AnalyzeOutput(string strLine)
        {
            strLine = strLine.Trim();
            if (strLine.Length == 0)
                return false;

            if (strLine.StartsWith("Initializing download"))
            {
                strState = strLine;
                OnInitialized();
                OnStateChanged();
            }
            else if (strLine.StartsWith("File size"))
            {				
                strState = strLine;
                string strSplit = strLine.Split(':')[1].Trim();				
                strTotalByteSize = strSplit.Split(' ')[0].Trim();
				strTotalByteSize = ConvertSize(strTotalByteSize);
                OnStateChanged();
            }
            else if (strLine.StartsWith("Opening"))
            {
                strState = strLine;
                OnStateChanged();
            }
            else if (strLine.StartsWith("["))
            {
                string strProgress = strLine.Substring(1, strLine.IndexOf('%')-1);
                intProgressPercent = int.Parse(strProgress.Trim());
                strTransferRate = strLine.Substring(strLine.LastIndexOf('[')+1).Replace("]", "").Trim();
                strState = "Downloading...";
                OnStateChanged();
                OnProgressChanged();
            }
            else if (strLine.StartsWith("Downloaded"))
            {
                strState = strLine;
                OnStateChanged();
				intProgressPercent = 100;
				OnProgressChanged();
                OnFinished();
                return true;
            }
            else if (strLine.StartsWith("Error") || strLine.StartsWith("Unable"))
            {
                strState = strLine;
                ex = new Exception(strLine);
                OnErrorOccured();
            }
                
            else
            {
                strState = strLine;
            }
            return false;
        }

        private bool DeleteFile(string strFilename)
        {
            try
            {
                File.Delete(strFilename);
				if(File.Exists(strFilename+".st"))
				   File.Delete(strFilename);
                return true;
            }
            catch (Exception ex)
            {
                this.ex = ex;
                OnErrorOccured();
                return false;
            }//catch
        }

        private string ConvertToLinuxPath(string str)
        {
            return str.Replace("\\", "/");
        }

        #endregion

        public void Terminate()
        {
            if (tExec.IsAlive)
                tExec.Abort();
            if (p != null)
            {
                try
                {
                    p.Kill();
                }
                catch
                {
                }
            }
        }
		
		private string ConvertSize(string size)
		{
			if(size==null)
				return "undefined";
			if(size.Length <= 3)
				size = size + " bytes";
			else if(size.Length <= 6)
				size = String.Format("{0:00} kb",Convert.ToDouble(size)/1000.00);
			else if(size.Length <= 9)
				size = String.Format("{0:00} mb",Convert.ToDouble(size)/1000000.00);
			else					
				size = String.Format("{0:00} gb",Convert.ToDouble(size)/1000000000.00);
			size = size.TrimStart('0');
			return size;
		}
    }
}
