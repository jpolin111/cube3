/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using o=System.Console;
namespace Cube
{
	public partial class CubeTerminal
	{
		public string ReadLine()
		{
			string entry;
			while(true)
			{
				o.Write("cube> ");
				entry = o.ReadLine();
				if(entry.Trim().Length > 0)
					break;
			}
			return entry;
		}
		
		public void ShowHeader()
		{
			o.WriteLine("Camicri Cube v1.0");
			o.WriteLine("-----------------");
		}
		
		public void DisplayPackageHeader()
		{
			o.WriteLine("{0}\t{1}\t{2}","".PadRight(20,'_'),"".PadRight(20,'_'),"".PadRight(20,'_'));
			o.WriteLine("{0}\t{1}\t{2}","Package".PadRight(20,' '),"Installed Version".PadRight(20,' '),"Latest Version".PadRight(20,' '));
			o.WriteLine("{0}\t{1}\t{2}","".PadRight(20,'_'),"".PadRight(20,'_'),"".PadRight(20,'_'));
		}
		
		public void DisplayPackage(CubePackage p)
		{
			string package = p.GetValue("Package");
			string installed = p.GetValue("Installed Version");
			string latest = p.GetValue("Version");
			if(installed==null)
				installed = "";
			o.WriteLine("{0}\t{1}\t{2}",package.PadRight(20,' '),installed.PadRight(20,' '),latest.PadRight(20,' '));
		}
		
		public bool AskYesNo(string message)
		{
			while(true)
			{
				o.Write(message + "(y/n) : ");
				string res = o.ReadLine();
				if(res.Trim().Length != 0)
				{
					char chAns = char.ToLower(res[0]);
					if(chAns == 'y')
						return true;
					else if(chAns == 'n')
						return false;
				}
			}
		}
	}
}