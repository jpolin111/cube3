/*
 * Camicri Cube
 * Copyright © Jake Capangpangan <camicrisystems@gmail.com>
 * 
 * Camicri Cube is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * Camicri Cube is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with Camicri
 * Cube; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 * 
 * */
using System;
using System.Collections.Generic;
namespace Cube
{
	public class Comparer : IComparer<CubePackage>
    {
        public int Compare(CubePackage x, CubePackage y)
        {			
            int result = string.Compare(x.GetValue("Package"), y.GetValue("Package"), true);			
			if(result == 0 && x.GetValue("Version")!=null)
			{				
				DebianCompare cmp = new DebianCompare();
				return cmp.Compare(x.GetValue("Version"), y.GetValue("Version"));
			}
			return result;
        }
    }

	public class ComparerConstraints : IComparer<CubeAptPreferencesConstraints>
	{
		public int Compare(CubeAptPreferencesConstraints x, CubeAptPreferencesConstraints y)
		{
			if (x.Priority > y.Priority)
				return -1;
			else if (x.Priority < y.Priority)
				return 1;
			return 0;
		}
	}	
	
	public class AptPreferencesComparer : IComparer<CubeAptPreferencesConstraints>
	{
		public int Compare(CubeAptPreferencesConstraints x, CubeAptPreferencesConstraints y)
		{
			if (x.Priority > y.Priority)
				return 1;
			else if (x.Priority < y.Priority)
				return -1;
			return 0;
		}
	}
}

